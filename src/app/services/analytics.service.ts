import { Injectable } from '@angular/core';
import { SchemaAssertionError } from '@app/types/schema';
import { environment } from '@env/environment';

declare function ga(
  cmd: 'send',
  hitType: 'exception',
  fields: { description: string; fatal: boolean },
): void;

@Injectable({ providedIn: 'root' })
export class AnalyticsService {
  schemaException(label: string, err: SchemaAssertionError) {
    const description = `${label}: ${err.message}`;
    if (environment.production) {
      console.warn('sent exception to Google Analytics');
      ga('send', 'exception', { description, fatal: false });
    } else {
      console.warn('did NOT send exception to Google Analytics (not prod env)');
      console.error(description);
    }
  }
}
