import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { filter, first, map, mergeMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { GlobalState } from '@app/state';
import * as selectors from '@app/selectors';
import { isLoading, isSuccess } from 'loadable.ts';

export const LAST_DISMISSED_BANNER_KEY = 'lastDismissedBanner';

export interface Banner {
  text: string;
  dismissalId: string;
  appearsAfter?: number;
  hiddenAfter?: number;
}

interface Banners {
  banners: Banner[];
}

@Injectable({ providedIn: 'root' })
export class BannerService {
  public banner = new BehaviorSubject<Banner | null>(null);

  constructor(private http: HttpClient, private store: Store<GlobalState>) {
    this.loadBanners().then(banner => this.banner.next(banner));
  }

  private async loadBanners(): Promise<Banner | null> {
    try {
      return await this.http
        .get<Banners>('/config/banner.json')
        .pipe(
          // Wait until the user preferences have loaded before deciding if the
          // user has already dismissed the current banner.
          mergeMap(({ banners }) => {
            return this.store.select(selectors.prefs.getAll).pipe(
              filter(prefs => !isLoading(prefs)),
              map(prefs => {
                return isSuccess(prefs)
                  ? prefs.value[LAST_DISMISSED_BANNER_KEY]
                  : null;
              }),
              map(pref => [banners, pref] as const),
            );
          }),
          first(),

          // Pick the banner that should appear, THEN check if the user has
          // already dismissed that banner. If the user has dismissed the banner,
          // show no banners. DO NOT pick a fallback banner.
          map(([banners, lastDismissedBanner]) => {
            const banner = this.pickBanner(banners, Date.now());
            if (lastDismissedBanner === banner?.text) {
              return null;
            } else {
              return banner;
            }
          }),
        )
        .toPromise();
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  private pickBanner(banners: Banner[], now: number): Banner | null {
    // Remove banners that ended before now
    banners = banners.filter(function (banner) {
      if (typeof banner.hiddenAfter === 'number') {
        return banner.hiddenAfter > now;
      } else {
        return true;
      }
    });

    // Remove banners that start after now
    banners = banners.filter(function (banner) {
      if (typeof banner.appearsAfter === 'number') {
        return banner.appearsAfter < now;
      } else {
        return true;
      }
    });

    if (banners.length > 0) {
      return banners[0];
    } else {
      return null;
    }
  }
}
