export interface Institution {
  darsInstitutionCode: string;
  darsInstitutionCodeDescription: string;
}
