export interface AuditBottomSection {
  memorandaSection: null | {
    label: string;
    memorandaLines: readonly string[];
  };
  exceptionsSection: null | {
    label: string;
    exceptions: readonly AuditException[];
  };
}

export interface AuditException {
  date: string;
  exceptionTypeCode: string;
  memo: string;
}
