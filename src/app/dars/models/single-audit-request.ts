export interface SingleAuditRequest {
  sisEmplId?: string;
  darsInstitutionCode: string;
  darsDegreeProgramCode: string;
  darsCatalogYearTerm?: string;
  darsAlternateCatalogYearTerm1?: string;
  darsHonorsOptionCode?: string;
  whichEnrolledCoursesIncluded?: string;
  degreePlannerPlanName?: string;
}
