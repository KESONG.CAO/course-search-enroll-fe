export type Json =
  | null
  | boolean
  | number
  | string
  | Json[]
  | { [key: string]: Json };

export namespace Json {
  export const isNull = (a: Json): a is null => a === null;

  export const isBoolean = (a: Json): a is boolean => typeof a === 'boolean';

  export const isNumber = (a: Json): a is number => typeof a === 'number';

  export const isString = (a: Json): a is string => typeof a === 'string';

  export const isArray = (a: Json): a is Json[] => {
    return typeof a === 'object' && a !== null && Array.isArray(a);
  };

  export const isDict = (a: Json): a is { [key: string]: Json } => {
    return typeof a === 'object' && a !== null && !Array.isArray(a);
  };
}
