export type Loadable<Data extends {}, Shared extends {} = {}> =
  | { mode: 'unloaded' }
  | ({ mode: 'loading' } & Shared)
  | ({ mode: 'error' } & Shared)
  | ({ mode: 'loaded' } & Shared & Data);

export const UNLOADED = { mode: 'unloaded' } as const;

export type Toggleable<Data extends {}> =
  | { mode: 'unloaded' }
  | ({ mode: 'loaded' } & Data);

export const sharedOrNull = <T>(a: Loadable<{}, T>): T | null => {
  if (a.mode === 'unloaded') {
    return null;
  } else {
    return a;
  }
};

export const loadedOrNull = <T>(a: Loadable<T> | Toggleable<T>): T | null => {
  if (a.mode === 'loaded') {
    return a;
  } else {
    return null;
  }
};

export const notUnloaded = <T>(a: Loadable<T> | Toggleable<T>): boolean => {
  return a.mode !== 'unloaded';
};
