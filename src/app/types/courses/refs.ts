import { curry2 } from '@app/core/utils';
import { TermCode } from '../terms';
import { HasCourseRef } from './attributes';
import { CurrentId } from './current';
import { RoadmapId } from './roadmap';
import { SavedId } from './saved';
import Schema, * as s from '@app/types/schema';

export interface RoadmapCourseRef {
  kind: 'roadmap';
  termCode: TermCode;
  roadmapId: RoadmapId;
}

export namespace RoadmapCourseRef {
  export const is = (ref: CourseRef): ref is RoadmapCourseRef => {
    return ref.kind === 'roadmap';
  };

  export const equals = (a: RoadmapCourseRef, b: RoadmapCourseRef): boolean => {
    return (
      TermCode.equals(a.termCode, b.termCode) && a.roadmapId === b.roadmapId
    );
  };

  export const schema = s.object<RoadmapCourseRef>({
    kind: s.constant('roadmap'),
    termCode: TermCode.schema,
    roadmapId: s.string,
  });
}

export interface CurrentCourseRef {
  kind: 'current';
  termCode: TermCode;
  currentId: CurrentId;
}

export namespace CurrentCourseRef {
  export const is = (ref: CourseRef): ref is CurrentCourseRef => {
    return ref.kind === 'current';
  };

  export const equals = (a: CurrentCourseRef, b: CurrentCourseRef): boolean => {
    return (
      TermCode.equals(a.termCode, b.termCode) && a.currentId === b.currentId
    );
  };

  export const schema = s.object<CurrentCourseRef>({
    kind: s.constant('current'),
    termCode: TermCode.schema,
    currentId: s.string,
  });
}

export interface SavedCourseRef {
  kind: 'saved';
  savedId: SavedId;
}

export namespace SavedCourseRef {
  export const is = (ref: CourseRef): ref is SavedCourseRef => {
    return ref.kind === 'saved';
  };

  export const equals = (a: SavedCourseRef, b: SavedCourseRef): boolean => {
    return a.savedId === b.savedId;
  };

  export const schema = s.object<SavedCourseRef>({
    kind: s.constant('saved'),
    savedId: s.number,
  });
}

export type CourseRef = RoadmapCourseRef | CurrentCourseRef | SavedCourseRef;

export namespace CourseRef {
  export const equals = (a: CourseRef, b: CourseRef): boolean => {
    if (a.kind === b.kind) {
      switch (a.kind) {
        case 'roadmap':
          return RoadmapCourseRef.equals(a, b as RoadmapCourseRef);
        case 'current':
          return CurrentCourseRef.equals(a, b as CurrentCourseRef);
        case 'saved':
          return SavedCourseRef.equals(a, b as SavedCourseRef);
      }
    }
    return false;
  };

  export const equalsCurry = curry2(equals);

  export const matches = (ref: CourseRef) => {
    return (course: HasCourseRef): boolean => {
      return CourseRef.equals(ref, course.ref);
    };
  };

  export const schema: Schema<CourseRef> = s
    .or(RoadmapCourseRef.schema)
    .or(CurrentCourseRef.schema)
    .or(SavedCourseRef.schema);
}
