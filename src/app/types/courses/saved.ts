import * as s from '@app/types/schema';
import { Term, Zero } from '@app/types/terms';
import { RawDetails, Details } from './details';
import { Course } from './attributes';
import { SavedCourseRef } from './refs';

export interface RawSavedCourse {
  catalogNumber: string;
  courseId: string;
  id: number;
  subjectCode: string;
  title: string;
  topicId: number;
}

export namespace RawSavedCourse {
  export const schema = s.object<RawSavedCourse>({
    catalogNumber: s.string,
    courseId: s.string,
    id: s.number,
    subjectCode: s.string,
    title: s.string,
    topicId: s.number,
  });
}

export type SavedId = number;

export interface SavedCourse extends Course {
  ref: SavedCourseRef;
}

export namespace SavedCourse {
  export const is = (course: Course | null): course is SavedCourse => {
    return s.object({ ref: SavedCourseRef.schema }).matches(course);
  };

  export const from = (
    term: Term<Zero>,
    raw: RawSavedCourse,
    details: RawDetails,
  ): SavedCourse => {
    const subject = term.subjects.find(({ code }) => code === raw.subjectCode);

    if (!subject) {
      throw new Error(`unknown subject-code ${raw.subjectCode} `);
    }

    return {
      ref: {
        kind: 'saved',
        savedId: raw.id,
      },

      // HasTitle
      shortCatalog: `${details.subject.shortDescription} ${raw.catalogNumber}`,
      longCatalog: `${details.subject.formalDescription} ${raw.catalogNumber}`,
      title: raw.title,

      // HasSubject
      subject,

      // HasCourseId
      courseId: raw.courseId,

      // HasCredits
      credits: { min: details.minimumCredits, max: details.maximumCredits },

      // HasLazyDetails
      details: { mode: 'loaded', ...Details.from(details) },
    };
  };

  export const fromCurry = (term: Term<Zero>, raw: RawSavedCourse) => {
    return (details: RawDetails) => from(term, raw, details);
  };
}
