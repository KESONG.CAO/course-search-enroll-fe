/**
 * Indicates a type that we know will have to be implemented in the future but
 * that we only know exists right now. Be careful when replacing this with a
 * concrete type to make sure that ALL possible values have been expressed. For
 * example:
 *
 * - Can the value be null? If it's a string 99% of the time, we especially need
 *   to know that it can be null 1% of the time.
 *
 * - Check the AngularJS app to see if existing code handles uncommon forms of
 *   this type.
 *
 * - Check with other teammembers to see if they know of uncommon forms this
 *   type can have that need to be represented.
 */
export type TODO = unknown;

export type UnixMilliseconds = number;

export type Seconds = number;
export type Minutes = number;

export type Pixels = number;
