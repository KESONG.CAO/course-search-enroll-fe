export type Styles = Record<string, string | number>;

/**
 * In order to maximize the distruction of colors, colors should be ordered in
 * the array by hue. The total number of colors should be a prime number so that
 * all colors will be used once before any colors repeat.
 */
const ALL_COLORS = [
  '#860000',
  // '#C5050C', don't use UW brand colors
  // '#EB004C', hidden to increase color distribution
  '#CD3D85',
  '#D54309',
  '#AA6003',
  '#8E704F',
  // '#8F7400', hidden to increase color distribution
  '#008817',
  '#457A3B',
  '#4A77B4',
  // '#0076D6', hidden to increase color distribution
  '#133FD8',
  '#6C5FF2',
  '#574CFA',
  '#540FB0',
  '#000066',
  // '#646569', hidden to increase color distribution
];

const PHI = (1 + Math.sqrt(5)) / 2;

export interface ColorQueue {
  pick(): Styles;
}

export class FullColorQueue implements ColorQueue {
  private next = 0;

  // distrubte colors in the palette using golden-ratio - Isaac
  private increment = Math.round(this.allColors.length * PHI);

  constructor(private allColors = ALL_COLORS) {
    console.assert(allColors.length > 0);
  }

  public pick(): Styles {
    const background = this.allColors[this.next] ?? '#0479a8';
    this.next = (this.next + this.increment) % this.allColors.length;
    return { background, color: 'white' };
  }
}

export class GrayColorQueue implements ColorQueue {
  public pick(): Styles {
    return { background: 'white', color: 'black', border: '1px solid black' };
  }
}
