import { DateTime } from 'luxon';
import { UnixMilliseconds } from '@app/types';

const UTC = 'utc';
const MADISON = 'America/Chicago';

// Madison, WI is 6 (or 5 hours during daylight-savings) hours after GMT
export const CST_OFFSET_IN_SECONDS =
  DateTime.local().setZone(MADISON, {
    keepLocalTime: true,
  }).offset * 60;

export const fromUtcSecondsToMadisonSeconds = (utc: number): number => {
  return utc + Math.abs(CST_OFFSET_IN_SECONDS);
};

export const fromMadisonSecondsToUtcSeconds = (madison: number): number => {
  return madison - Math.abs(CST_OFFSET_IN_SECONDS);
};

const to = (fmt: string, zone: string) => {
  return (millis: UnixMilliseconds): string => {
    return DateTime.fromMillis(millis, { zone }).toFormat(fmt);
  };
};

export const WeekdayMonthDayYear = 'EEEE, DD'; // Wed, Aug 6, 2014
export const MonthDayYear = 'DD';
export const MonthDay = 'LLL d';
export const HourMinuteMeridian = 't';
export const HourMeridiem = 'h a';
export const DateAndTime = `DD 'at' t`;

export const toWeekdayMonthDayYearMadison = to(WeekdayMonthDayYear, MADISON);
export const toMonthDayYearMadison = to(MonthDayYear, MADISON);
export const toMonthDayMadison = to(MonthDay, MADISON);
export const toHourMinuteMeridianMadison = to(HourMinuteMeridian, MADISON);
export const toDateAndTimeMadison = to(DateAndTime, MADISON);

export const toWeekdayMonthDayYearUtc = to(WeekdayMonthDayYear, UTC);
export const toMonthDayYearUtc = to(MonthDayYear, UTC);
export const toMonthDayUtc = to(MonthDay, UTC);
export const toHourMinuteMeridianUtc = to(HourMinuteMeridian, UTC);
export const toHourMeridiemUtc = to(HourMeridiem, UTC);
export const toDateAndTimeUtc = to(DateAndTime, UTC);
