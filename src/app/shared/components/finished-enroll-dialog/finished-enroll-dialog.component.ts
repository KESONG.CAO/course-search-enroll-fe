import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Course } from '@app/types/courses';
import { EnrollRequest } from '@app/types/enroll';
import * as s from '@app/types/schema';

export interface FinishedEnrollmentData {
  lastEnrollment: unknown[];
  requests: Array<[Course, EnrollRequest]>;
}

interface EnrollResult {
  isOkay: boolean;
  shortCatalog: string;
  messages: string[];
}

@Component({
  selector: 'cse-finished-enroll-dialog',
  templateUrl: './finished-enroll-dialog.component.html',
  styleUrls: ['./finished-enroll-dialog.component.scss'],
})
export class FinishedEnrollDialogComponent {
  public results: EnrollResult[];

  constructor(@Inject(MAT_DIALOG_DATA) data: FinishedEnrollmentData) {
    this.results = data.requests.map(
      ([course], index): EnrollResult => {
        const result = data.lastEnrollment[index] as any;
        return {
          isOkay: result.enrollmentState === 'Completed',
          shortCatalog: course.shortCatalog,
          messages: (JSON.parse(result.enrollmentMessages) as unknown[])
            .filter(s.object({ description: s.string }).matches)
            .map(message => message.description),
        };
      },
    );
  }
}
