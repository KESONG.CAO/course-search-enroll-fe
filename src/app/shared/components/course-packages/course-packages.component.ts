import { BreakpointObserver } from '@angular/cdk/layout';
import {
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NestedPackage, PackageGroup } from '@app/core/models/packageGroup';
import { unsubscribeAll } from '@app/core/utils';
import { ModeOfInstruction } from '@app/search/store/state';
import { Breakpoints } from '@app/shared/breakpoints';
import { EnrollmentPackage } from '@app/types/courses';
import { isSuccess, Loadable } from 'loadable.ts';
import { Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

export interface Filters {
  includeOpen: boolean;
  includeWaitlisted: boolean;
  includeClosed: boolean;
  includeHonors: boolean;
  includeMode: ModeOfInstruction;
}

const DEFAULT_FILTERS: Filters = {
  includeOpen: true,
  includeWaitlisted: true,
  includeClosed: false,
  includeHonors: false,
  includeMode: 'all',
};

@Component({
  selector: 'cse-course-packages',
  templateUrl: './course-packages.component.html',
  styleUrls: ['./course-packages.component.scss'],
})
export class CoursePackagesComponent implements OnInit, OnDestroy, OnChanges {
  // If the user already chosen a package for the relevant course, this is it.
  // tslint:disable-next-line: no-input-rename
  @Input('chosen')
  public loadingChosen: Loadable<EnrollmentPackage> | null = null;

  // All packages for the relevant course. If the course has
  // a chosen package, this list WILL contain that package.
  // tslint:disable-next-line: no-input-rename
  @Input('packages')
  public loadingPackages: Loadable<EnrollmentPackage[]> | null = null;

  // Let the parent view configure the initial filter state. This is useful in
  // the search view so that the initial state of the package filters can
  // reflect the filters that produced the list of search results.
  @Input() public initialFilters: Filters = DEFAULT_FILTERS;

  // An optional template from the parent component that allows for custom UI
  // within next to package headers
  @ContentChild('auxHeader')
  public auxHeaderRef: TemplateRef<any>;

  // An optional template from the parent component that allows for a gap that
  // is the same width as the `auxHeader` to keep the package columns aligned.
  @ContentChild('auxSpacer')
  public auxSpacerRef: TemplateRef<any>;

  // An optional template from the parent component that allows for custom UI
  // within the chosen package detail.
  @ContentChild('chosenPackageActions')
  public chosenPackageActionsRef: TemplateRef<any>;

  // An optional template from the parent component that allows for custom
  // UI within each package detail. Often this is used to expose enrollment
  // options specific to the parent view and the enrollment package.
  @ContentChild('packageActions') public packageActionsRef: TemplateRef<any>;

  @Output() public retryChosenPackage = new EventEmitter<void>();
  @Output() public retryAllPackages = new EventEmitter<void>();

  public isDesktop = this.breakpoints
    .observe(Breakpoints.Mobile)
    .pipe(map(({ matches }) => !matches));

  public filterForm = this.formBuilder.group(DEFAULT_FILTERS);

  public filtered: EnrollmentPackage[] = [];

  public seatTotal: string | null = null;

  public modeTotal: string | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private breakpoints: BreakpointObserver,
  ) {
    this.subscriptions.push(
      this.filterForm.valueChanges
        .pipe(
          map((filters: Filters): string | null => {
            let total = 0;

            if (filters.includeOpen) {
              total++;
            }

            if (filters.includeWaitlisted) {
              total++;
            }

            if (filters.includeClosed) {
              total++;
            }

            return total ? `${total} selected` : null;
          }),
        )
        .subscribe(seatTotal => (this.seatTotal = seatTotal)),
    );

    this.subscriptions.push(
      this.filterForm.valueChanges
        .pipe(
          map((filters: Filters): string | null => {
            if (filters.includeMode === 'all') {
              return null;
            } else {
              return '1 selected';
            }
          }),
        )
        .subscribe(modeTotal => (this.modeTotal = modeTotal)),
    );

    this.subscriptions.push(
      this.filterForm.valueChanges
        .pipe(
          map(filters => filterPackages(this.loadingPackages, filters)),
          startWith([]),
        )
        .subscribe(filtered => (this.filtered = filtered)),
    );
  }

  public ngOnInit() {
    console.assert(
      this.loadingChosen || this.loadingPackages,
      'cse-course-packages expects `chosen` or `packages` to be set',
    );

    this.filterForm.setValue(this.initialFilters ?? DEFAULT_FILTERS);
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.loadingPackages) {
      this.filtered = filterPackages(
        this.loadingPackages,
        this.filterForm.value,
      );
    }
  }

  public ngOnDestroy() {
    unsubscribeAll(this.subscriptions);
  }

  public trackGroup(_index: number, group: PackageGroup): string {
    if (group.kind === 'nested') {
      return `${group.lecture.type}-${group.lecture.sectionNumber}`;
    } else {
      return group.package.id;
    }
  }

  public trackNested(_index: number, group: NestedPackage): string {
    return group.id;
  }
}

const filterPackages = (
  loadingPackages: Loadable<EnrollmentPackage[]> | null,
  filters: Filters,
): EnrollmentPackage[] => {
  if (!loadingPackages || !isSuccess(loadingPackages)) {
    return [];
  }

  return loadingPackages.value.filter(applyFilters(filters));
};

const modeIs = (expect: string) => (section: { instructionMode: string }) => {
  return section.instructionMode === expect;
};

const applyFilters = (filters: Filters) => {
  return (pack: EnrollmentPackage): boolean => {
    let matchesSeats: boolean;
    if (
      (filters.includeOpen && pack.status === 'OPEN') ||
      (filters.includeWaitlisted && pack.status === 'WAITLISTED') ||
      (filters.includeClosed && pack.status === 'CLOSED')
    ) {
      matchesSeats = true;
    } else {
      matchesSeats = false;
    }

    let matchesMode: boolean;
    if (filters.includeMode === 'classroom') {
      matchesMode = pack.sections.every(modeIs('Classroom Instruction'));
    } else if (filters.includeMode === 'hybrid') {
      matchesMode = pack.sections.every(modeIs('Online (some classroom)'));
    } else {
      const isOnline = pack.sections.every(modeIs('Online Only'));
      const isAsync = pack.isAsynchronous;
      const isSync = pack.sections.every(({ classMeetings }) => {
        return (
          classMeetings
            .filter(meeting => meeting.meetingType === 'CLASS')
            .filter(meeting => {
              const missingStart = typeof meeting.meetingTimeStart !== 'number';
              const missingEnd = typeof meeting.meetingTimeEnd !== 'number';
              return missingStart && missingEnd;
            }).length === 0
        );
      });

      if (filters.includeMode === 'async') {
        matchesMode = isOnline && isAsync;
      } else if (filters.includeMode === 'sync') {
        matchesMode = isOnline && isSync;
      } else if (filters.includeMode === 'either') {
        matchesMode = isOnline;
      } else {
        matchesMode = true;
      }
    }

    let matchesHonors: boolean;
    if (filters.includeHonors) {
      matchesHonors = pack.honors !== null;
    } else {
      matchesHonors = true;
    }

    return matchesSeats && matchesMode && matchesHonors;
  };
};
