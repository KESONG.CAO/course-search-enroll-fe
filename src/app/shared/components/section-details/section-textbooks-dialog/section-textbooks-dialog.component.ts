import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EnrollmentPackage } from '@app/types/courses';

@Component({
  selector: 'cse-section-textbooks-dialog',
  templateUrl: './section-textbooks-dialog.component.html',
  styleUrls: ['./section-textbooks-dialog.component.scss'],
  preserveWhitespaces: true,
})
export class SectionTextbooksDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public pack: EnrollmentPackage) {}

  onImageError() {
    console.log('ERROR LOADING IMAGE');
  }
}
