import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Session } from '@app/types/terms';
import { toMonthDayYearMadison } from '@app/shared/date-formats';

@Component({
  selector: 'cse-session-deadlines-dialog',
  templateUrl: './session-deadlines-dialog.component.html',
  styleUrls: ['./session-deadlines-dialog.component.scss'],
})
export class SessionDeadlinesDialogComponent implements OnInit {
  public addDate: string;
  public withdrawDate: string;
  public dropDate: string;

  constructor(@Inject(MAT_DIALOG_DATA) public session: Session) {}

  ngOnInit(): void {
    this.addDate =
      this.session.addDate !== null
        ? toMonthDayYearMadison(this.session.addDate)
        : 'Not specified';
    this.withdrawDate =
      this.session.drwDate !== null
        ? toMonthDayYearMadison(this.session.drwDate)
        : 'Not specified';
    this.dropDate =
      this.session.dropDate !== null
        ? toMonthDayYearMadison(this.session.dropDate)
        : 'Not specified';
  }
}
