import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { InstructorProvidedContent } from '@app/types/courses';

@Component({
  selector: 'cse-instructor-content-dialog',
  templateUrl: './instructor-content-dialog.component.html',
  styleUrls: ['./instructor-content-dialog.component.scss'],
})
export class InstructorContentDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public content: InstructorProvidedContent,
  ) {}
}
