import { Component, Input } from '@angular/core';
import { toWeekdayMonthDayYearMadison } from '@app/shared/date-formats';
import { MeetingLocation } from '@app/types/courses';
import { CourseWithExam } from '@app/types/schedules';

interface Exam {
  day: string;
  title: string;
  start: string;
  end: string;
  sortBy: number;
  location: MeetingLocation;
}

interface ExamGroup {
  day: string;
  exams: Exam[];
}

@Component({
  selector: 'cse-exam-schedule',
  templateUrl: './exam-schedule.component.html',
  styleUrls: ['./exam-schedule.component.scss'],
})
export class ExamScheduleComponent {
  @Input() public set courses(courses: CourseWithExam[]) {
    this.groupedExams = groupExams(courses);
  }

  public groupedExams: ExamGroup[] = [];
}

const groupExams = (courses: CourseWithExam[]): ExamGroup[] => {
  const ungroupedExams = courses
    .flatMap(course => {
      const title = `${course.shortCatalog}: ${course.title}`;
      return course.package.examMeetings.map(
        (exam): Exam => ({
          day: toWeekdayMonthDayYearMadison(exam.schedule.startTimestamp),
          title,
          start: exam.schedule.startTime,
          end: exam.schedule.endTime,
          sortBy: exam.schedule.startTimestamp,
          location: exam.location,
        }),
      );
    })
    .sort((a, b) => a.sortBy - b.sortBy);

  return ungroupedExams.reduce((acc, exam) => {
    const last = acc[acc.length - 1];

    if (!last || last.day !== exam.day) {
      acc.push({ day: exam.day, exams: [exam] });
    } else {
      last.exams.push(exam);
    }

    return acc;
  }, [] as ExamGroup[]);
};
