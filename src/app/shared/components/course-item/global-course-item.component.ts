import { Component, Input, OnInit } from '@angular/core';
import { CourseLike } from '@app/core/models/course';

@Component({
  selector: 'cse-global-course-item',
  templateUrl: './global-course-item.component.html',
  styleUrls: ['./global-course-item.component.scss'],
})
export class GlobalCourseItemComponent implements OnInit {
  @Input() course: CourseLike;

  constructor() {}

  ngOnInit() {}
}
