import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

interface Data {
  headline: string;
  message: string;
}

@Component({
  selector: 'cse-keep-open-dialog',
  templateUrl: './keep-open-dialog.component.html',
  styleUrls: ['./keep-open-dialog.component.scss'],
})
export class KeepOpenDialogComponent {
  public headline: string;
  public message: string;

  constructor(@Inject(MAT_DIALOG_DATA) data: Data) {
    this.headline = data.headline;
    this.message = data.message;
  }
}
