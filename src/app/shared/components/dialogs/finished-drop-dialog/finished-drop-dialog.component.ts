import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Course } from '@app/types/courses';

export interface FinishedDropDialogData {
  failed: Array<[Course, string]>;
}

@Component({
  selector: 'cse-finished-drop-dialog',
  templateUrl: './finished-drop-dialog.component.html',
  styleUrls: ['./finished-drop-dialog.component.scss'],
})
export class FinishedDropDialogComponent {
  public failed: FinishedDropDialogData['failed'];

  constructor(@Inject(MAT_DIALOG_DATA) data: FinishedDropDialogData) {
    this.failed = data.failed;
  }
}
