import { Component } from '@angular/core';

@Component({
  selector: 'cse-failed-enroll-dialog',
  templateUrl: './failed-enroll-dialog.component.html',
  styleUrls: ['./failed-enroll-dialog.component.scss'],
})
export class FailedEnrollDialogComponent {}
