# Schedule component ideas

## Reasoning

There are a few places in the app that will have to display a week's worth of
classes/exams to users:

- In the my-courses view students want to see how their enrolled class meetings
  are arranged within a single week.
- In the scheduler view students want to generate possible schedules based on
  the courses in their cart and evaluate schedules in a visual way.

I don't think it makes sense to use an off-the-shelf calendar component for this
task because the data that's being displayed isn't calendar data in the typical
sense. When displaying a class schedule, it doesn't matter what month or
calendar year is being displayed, the component only has to show a single week.
Also off-the-shelf calendar components risk coming with a lot of extra
functionality that will bloat the bundle size, complicate our customizations, or
both.

I think the task is specific enough that we can create a custom component to do
_exactly_ what we need.

## Content projection

Angular has a feature called 'content projection' that allows a parent component
to pass HTML elements into a child component for the child component to render.
This is a good way of giving the parent component some control over what part of
the child component looks and behaves like.

I think content projection will be a good way to control what the HTML inside
each meeting will look like. It lets the schedule component not worry about how
the text inside each meeting is formatted, what event inside the meeting need to
be handled, etc.

Using content projection, the schedule component can be used like this:

```html
<cse-schedule>
  <cse-meeting day="mon" start="50400" duration="4500">
    <p>BIOLOGY 151: LEC 001</p>
    <button (click)="showInfo()">More info</button>
  </cse-meeting>

  <cse-meeting day="wed" start="50400" duration="4500">
    <p>BIOLOGY 151: LEC 001</p>
    <button (click)="showInfo()">More info</button>
  </cse-meeting>

  <cse-meeting day="thur" start="50400" duration="4500">
    <p>COMP SCI 200: LEC 002</p>
    <button (click)="showInfo()">More info</button>
  </cse-meeting>
</cse-schedule>
```

This allows the schedule component to focus just on positioning the meetings
without having to worry about each meeting component's content or events.

Some docs about content projection that may be useful:

- [An overview of the content projection APIs](https://blog.angular-university.io/angular-ng-content/)
- [Angular's `ContentChildren` decorator](https://angular.io/api/core/ContentChildren)

### A possible strategy for position meeting components from the scheduler component

Even though each meeting component has its own content, the meeting components
still need to be positioned by the schedule component.

I think this could be done in the schedule component by getting a list of all
the `cse-meeting` components using Angular's `ContentChildren` decorator. Then
the schedule component loop over all the meeting components and compute the
vertical position, horizontal position, and height based on that meeting's day,
start time, and duration. These computed values could then be used to style the
DOM element provided by the `ContentChildren` query.

There still could be problems with this approach but I think it could be a
promising place to start. If this isn't a realistic or useful way to use these
APIs, we can always find another strategy.

## Meeting components

Each meeting component inside the schedule component will have a few required
attributes.

### `day`

One of the following abbreviations for a single day of the week:

| Day       | Abbreviation |
| --------- | ------------ |
| Monday    | `mon`        |
| Tuesday   | `tue`        |
| Wednesday | `wed`        |
| Thursday  | `thu`        |
| Friday    | `fri`        |
| Saturday  | `sat`        |
| Sunday    | `sun`        |

#### `start`

The number of _seconds_ after midnight when the event begins.

#### `duration`

The length of the event in _seconds_.

## Design considerations

### The schedule should be responsive on reasonable screen sizes

If the user makes their screen more narrow or if the app layout needs the
schedule to be shorter, the schedule component shouldn't need to recalculate the
positions of the meeting elements. I would recommend using CSS percentages to
position meetings elements so that all of the layout adjustments can be done by
the browser's own CSS engine.

### The height of an meeting element should not be impacted by the contents inside that element

If a 5 minute meeting has a very long title that wraps accross multiple lines,
it's more important for now that the meeting element stay the correct size with
regard to the height of the schedule component.

This decision is motivated by poor experiences with past enrollment tools. UW
had a prior enrollment tool that could display a schedule of classes but each
meeting in the calendar would grow to accommodate the text inside the meeting
element. This made it very difficult to judge whether a particular lecture or
section took a long time in the day or just had a lot of text to show.

## Development roadmap

With a complex component like this, we want to start simple to prove the basics
are working before adding more features.

### First phase goals

- Get the `cse-scheduler` component rendering _something_ in the scheduler view.
- Create the `cse-meeting` component and hard-code some dummy values for each
  meeting.

### Second phase goals

- Get the `cse-scheduler` to loop over any `cse-meeting` components and
  console.log each meeting's day, start time, and duration as taken from the
  `cse-meeting` component's attributes.
- For each meeting, use it's start time and duration to calculate the meeting's
  vertical height as a percentage of how much of a full day the meeting takes
  up. Also calculate the meeting's vertical offset to represent the meeting's
  starting time. Assume the height of the `cse-scheduler` components reprensents
  a full 24 hour day to make the math easier for now. Just log these numbers to
  the console for now.

### Third phase goals

- Apply the numbers calculated in the second phase to the `cse-meeting`
  component from within the `cse-scheduler` component so that the `cse-meeting`
  components are correctly positioned within the `cse-scheduler` component. It's
  okay for now if meetings that are on the same day and take place at the same
  time are visually overlapping.

### Fourth phase

- _To be determined_
