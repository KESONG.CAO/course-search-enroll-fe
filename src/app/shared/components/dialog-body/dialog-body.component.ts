import { Component } from '@angular/core';

@Component({
  selector: 'cse-dialog-body',
  template: '<ng-content></ng-content>',
  styles: [
    `
      :host {
        display: block;
        flex: 1;
      }
    `,
  ],
})
export class DialogBodyComponent {}
