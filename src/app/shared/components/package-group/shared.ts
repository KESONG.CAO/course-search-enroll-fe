import { RawSection, RawMeeting } from '@app/types/courses';
import { toHourMinuteMeridianMadison } from '@app/shared/date-formats';
import { RawSchedulerMeeting } from '@app/types/schedules';

export const NOT_REAL_BUILDING = ['PENDING', 'ONLINE', 'OFF CAMPUS'];

type Meeting =
  | { dayTime: string; session: string | null }
  | { dayTime: null; session: string };

export interface Columns {
  catalogRef: string;
  topic: string | null;
  session: string;
  meetings: Meeting[];
  locations: (string | null)[];
  instructors: string[];
  reservedSection: boolean;
  honors: string | null;
  commB: boolean;
}

const RESERVED_ATTR_CODES = ['SVCL', 'TEXT'];

export const isReservedSection = (sec: RawSection): boolean => {
  return (
    !!sec.classAttributes.length &&
    RESERVED_ATTR_CODES.indexOf(sec.classAttributes[0].attributeCode) === -1
  );
};

/**
 * - If there are no meetings, show session begin/end date
 * - If there are meetings:
 *   - Show the topic if the section has a topic
 *   - Show the meeting days if applicable
 *   - Show session begin/end date if the meeting has no meeting
 *     days OR if the session code is NOT 'A1'
 */
export const toMeeting = (session: string, sec: RawSection, m: RawMeeting) => {
  if (m.meetingDays && m.meetingTimeStart && m.meetingTimeEnd) {
    return {
      session: sec.sessionCode !== 'A1' ? session : null,
      dayTime: `${m.meetingDays} ${toHourMinuteMeridianMadison(
        m.meetingTimeStart,
      )} - ${toHourMinuteMeridianMadison(m.meetingTimeEnd)}`,
    };
  } else if (m.meetingDays && m.meetingTimeStart) {
    return {
      session: sec.sessionCode !== 'A1' ? session : null,
      dayTime: `${m.meetingDays} ${toHourMinuteMeridianMadison(
        m.meetingTimeStart,
      )}`,
    };
  } else if (m.meetingDays) {
    return {
      session: sec.sessionCode !== 'A1' ? session : null,
      dayTime: `${m.meetingDays}`,
    };
  } else {
    return { session, dayTime: null };
  }
};

/**
 * Transfrom a RawMeeting into an array of scheduler-compatible meeting objects.
 * Skip if:
 * - Either of meetingTimeStart and meetingTimeEnd is null (missing)
 * - meetingType is "EXAM"
 *
 * Convert meetingTimeStart and meetingTimeEnd from Unix milliseconds to seconds
 * after midnight.
 *
 * For each day in meetingDaysList, create a SchedulerMeeting object
 * consisting of {day, start, end} fields.
 */
export const toSchedulerMeeting = (
  m: RawMeeting,
): RawSchedulerMeeting[] | undefined => {
  if (
    m.meetingTimeStart === null ||
    m.meetingTimeEnd === null ||
    m.meetingType === 'EXAM'
  ) {
    return; // skip, will return undefined
  }

  const timeZoneDifference = 21600; // CST is UTC-6, in seconds
  const start = m.meetingTimeStart / 1000 - timeZoneDifference;
  const duration = (m.meetingTimeEnd - m.meetingTimeStart) / 1000;

  const s: RawSchedulerMeeting[] = m.meetingDaysList.map(dayOfTheWeek => {
    const day = dayOfTheWeek.slice(0, 3).toLowerCase();
    return { day, start, duration };
  });
  return s;
};

/**
 * Transfrom a RawMeeting array into an array of SchedulerMeeting objects.
 * Skipped meetings (with an undefined value) will be filtered.
 */
export const toSchedulerMeetingArray = (
  r: RawMeeting[],
): RawSchedulerMeeting[] => {
  return r
    .flatMap(m => toSchedulerMeeting(m))
    .filter((x): x is RawSchedulerMeeting => x !== undefined);
};

export const toLocation = (m: RawMeeting) => {
  if (
    m.building &&
    NOT_REAL_BUILDING.includes(m.building.buildingName) === false
  ) {
    return m.room
      ? `${m.room} ${m.building.buildingName}`
      : m.building.buildingName;
  } else if (
    m.building &&
    (m.building as { latitude?: number }).latitude !== undefined
  ) {
    // Not exactly sure what this check achieves but it's
    // copied from the AngularJS template - Isaac
    return `${m.building.buildingName}`;
  } else {
    return null;
  }
};

export const processMeetings = (session: string, sec: RawSection) => {
  return sec.classMeetings
    .filter(m => m.meetingType === 'CLASS')
    .map(m => ({
      meeting: toMeeting(session, sec, m),
      location: toLocation(m),
    }))
    .filter((next, index, all) => {
      return !all.slice(0, index).some(other => {
        return JSON.stringify(next) === JSON.stringify(other);
      });
    });
};

export const formatHonors = (raw: RawSection['honors']): string | null => {
  switch (raw) {
    case 'HONORS_ONLY':
      return 'Honors only';
    case 'HONORS_LEVEL':
      return 'Accelerated honors';
    case 'INSTRUCTOR_APPROVED':
      return 'Honors optional';
    default:
      return raw;
  }
};
