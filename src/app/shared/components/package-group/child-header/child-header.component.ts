import { Component, Input, OnInit } from '@angular/core';
import { NestedPackage } from '@app/core/models/packageGroup';
import { Observable } from 'rxjs';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Breakpoints } from '@app/shared/breakpoints';
import { map } from 'rxjs/operators';
import { titlecase } from '@app/core/utils';
import {
  Columns,
  isReservedSection,
  processMeetings,
  formatHonors,
} from '../shared';
import { EnrollmentPackage } from '@app/types/courses';
import { toMonthDayMadison } from '@app/shared/date-formats';

@Component({
  selector: 'cse-child-header',
  templateUrl: './child-header.component.html',
  styleUrls: ['./child-header.component.scss'],
})
export class ChildHeaderComponent implements OnInit {
  @Input() public package: NestedPackage;
  @Input() public isSelected: boolean;

  public isDesktop: Observable<boolean>;
  public rows: Columns[];
  public seats: EnrollmentPackage['status'];

  constructor(breakpoints: BreakpointObserver) {
    this.isDesktop = breakpoints
      .observe(Breakpoints.Mobile)
      .pipe(map(({ matches }) => !matches));
  }

  ngOnInit() {
    this.seats = this.package.status;
    this.rows = this.package.sectionsWithoutLecture.map(
      (sec): Columns => {
        const session = `${toMonthDayMadison(
          sec.startDate,
        )} - ${toMonthDayMadison(sec.endDate)} (${sec.sessionCode})`;

        // Simplify meeting structure, remove duplicate meeting entries
        const dedupedMeetingAndLocation = processMeetings(session, sec);

        return {
          catalogRef: `${sec.type} ${sec.sectionNumber}`,
          topic: sec.topic ? sec.topic.shortDescription : null,
          session,
          meetings: dedupedMeetingAndLocation.map(({ meeting }) => meeting),
          locations: dedupedMeetingAndLocation.map(({ location }) => location),
          instructors: sec.instructors.map(i => {
            return `${titlecase(i.name.first)} ${titlecase(i.name.last)}`;
          }),
          reservedSection: isReservedSection(sec),
          honors: formatHonors(sec.honors),
          commB: sec.comB,
        };
      },
    );
  }
}
