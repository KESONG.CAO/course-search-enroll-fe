import { Component, OnInit, Input } from '@angular/core';
import { EnrollmentPackage, RawSection } from '@app/types/courses';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Breakpoints } from '@app/shared/breakpoints';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {
  Columns,
  formatHonors,
  isReservedSection,
  processMeetings,
} from '../shared';
import { titlecase } from '@app/core/utils';
import { toMonthDayMadison } from '@app/shared/date-formats';

@Component({
  selector: 'cse-parent-header',
  templateUrl: './parent-header.component.html',
  styleUrls: ['./parent-header.component.scss'],
})
export class ParentHeaderComponent implements OnInit {
  @Input() public lecture?: RawSection;
  @Input() public package?: EnrollmentPackage;
  @Input() public isSelected: boolean;

  public sections: RawSection[];
  public isDesktop: Observable<boolean>;
  public rows: Columns[];
  public seats: EnrollmentPackage['status'] | null;

  constructor(breakpoints: BreakpointObserver) {
    this.isDesktop = breakpoints
      .observe(Breakpoints.Mobile)
      .pipe(map(({ matches }) => !matches));
  }

  ngOnInit() {
    console.assert(
      (this.lecture && !this.package) || (!this.lecture && this.package),
      'component expects `lecture` OR `package` to be set but NOT BOTH',
    );

    if (this.lecture) {
      this.sections = [this.lecture];
    } else if (this.package) {
      this.sections = this.package.sections;
    }

    console.assert(
      Array.isArray(this.sections) && this.sections.length > 0,
      'found an enrollment package with 0 sections',
    );

    this.seats = this.package ? this.package.status : null;

    this.rows = this.sections.map(
      (sec): Columns => {
        const session = `${toMonthDayMadison(
          sec.startDate,
        )} - ${toMonthDayMadison(sec.endDate)} (${sec.sessionCode})`;

        // Simplify meeting structure, remove duplicate meeting entries
        const dedupedMeetingAndLocation = processMeetings(session, sec);

        return {
          catalogRef: `${sec.type} ${sec.sectionNumber}`,
          topic: sec.topic ? sec.topic.shortDescription : null,
          session,
          meetings: dedupedMeetingAndLocation.map(({ meeting }) => meeting),
          locations: dedupedMeetingAndLocation.map(({ location }) => location),
          instructors: sec.instructors.map(i => {
            return `${titlecase(i.name.first)} ${titlecase(i.name.last)}`;
          }),
          reservedSection: isReservedSection(sec),
          honors: formatHonors(sec.honors),
          commB: sec.comB,
        };
      },
    );
  }
}
