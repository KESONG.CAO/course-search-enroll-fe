import { Component } from '@angular/core';

@Component({
  selector: 'cse-dialog-actions',
  template: '<ng-content></ng-content>',
  styles: [
    `
      :host {
        display: flex;
        flex: 1;
      }
    `,
  ],
})
export class DialogActionsComponent {}
