import { Component, Input } from '@angular/core';

@Component({
  selector: 'cse-course-message',
  templateUrl: './course-message.component.html',
  styleUrls: ['./course-message.component.scss'],
})
export class CourseMessageComponent {
  @Input() public icon?: string;
  @Input() public color?: string;
}
