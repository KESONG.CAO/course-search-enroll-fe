import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

const KNOWN_EMOTES = [
  // 'dead',
  // 'grimace',
  'happy',
  // 'meh',
  'sad',
  'shrug',
  // 'whistle',
] as const;

type Emote = typeof KNOWN_EMOTES[number];

const EMOTE_ALTS: Record<Emote, string> = {
  // dead: 'Bucky Badger knocked-out',
  // grimace: 'Bucky Badger grimacing',
  happy: 'Bucky Badger smiling',
  // meh: 'Bucky Badger with a skewed frown',
  sad: 'Bucky Badger frowning with sad eyes',
  shrug: 'Bucky Badger shrugging',
  // whistle: 'Bucky Badger whistling',
};

const EMOTE_IMGS: Record<Emote, { default: string }> = {
  // dead: require('!!raw-loader!../../../../assets/img/bucky-dead.svg'),
  // grimace: require('!!raw-loader!../../../../assets/img/bucky-grimace.svg'),
  happy: require('!!raw-loader!../../../../assets/img/bucky-happy.svg'),
  // meh: require('!!raw-loader!../../../../assets/img/bucky-meh.svg'),
  sad: require('!!raw-loader!../../../../assets/img/bucky-sad.svg'),
  shrug: require('!!raw-loader!../../../../assets/img/bucky-shrug.svg'),
  // whistle: require('!!raw-loader!../../../../assets/img/bucky-whistle.svg'),
};

@Component({
  selector: 'cse-bucky-emote',
  template: `
    <div class="emote" [innerHTML]="img"></div>
    <div class="message"><ng-content select="[emoteMessage]"></ng-content></div>
    <div class="action"><ng-content select="[emoteAction]"></ng-content></div>
  `,
  styles: [
    `
      :host {
        display: block;
      }

      .emote::ng-deep svg {
        height: 100px;
        display: block;
        margin-left: auto;
        margin-right: auto;
      }

      .message {
        margin-left: auto;
        margin-right: auto;
        text-align: center;
        max-width: 100ex;
      }

      .message:empty {
        display: none;
      }

      .action {
        text-align: center;
      }

      .action:empty {
        display: none;
      }
    `,
  ],
})
export class BuckyEmoteComponent implements OnInit {
  @Input() public emote: Emote;
  public alt: string;
  public img: SafeHtml;

  constructor(private sanitizer: DomSanitizer) {}

  ngOnInit(): void {
    if (!KNOWN_EMOTES.includes(this.emote)) {
      this.emote = 'shrug';
    }

    this.alt = EMOTE_ALTS[this.emote];
    this.img = this.sanitizer.bypassSecurityTrustHtml(
      EMOTE_IMGS[this.emote].default,
    );
  }
}
