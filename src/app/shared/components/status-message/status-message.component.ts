import { Component, Input } from '@angular/core';

@Component({
  selector: 'cse-status-message',
  templateUrl: './status-message.component.html',
  styleUrls: ['./status-message.component.scss'],
})
export class StatusMessageComponent {
  @Input() public status: 'okay' | 'warning' | 'error';
  @Input() public headline: string;
}
