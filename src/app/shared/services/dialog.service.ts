import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { map } from 'rxjs/operators';
import { ComponentType } from '@angular/cdk/portal';
import { Course } from '@app/types/courses';
import { GenericSmallDialogComponent } from '../components/dialogs/generic-small-dialog/generic-small-dialog.component';
import { KeepOpenDialogComponent } from '../components/dialogs/keep-open-dialog/keep-open-dialog.component';

interface DialogOptions {
  headline: string;
  reasoning?: string[];
  courses?: Course[];
  affirmative?: string;
  negative?: string;
  moreInfo?: { label: string; href: string };
}

@Injectable({ providedIn: 'root' })
export class DialogService {
  constructor(private dialog: MatDialog) {}

  confirm(options: DialogOptions): Promise<boolean> {
    return this.dialog
      .open(GenericSmallDialogComponent, {
        data: options,
        panelClass: 'cse-small-dialog',
      })
      .afterClosed()
      .pipe(map(choice => choice === true))
      .toPromise();
  }

  acknowledge(options: DialogOptions): Promise<void> {
    return this.dialog
      .open(GenericSmallDialogComponent, {
        data: { ...options, hideNegativeAction: true },
        panelClass: 'cse-small-dialog',
      })
      .afterClosed()
      .toPromise();
  }

  small<R>(component: ComponentType<any>, data?: any): Promise<R | undefined> {
    return this.dialog
      .open(component, { data, panelClass: 'cse-small-dialog' })
      .afterClosed()
      .toPromise();
  }

  cannotClose<R>(
    component: ComponentType<any>,
    data?: any,
  ): Promise<R | undefined> {
    return this.dialog
      .open(component, {
        data,
        panelClass: 'cse-small-dialog',
        disableClose: true,
      })
      .afterClosed()
      .toPromise();
  }

  large<R>(component: ComponentType<any>, data?: any): Promise<R | undefined> {
    return this.dialog
      .open(component, { data, panelClass: 'cse-large-dialog' })
      .afterClosed()
      .toPromise();
  }

  keepOpen(headline: string, message: string): () => void {
    const ref = this.dialog.open(KeepOpenDialogComponent, {
      data: { headline, message },
      panelClass: 'cse-large-dialog',
      disableClose: true,
    });

    return () => {
      ref.close();
    };
  }
}
