export const Breakpoints = {
  Mobile: `(max-width: 1279px)`,
  Desktop: `(min-width: 1280px)`,
};
