import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'get', pure: true })
export class GetPipe implements PipeTransform {
  transform(thing: object, key: string): unknown {
    return thing[key];
  }
}
