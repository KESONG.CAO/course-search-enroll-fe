import { Pipe, PipeTransform } from '@angular/core';
import { MeetingLocation } from '@app/types/courses';
import * as s from '@app/types/schema';

@Pipe({ name: 'location' })
export class LocationPipe implements PipeTransform {
  transform(location: MeetingLocation | null): string {
    if (location === null) {
      return 'No location provided';
    }

    switch (location.type) {
      case 'classroom':
        if (location.room) {
          return `${location.room} ${location.building}`;
        } else {
          return location.building;
        }
      case 'online':
        return 'Online';
      default:
        if (s.object({ building: s.string }).matches(location)) {
          return location.building;
        } else {
          return 'No location provided';
        }
    }
  }
}
