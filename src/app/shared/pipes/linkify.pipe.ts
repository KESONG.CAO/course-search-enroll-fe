import { Pipe, PipeTransform } from '@angular/core';
import linkify from 'linkifyjs/html';

@Pipe({ name: 'linkify' })
export class LinkifyPipe implements PipeTransform {
  transform(value: string): string {
    return linkify(value);
  }
}
