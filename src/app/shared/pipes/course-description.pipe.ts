import { Pipe, PipeTransform } from '@angular/core';
import { CourseBase, CourseLike } from '@app/core/models/course';

@Pipe({ name: 'courseDescription', pure: true })
export class CourseDescriptionPipe implements PipeTransform {
  transform(arg: CourseBase | CourseLike) {
    if (arg instanceof CourseLike) {
      return arg.catalog();
    } else if (arg.studentEnrollmentStatus === 'Transfer') {
      return `${arg.subjectDescription} ${arg.catalogNumber || ''}`;
    } else {
      if (arg.hasOwnProperty('subject')) {
        return `${(arg as any).subject.shortDescription} ${arg.catalogNumber}`;
      } else if (arg.hasOwnProperty('subjectDescription')) {
        return `${(arg as any).subjectDescription} ${arg.catalogNumber}`;
      } else if (arg.hasOwnProperty('shortCatalog')) {
        return `${(arg as any).shortCatalog}`;
      } else {
        return `TODO ${arg.catalogNumber}`;
      }
    }
  }
}
