import { Pipe, PipeTransform } from '@angular/core';
import { TermCodeOrZero, WithEra, Era } from '@app/types/terms';

@Pipe({ name: 'getTermDescription' })
export class GetTermDescriptionPipe implements PipeTransform {
  transform(termCode: TermCodeOrZero | string | null): string {
    if (termCode === null) {
      return '';
    }

    termCode =
      typeof termCode === 'string'
        ? TermCodeOrZero.decodeOrThrow(termCode)
        : termCode;
    return TermCodeOrZero.describe(termCode);
  }
}

@Pipe({ name: 'encodeTerm' })
export class EncodeTermPipe implements PipeTransform {
  transform(termCode: TermCodeOrZero): string {
    return TermCodeOrZero.encode(termCode);
  }
}

@Pipe({ name: 'isInactive', pure: true })
export class IsInactivePipe implements PipeTransform {
  transform(termCode: WithEra): boolean {
    return termCode.era !== Era.Active;
  }
}

@Pipe({ name: 'isActive', pure: true })
export class IsActivePipe implements PipeTransform {
  transform(termCode: WithEra): boolean {
    return termCode.era === Era.Active;
  }
}

@Pipe({ name: 'isPast', pure: true })
export class IsPastPipe implements PipeTransform {
  transform(termCode: WithEra): boolean {
    return termCode.era === Era.Past;
  }
}

@Pipe({ name: 'isFuture', pure: true })
export class IsFuturePipe implements PipeTransform {
  transform(termCode: WithEra): boolean {
    return termCode.era === Era.Future;
  }
}
