import { Pipe, PipeTransform } from '@angular/core';
import { CourseLike } from '@app/core/models/course';

@Pipe({ name: 'courseTitle', pure: true })
export class CourseTitlePipe implements PipeTransform {
  transform(arg: CourseLike) {
    return arg.title;
  }
}
