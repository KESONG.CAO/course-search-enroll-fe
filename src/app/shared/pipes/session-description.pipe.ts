import { Pipe, PipeTransform } from '@angular/core';
import { Session } from '@app/types/terms';
import { toMonthDayMadison, toMonthDayYearMadison } from '../date-formats';

@Pipe({ name: 'sessionDescription' })
export class SessionDescriptionPipe implements PipeTransform {
  transform(session: Session): string {
    const begin = toMonthDayMadison(session.beginDate);
    const end = toMonthDayYearMadison(session.endDate);
    const desc = session.description;
    return `${begin} - ${end} (${desc})`;
  }
}
