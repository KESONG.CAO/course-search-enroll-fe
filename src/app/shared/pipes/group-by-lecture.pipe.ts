import { Pipe, PipeTransform } from '@angular/core';
import {
  PackageGroup,
  groupPackages,
  groupWithoutNesting,
} from '@app/core/models/packageGroup';
import { EnrollmentPackage } from '@app/types/courses';

@Pipe({ name: 'groupByLecture', pure: true })
export class GroupByLecturePipe implements PipeTransform {
  transform(packages: EnrollmentPackage[]): PackageGroup[] {
    return groupPackages(packages);
  }
}

@Pipe({ name: 'groupOneWithoutNesting', pure: true })
export class GroupOneWithoutNesting implements PipeTransform {
  transform(pack: EnrollmentPackage): PackageGroup {
    return groupWithoutNesting([pack])[0];
  }
}
