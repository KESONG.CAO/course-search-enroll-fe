import { Pipe, PipeTransform } from '@angular/core';
import * as Schema from '@app/types/schema';

const schema = Schema.object({ mode: Schema.string });

@Pipe({ name: 'isUnloaded' })
export class IsUnloadedPipe implements PipeTransform {
  transform(loadable: unknown) {
    if (schema.matches(loadable)) {
      return loadable.mode === 'unloaded' ? loadable : false;
    } else {
      return false;
    }
  }
}

@Pipe({ name: 'isLoading' })
export class IsLoadingPipe implements PipeTransform {
  transform(loadable: unknown) {
    if (schema.matches(loadable)) {
      return loadable.mode === 'loading' ? loadable : false;
    } else {
      return false;
    }
  }
}

@Pipe({ name: 'isError' })
export class IsErrorPipe implements PipeTransform {
  transform(loadable: unknown) {
    if (schema.matches(loadable)) {
      return loadable.mode === 'error' ? loadable : false;
    } else {
      return false;
    }
  }
}

@Pipe({ name: 'isLoaded' })
export class IsLoadedPipe implements PipeTransform {
  transform(loadable: unknown) {
    if (schema.matches(loadable)) {
      return loadable.mode === 'loaded' ? loadable : false;
    } else {
      return false;
    }
  }
}
