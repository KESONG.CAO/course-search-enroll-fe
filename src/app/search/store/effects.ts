import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as globalActions from '@app/actions';
import * as actions from './actions';
import {
  map,
  withLatestFrom,
  mergeMap,
  catchError,
  filter,
  switchMap,
  tap,
} from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { State, FORM_ID, FormState } from '@search/store/state';
import * as globalSelectors from '@app/selectors';
import * as searchSelectors from '@search/store/selectors';
import {
  EnrollmentPackage,
  RawEnrollmentPackage,
  SearchResult,
} from '@app/types/courses';
import { Observable, of, pipe } from 'rxjs';
import { TermCode, TermCodeOrZero, Session, ZERO } from '@app/types/terms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { unsubscribeAll, notNull } from '@app/core/utils';
import { stateToElasticQuery } from '@search/filters';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { fromParams, toParams } from '@search/query-params';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Breakpoints } from '@app/shared/breakpoints';
import { SetValueAction } from 'ngrx-forms';
import { isSuccess, Success } from 'loadable.ts';
import { ApiService } from '@app/services/api.service';
import * as s from '@app/types/schema';

const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable()
export class SearchEffects {
  private isMobile: Observable<boolean> = this.breakpoints
    .observe(Breakpoints.Mobile)
    .pipe(map(({ matches }) => matches));

  private activeTermSet: Observable<TermCode[]> = this.store$
    .select(globalSelectors.terms.getActiveTermCodes)
    .pipe(
      filter(isSuccess),
      map(terms => terms.value),
    );

  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private store$: Store<State>,
    private snackbar: MatSnackBar,
    private router: Router,
    private active: ActivatedRoute,
    private breakpoints: BreakpointObserver,
    private api: ApiService,
  ) {}

  @Effect()
  LifecycleInit$ = this.actions$.pipe(
    ofType(actions.lifecycle.init),

    // If the search view state was already initialized (likely because the user
    // had already opened this view during this session), don't initialize the
    // view again. If a view is initialized again, any state the user had
    // changed in that view will be lost which kind of defeats the purpose of a
    // single-page-app.
    withLatestFrom(this.store$.select(searchSelectors.getWasInitialized)),
    filter(([_action, wasInitialized]) => wasInitialized !== 'done'),

    mergeMap(() =>
      this.store$.select(globalSelectors.terms.getAll).pipe(filter(isSuccess)),
    ),
    withLatestFrom(
      this.active.queryParams,
      this.store$.select(searchSelectors.getResults),
      this.store$
        .select(globalSelectors.currentTermCode.get)
        .pipe(map(current => (isSuccess(current) ? current.value : undefined))),
    ),
    map(([terms, params, results, currentTermCode]) => {
      const defaultActiveTerm = TermCode.findFirst(terms.value.activeTermCodes);

      if (results !== 'unloaded') {
        // If some results state has already been loaded, preserve the results
        // state and any existing search state.
        return actions.lifecycle.success({
          defaultActiveTerm,
          formDerrivedFromParams: null,
        });
      } else {
        const formDerrivedFromParams = fromParams(
          terms.value,
          params,
          currentTermCode,
        );
        return actions.lifecycle.success({
          defaultActiveTerm,
          formDerrivedFromParams,
        });
      }
    }),
  );

  @Effect({ dispatch: false })
  SyncQueryParams$ = this.actions$.pipe(
    ofType(
      // Don't trigger on newSearchWithOrderBy because that action is always
      // transformed info a search.withFilters.
      actions.search.withCurrentFilters,
      actions.search.withFilters,
      actions.filters.reset,
    ),
    withLatestFrom(
      this.activeTermSet,
      this.store$.select(searchSelectors.getFormValue),
    ),
    map(([_, activeTermCodes, form]) => {
      this.router.navigate(['/search'], {
        queryParams: toParams(activeTermCodes, form),
      });
    }),
  );

  @Effect()
  search$ = this.actions$.pipe(
    ofType(actions.search.withCurrentFilters, actions.search.withFilters),
    map(() => actions.page.next()),
  );

  @Effect()
  nextPage$ = this.actions$.pipe(
    ofType(actions.page.next, actions.page.retry),
    withLatestFrom(this.store$.select(searchSelectors.getCurrentFilters)),
    map(([, filters]) => filters),
    filter(notNull),
    withLatestFrom(this.store$.select(searchSelectors.getResults)),
    filter(([, res]) => {
      // Only proceed if the next page is not already loaded.
      return (
        res === 'unloaded' ||
        (res.viewing.state === 'next' && res.next !== 'error')
      );
    }),
    map(([filters, res]) =>
      stateToElasticQuery(
        filters,
        res === 'unloaded' ? 1 : res.pages.length + 1,
      ),
    ),
    switchMap(query =>
      this.api.postSearch(query).pipe(
        withLatestFrom(this.activeTermSet),
        map(([response, active]) => {
          return actions.page.done({
            found: response.found,
            hits: response.hits.map(SearchResult.fromCurry(active)),
          });
        }),
        catchError(() => of(actions.page.error())),
      ),
    ),
  );

  @Effect()
  NewSearchWithOrderBy$ = this.actions$.pipe(
    ofType(actions.search.withOrderBy),
    withLatestFrom(this.store$.select(searchSelectors.getCurrentFilters)),
    map(([{ orderBy }, currentFilters]) => {
      if (currentFilters) {
        return actions.search.withFilters({
          filters: currentFilters,
          orderBy,
        });
      } else {
        return actions.util.noop();
      }
    }),
  );

  @Effect()
  LoadSections$ = this.actions$.pipe(
    ofType(actions.packages.show),
    withLatestFrom(
      this.store$
        .select(searchSelectors.getSelectedIndexOrNull)
        .pipe(filter(notNull)),
      this.store$
        .select(searchSelectors.getSelectedCourse)
        .pipe(filter(notNull)),
      this.store$.select(searchSelectors.details),
      this.store$.select(searchSelectors.chosenTermCode),
      this.store$
        .select(searchSelectors.chosenTermCode)
        .pipe(
          mergeMap(termCodeOrNull =>
            this.store$.select(
              globalSelectors.terms.getSessionsForTerm,
              termCodeOrNull ?? ZERO,
            ),
          ),
        ),
    ),
    filter(([_action, _forIndex, _course, details, _termCode, sessions]) => {
      return details !== null && isSuccess(sessions);
    }),
    mergeMap(([_action, forIndex, course, _details, termCode, sessions]) => {
      const { subject, courseId } = course;
      const encoded = TermCodeOrZero.encode(termCode ?? ZERO);
      const url = `/api/search/v1/enrollmentPackages/${encoded}/${subject.code}/${courseId}`;
      return this.http.get<RawEnrollmentPackage[]>(url, HTTP_OPTIONS).pipe(
        map(packages =>
          packages.map(
            EnrollmentPackage.fromCurry((sessions as Success<Session[]>).value),
          ),
        ),
        map(packages => actions.packages.ready({ forIndex, packages })),
        catchError(() => of(actions.packages.error({ forIndex }))),
      );
    }),
  );

  @Effect({ dispatch: false })
  AddToPlan$ = this.actions$.pipe(
    ofType(actions.course.addToPlan),
    mergeMap(({ plan, termCode, course }) => {
      const message = `${course.shortCatalog} was added to your ${plan.name} degree plan`;
      const url = `/api/planner/v1/degreePlan/${plan.roadmapId}/courses`;
      const payload = {
        subjectCode: course.subject.code,
        courseId: course.courseId,
        termCode: TermCode.encode(termCode),
      };
      return this.http.post<void>(url, payload, HTTP_OPTIONS).pipe(
        this.navigationSnackbar(message, 'View plan', ['degree-planner'], {
          // This is a signal to the degree-planner view that it
          // should show the appropriate plan on initial load
          showPlan: plan.roadmapId,
        }),
        catchError(err => {
          const schema = s.object({ error: s.object({ message: s.string }) });
          const message = schema.matches(err)
            ? err.error.message
            : `Error adding ${course.title} to ${plan.name}`;
          return this.snackbar.open(message).afterDismissed();
        }),
      );
    }),
  );

  @Effect()
  FilterChange$ = this.actions$.pipe(
    ofType('ngrx/forms/SET_VALUE'),
    filter((action: SetValueAction<any>) => {
      return action.controlId !== `${FORM_ID}.keywords`;
    }),
    withLatestFrom(this.isMobile),
    filter(([_action, isMobile]) => !isMobile),
    map(() => actions.search.withCurrentFilters()),
  );

  @Effect()
  syncGlobalCurrentTerm$ = this.actions$.pipe(
    ofType('ngrx/forms/SET_VALUE'),
    filter((action: SetValueAction<any>) => {
      return action.controlId === `${FORM_ID}.term`;
    }),
    map(action => action.value as FormState['term']),
    filter(notNull),
    map(boxed =>
      globalActions.currentTermCode.set({ currentTermCode: boxed.value }),
    ),
  );

  private navigationSnackbar(
    message: string,
    action: string,
    commands: any[],
    state: any,
  ) {
    return pipe(
      tap(() => {
        const ref = this.snackbar.open(message, action);
        const subs = [
          ref.afterDismissed().subscribe(() => unsubscribeAll(subs)),
          ref.onAction().subscribe(() => {
            this.router.navigate(commands, { state });
          }),
        ];
      }),
    );
  }
}
