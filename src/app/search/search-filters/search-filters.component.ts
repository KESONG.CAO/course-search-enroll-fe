import { Component, Inject, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  State,
  FORM_ID,
  MIN_CREDIT_VALUE,
  MAX_CREDIT_VALUE,
} from '@search/store/state';
import { Observable, ReplaySubject, Subscription } from 'rxjs';
import * as globalSelectors from '@app/selectors';
import * as searchSelectors from '@search/store/selectors';
import {
  TermCodeOrZero,
  Zero,
  Subject,
  Session,
  SpecialGroups,
  SpecialGroup,
  TermCode,
  ZERO,
} from '@app/types/terms';
import * as actions from '@search/store/actions';
import { ResetAction, SetValueAction } from 'ngrx-forms';
import { fuzzy } from '@search/fuzzy-search';
import { FormControl } from '@angular/forms';
import {
  withLatestFrom,
  distinctUntilChanged,
  map,
  filter,
  mergeMap,
  first,
} from 'rxjs/operators';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { ifSuccessThenUnwrap, notNull } from '@app/core/utils';
import { DialogService } from '@app/shared/services/dialog.service';
import { AddByClassNumDialogComponent } from '../add-by-class-num-dialog/add-by-class-num-dialog.component';
import { IS_PRIVATE } from '@app/services/is-private.token';

@Component({
  selector: 'cse-search-filters',
  templateUrl: './search-filters.component.html',
  styleUrls: ['./search-filters.component.scss'],
})
export class SearchFiltersComponent implements OnDestroy {
  public form = this.store.select(searchSelectors.getForm);

  public termCodesOrArray: Observable<TermCode[]> = this.store
    .select(globalSelectors.terms.getActiveTermCodes)
    .pipe(
      map(termCodes => {
        if (termCodes.success) {
          return termCodes.value;
        } else {
          return [];
        }
      }),
    );

  public isZeroTerm: Observable<boolean> = this.store
    .select(searchSelectors.chosenTermCode)
    .pipe(map(termCode => termCode === null || Zero.is(termCode)));

  public currentSessions: Observable<Session[]> = this.store
    .select(searchSelectors.chosenTermCode)
    .pipe(
      filter(notNull),
      mergeMap(termCode =>
        this.store.select(globalSelectors.terms.getSessionsForTerm, termCode),
      ),
      ifSuccessThenUnwrap(),
    );

  public currentSubjects: Observable<Subject[]> = this.store
    .select(searchSelectors.chosenTermCode)
    .pipe(
      mergeMap(termCode =>
        this.store.select(
          globalSelectors.terms.getSubjectsForTerm,
          termCode ?? ZERO,
        ),
      ),
      ifSuccessThenUnwrap(),
    );

  public currentSpecialGroups: Observable<SpecialGroups> = this.store
    .select(searchSelectors.chosenTermCode)
    .pipe(
      filter(notNull),
      mergeMap(termCode =>
        this.store.select(
          globalSelectors.terms.getSpecialGroupsForTerm,
          termCode,
        ),
      ),
      ifSuccessThenUnwrap(),
    );

  public subjectSuggestions: Observable<Subject[]>;

  public isFormDisabled = this.store.select(searchSelectors.isFormDisabled);

  public formId = FORM_ID;

  public subjectFilterCtrl = new FormControl();
  public filteredSubjects = new ReplaySubject<Array<Subject | null>>(1);
  public specialGroupsCategoryCtrl = new FormControl('all');
  public filteredSpecialGroups = new ReplaySubject<SpecialGroup[]>(1);

  // Updating this list of subjects isn't ideal but it's an easy way to build
  // a synchronous function to filter subjects.
  private subjectCache: Array<Subject | null> = [];

  public termFilterLabel = 'All terms';
  public subjectFilterLabel = 'All subjects';

  public absoluteMinCreditValue = MIN_CREDIT_VALUE;
  public absoluteMaxCreditValue = MAX_CREDIT_VALUE;

  public updatedMinCreditValue: Observable<number> = this.store.select(
    searchSelectors.minCreditValue,
    MIN_CREDIT_VALUE,
  );

  public updatedMaxCreditValue: Observable<number> = this.store.select(
    searchSelectors.maxCreditValue,
    MAX_CREDIT_VALUE,
  );

  private subscriptions: Subscription[] = [];

  constructor(
    public store: Store<State>,
    private announcer: LiveAnnouncer,
    private dialog: DialogService,
    @Inject(IS_PRIVATE) public isPrivate: boolean,
  ) {
    /**
     * Pass an empty array to the filtered subjects list so that the there's
     * always _some_ value to render the filterable list. When the user starts
     * typing, the `filteredSubjects` will be updated from the list of subjects
     * in the current term.
     */
    this.filteredSubjects.next([]);

    this.subscriptions = [
      /**
       * Listen for the chosen term to change. When the term changes, enable or
       * disable some of the filters.
       */
      store.select(searchSelectors.chosenTermCode).subscribe(termCode => {
        this.termFilterLabel = termCode ? 'Term' : 'All terms';

        /**
         * Reset the reserved sections filter when switching terms ONLY IF the
         * value of the control is not "all". This condition will prevent
         * excessive numbers of changes to the NgRX state.
         */
        if (this.specialGroupsCategoryCtrl.value !== 'all') {
          this.specialGroupsCategoryCtrl.setValue('all');
          this.store.dispatch(
            new SetValueAction(`${FORM_ID}.reservedSections`, null),
          );
        }
      }),

      store.select(searchSelectors.chosenSubject).subscribe(subject => {
        this.subjectFilterLabel = subject ? 'Subject' : 'All subjects';
      }),

      /**
       * Listen for changes to the Special Groups radio button. When the user
       * chooses a different radio button, update the list of special groups in
       * the dropdown to match the corresponding special groups category.
       */
      this.specialGroupsCategoryCtrl.valueChanges
        .pipe(distinctUntilChanged(), withLatestFrom(this.currentSpecialGroups))
        .subscribe(([val, groups]) => {
          const categories: (keyof SpecialGroups)[] = [
            'residenceHalls',
            'firstYearInterestGroups',
            'communityLearning',
            'emergingScholars',
          ];

          if (categories.includes(val)) {
            this.filteredSpecialGroups.next(groups[val as keyof SpecialGroups]);
          } else {
            this.filteredSpecialGroups.next([]);
          }
        }),

      store.select(searchSelectors.getReservedSections).subscribe(category => {
        if (
          category === null &&
          this.specialGroupsCategoryCtrl.value !== 'all'
        ) {
          this.specialGroupsCategoryCtrl.setValue('all');
        } else if (category !== null) {
          switch (category.value.attribute) {
            case 'RESH':
              this.specialGroupsCategoryCtrl.setValue('residenceHalls');
              break;
            case 'FIG':
              this.specialGroupsCategoryCtrl.setValue(
                'firstYearInterestGroups',
              );
              break;
            case 'SVCL':
              this.specialGroupsCategoryCtrl.setValue('communityLearning');
              break;
            case 'WES':
              this.specialGroupsCategoryCtrl.setValue('emergingScholars');
              break;
          }
        }
      }),

      /**
       * Update the list of subjects whenever the term changes.
       */
      this.currentSubjects.subscribe(subjects => {
        this.subjectCache = [null, ...subjects.slice().sort(Subject.sort)];
        this.applySubjectFilter();
      }),

      /**
       * Update the filtered list of subjects when the subject filter changes
       */
      this.subjectFilterCtrl.valueChanges.subscribe(() => {
        this.applySubjectFilter();
      }),
    ];
  }

  applySubjectFilter() {
    const rawFilter: unknown = this.subjectFilterCtrl.value;
    if (!rawFilter) {
      this.filteredSubjects.next(this.subjectCache);
    } else {
      const search = `${rawFilter ?? ''}`.toLowerCase();
      const filtered = fuzzy<null | Subject>(search, this.subjectCache, s => {
        return s?.description ?? 'All subjects';
      });
      this.filteredSubjects.next(filtered);
    }
  }

  async onAddByClassNumDialog() {
    const termCode = await this.store
      .select(searchSelectors.chosenTermCode)
      .pipe(first())
      .toPromise();

    if (!TermCode.is(termCode)) {
      // Don't show the dialog if the current term is zero.
      return;
    }

    const sessions = await this.store
      .select(globalSelectors.terms.getSessionsForTerm, termCode)
      .pipe(ifSuccessThenUnwrap(), first())
      .toPromise();

    await this.dialog.small(AddByClassNumDialogComponent, {
      termCode,
      sessions,
    });
  }

  onClearSubjectFilter() {
    this.store.dispatch(new SetValueAction(`${FORM_ID}.subject`, null));
    this.store.dispatch(new ResetAction(`${FORM_ID}.subject`));
  }

  onClearSessionFilter() {
    this.store.dispatch(new SetValueAction(`${FORM_ID}.session`, null));
    this.store.dispatch(new ResetAction(`${FORM_ID}.session`));
  }

  onSubmit() {
    this.announcer.announce('Searching for courses', 'assertive');
    this.store.dispatch(actions.search.withCurrentFilters());
  }

  onReset(): void {
    this.store.dispatch(actions.filters.reset());
    this.store.dispatch(new ResetAction(FORM_ID));
  }

  compareSubjects(s1: unknown, s2: unknown): boolean {
    if (s1 === s2) {
      return true;
    } else if (Subject.schema.matches(s1) && Subject.schema.matches(s2)) {
      return s1.code === s2.code;
    } else {
      return false;
    }
  }

  compareTermCodes(t1: unknown, t2: unknown): boolean {
    return (
      TermCodeOrZero.is(t1) &&
      TermCodeOrZero.is(t2) &&
      TermCodeOrZero.equals(t1, t2)
    );
  }

  compareSessions(s1: unknown, s2: unknown): boolean {
    if (s1 === s2) {
      return true;
    } else if (Session.schema.matches(s1) && Session.schema.matches(s2)) {
      return s1.code === s2.code;
    } else {
      return false;
    }
  }

  compareSpecialGroups(g1: unknown, g2: unknown): boolean {
    if (g1 === g2) {
      return true;
    } else if (
      SpecialGroup.schema.matches(g1) &&
      SpecialGroup.schema.matches(g2)
    ) {
      return g1.code === g2.code;
    } else {
      return false;
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
