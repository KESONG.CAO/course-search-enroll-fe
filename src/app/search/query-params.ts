/**
 * This file contains functions to convert the form state into a set of query
 * params for use in the view's URL.
 *
 * Basically each key with type `T` in the `FormState` type is assigned a
 * serializer of type `Serializer<T>`. These `Serializer` types have
 * `serialize(T) => string` and `deserialize(string) => T | null` functions to
 * perform the conversion in each direction.
 *
 * The top-level `toParams` and `fromParams` functions then loop over the
 * `serializers` object containing all of the indicidual serializers doing the
 * appropriate conversion to generate query parameters from a `FormState` or to
 * generate a `FormState` from a map of query parameters.
 */

import {
  FormState,
  createEmptyFormState,
  FOREIGN_LANGUAGE,
  ForeignLanguage,
  isOrderBy,
  DEFAULT_ORDER_BY,
  MODES_OF_INSTRUCTION,
  ModeOfInstruction,
} from '@search/store/state';
import {
  Terms,
  TermCodeOrZero,
  Term,
  TermCode,
  SpecialGroup,
  Zero,
} from '@app/types/terms';
import { Params } from '@angular/router';
import { box, Boxed } from 'ngrx-forms';

interface Serializer<T> {
  /**
   * This function will only be called if this parameter exists and the value
   * is a non-empty string.
   */
  deserialize: (param: string, term: Term) => T;

  /**
   * The result of this function will only be included in the query params map
   * if it returns a non-empty string and if that string is different from the
   * this key's serialied value in the EMPTY_FORM_STATE object.
   */
  serialize: (state: T) => string;
}

type FormSerializer = {
  [K in keyof FormState]: Serializer<FormState[K]>;
};

const boolSerializer: Serializer<boolean> = {
  deserialize: param => param === 'true',
  serialize: state => `${state}`,
};

const formSerializers: FormSerializer = {
  orderBy: {
    deserialize: param => (isOrderBy(param) ? param : DEFAULT_ORDER_BY),
    serialize: state => state,
  },

  term: {
    deserialize: param => {
      const parsed = TermCode.decodeOrNull(param);
      return parsed ? box(parsed) : null;
    },
    serialize: state => (state ? TermCode.encode(state.value) : '0000'),
  },

  subject: {
    deserialize: (param, term) => {
      const found = term.subjects.find(s => s.code === param);
      return found ? box(found) : null;
    },
    serialize: state => (state ? state.value.code : ''),
  },

  keywords: {
    deserialize: param => param.trim(),
    serialize: state => (state ?? '').trim(),
  },

  open: boolSerializer,
  waitlisted: boolSerializer,
  closed: boolSerializer,

  commA: boolSerializer,
  commB: boolSerializer,
  quantA: boolSerializer,
  quantB: boolSerializer,
  ethnicStudies: boolSerializer,

  biologicalSciences: boolSerializer,
  humanities: boolSerializer,
  literature: boolSerializer,
  naturalSciences: boolSerializer,
  physicalSciences: boolSerializer,
  socialSciences: boolSerializer,

  elementary: boolSerializer,
  intermediate: boolSerializer,
  advanced: boolSerializer,

  language: {
    deserialize: param => {
      return FOREIGN_LANGUAGE.includes(param as any)
        ? (param as ForeignLanguage)
        : 'all';
    },
    serialize: state => state,
  },

  honorsOnly: boolSerializer,
  acceleratedHonors: boolSerializer,
  honorsOptional: boolSerializer,

  reservedSections: {
    deserialize: (param, term) => {
      const PATTERN = /^(RESH|FIG|SVCL|WES)-(\S+)$/i;
      const match = PATTERN.exec(param);

      if (match) {
        const attr = match[1];
        const code = match[2];
        const groups = term.specialGroups;
        const hasCode = (group: { code: string }) => group.code === code;
        let found: SpecialGroup | null = null;
        switch (attr.toLowerCase()) {
          case 'resh':
            found = groups.residenceHalls.find(hasCode) ?? null;
            break;
          case 'fig':
            found = groups.firstYearInterestGroups.find(hasCode) ?? null;
            break;
          case 'svcl':
            found = groups.communityLearning.find(hasCode) ?? null;
            break;
          case 'wes':
            found = groups.emergingScholars.find(hasCode) ?? null;
            break;
        }

        if (found) {
          return box(found);
        }
      }

      return null;
    },
    serialize: state => {
      if (state) {
        return `${state.value.attribute}-${state.value.code}`;
      } else {
        return '';
      }
    },
  },

  modeOfInstruction: {
    deserialize: param => {
      if (MODES_OF_INSTRUCTION.includes(param as any)) {
        return param as ModeOfInstruction;
      } else {
        return 'all';
      }
    },
    serialize: state => {
      return state;
    },
  },

  sustainability: boolSerializer,
  graduateCourseworkRequirement: boolSerializer,
  workplaceExperience: boolSerializer,
  communityBasedLearning: boolSerializer,

  credits: {
    deserialize: param => {
      let match: RegExpExecArray | null = null;
      if ((match = /^-(\d+)$/.exec(param))) {
        const max = parseInt(match[1], 10);
        return { min: null, max };
      } else if ((match = /^(\d+)-$/.exec(param))) {
        const min = parseInt(match[1], 10);
        return { min, max: null };
      } else if ((match = /^(\d+)-(\d+)$/.exec(param))) {
        const min = parseInt(match[1], 10);
        const max = parseInt(match[2], 10);
        return { min, max };
      } else {
        return { min: null, max: null };
      }
    },
    serialize: ({ min, max }) => {
      if (min !== null && max !== null) {
        return `${min}-${max}`;
      } else if (min !== null) {
        return `${min}-`;
      } else if (max !== null) {
        return `-${max}`;
      } else {
        return '';
      }
    },
  },

  session: {
    deserialize: (param, term) => {
      const found = term.sessions.find(s => s.code === param) ?? null;
      return found ? box(found) : null;
    },
    serialize: state => {
      if (state) {
        return state.value.code;
      } else {
        return '';
      }
    },
  },
};

export const toParams = (
  activeTermCodes: TermCode[],
  paramState: FormState,
): Params => {
  const params: Params = {};
  const emptyState = createEmptyFormState(TermCode.findFirst(activeTermCodes));

  for (const key in paramState) {
    if (paramState.hasOwnProperty(key)) {
      const serializer = formSerializers[key] as Serializer<unknown>;
      const paramValue = serializer.serialize(paramState[key]);
      const emptyValue = serializer.serialize(emptyState[key]);
      if (paramValue !== '' && paramValue !== emptyValue) {
        params[key] = paramValue;
      }
    }
  }

  return params;
};

const chosenTermFromParams = (
  terms: Terms,
  params: Params,
  currentTermCode?: TermCode,
): Term => {
  const termCodeFromParams = TermCodeOrZero.decodeOrNull(params['term']);

  const currentTerm: Term | undefined = currentTermCode
    ? terms.nonZero[TermCode.encode(currentTermCode)]
    : undefined;

  if (TermCode.is(termCodeFromParams)) {
    const encoded = TermCodeOrZero.encode(termCodeFromParams);
    return terms.nonZero[encoded] ?? currentTerm ?? terms.zero;
  }

  if (Zero.is(termCodeFromParams)) {
    return terms.zero;
  }

  if (currentTerm) {
    return currentTerm;
  }

  const firstActive = TermCode.findFirst(terms.activeTermCodes);
  return terms.nonZero[TermCodeOrZero.encode(firstActive)];
};

export const fromParams = (
  terms: Terms,
  params: Params,
  currentTermCode?: TermCode,
): FormState => {
  const term = chosenTermFromParams(terms, params, currentTermCode);

  const defaultTermCode = TermCode.findFirst(terms.activeTermCodes);
  const emptyState = createEmptyFormState(defaultTermCode);

  const fields: Partial<FormState> & { term: FormState['term'] } = {
    term: term === terms.zero ? null : box(term.termCode as TermCode),
  };

  for (const key in params) {
    if (!params.hasOwnProperty(key)) {
      continue;
    }

    if (emptyState.hasOwnProperty(key)) {
      const param = `${params[key] ?? ''}`;
      if (param.trim().length === 0) {
        continue;
      }

      const serializer = formSerializers[key] as Serializer<unknown>;
      let state = serializer.deserialize(param, term);

      // Handle the case where the `term` parameter in the URL contained valid
      // term-code syntax but represented a term that isn't part of the active
      // term set.
      if (
        key === 'term' &&
        TermCode.is((state as FormState['term'])?.value) &&
        TermCode.isContainedInArray(
          (state as Boxed<TermCode>).value,
          terms.activeTermCodes,
        ) === false
      ) {
        state = box(defaultTermCode);
      }

      fields[key] = state;
    }
  }

  return { ...emptyState, ...fields };
};
