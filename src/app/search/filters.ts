import { FormState, ModeOfInstruction, OrderBy } from '@search/store/state';
import { TermCode } from '@app/types/terms';
import { notNull } from '@app/core/utils';
import { SearchParameters } from '@app/services/api.service';

export const DEFAULT_SEARCH_PAGE_SIZE = 50;

/**
 * When the user submits a search, build a JSON ElasticSearch query from the
 * filters that the user selected. Some of the filters structures are kind of
 * esoteric and most of this code is a re-interpretation of the original
 * AngularJS implementation.
 *
 * @param state FormState
 * @param page number
 */
export const stateToElasticQuery = (
  state: FormState,
  page: number,
): SearchParameters => {
  const filters: unknown[] = [
    ...enrollmentStatusFilters(state),
    ...subjectFilters(state),
    ...genEdFilters(state),
    ...breadthFilters(state),
    ...levelFilters(state),
    ...languageFilters(state),
    ...honorsFilters(state),
    ...reservedSectionsFilters(state),
    ...modeOfInstructionFilters(state),
    ...sustainabilityFilter(state),
    ...workplaceExperienceFilter(state),
    ...gradCourseworkFilter(state),
    ...communityLearningFilter(state),
    ...minCreditsFilter(state),
    ...maxCreditsFilter(state),
    ...sessionFilter(state),
  ];

  return {
    selectedTerm: selectedTerm(state),
    queryString: queryString(state),
    filters,
    page,
    pageSize: DEFAULT_SEARCH_PAGE_SIZE,
    sortOrder: orderByFilter(state.orderBy),
  };
};

const selectedTerm = (state: FormState): string => {
  return state.term ? TermCode.encode(state.term.value) : '0000';
};

const queryString = (state: FormState): string => {
  return !(state.keywords ?? '').trim() ? '*' : state.keywords!.trim();
};

const enrollmentStatusFilters = (state: FormState): unknown[] => {
  const statuses: string[] = [];

  if (state.open) {
    statuses.push('OPEN');
  }

  if (state.waitlisted) {
    statuses.push('WAITLISTED');
  }

  if (state.closed) {
    statuses.push('CLOSED');
  }

  if (TermCode.is(state.term?.value) && statuses.length > 0) {
    return [
      {
        has_child: {
          type: 'enrollmentPackage',
          query: {
            match: {
              'packageEnrollmentStatus.status': statuses.join(' '),
            },
          },
        },
      },
    ];
  } else {
    return [];
  }
};

const subjectFilters = (state: FormState): unknown[] => {
  if (state.subject !== null) {
    return [
      {
        term: {
          'subject.subjectCode': state.subject.value.code,
        },
      },
    ];
  } else {
    return [];
  }
};

const genEdFilters = (state: FormState): unknown[] => {
  const should: unknown[] = [];

  const generalEdCode: string[] = [
    state.commA ? 'COM A' : null,
    state.commB ? 'COM B' : null,
    state.quantA ? 'QR-A' : null,
    state.quantB ? 'QR-B' : null,
  ].filter(notNull);

  if (generalEdCode.length > 0) {
    should.push({ terms: { 'generalEd.code': [generalEdCode] } });
  }

  if (state.ethnicStudies) {
    should.push({ term: { 'ethnicStudies.code': 'ETHNIC ST' } });
  }

  if (state.commB) {
    should.push({
      has_child: {
        type: 'class',
        query: { term: { comB: true } },
      },
    });
  }

  if (should.length > 0) {
    return [{ bool: { should } }];
  } else {
    return [];
  }
};

const breadthFilters = (state: FormState): unknown[] => {
  const relevant: string[] = [
    state.biologicalSciences ? 'B' : null,
    state.humanities ? 'H' : null,
    state.literature ? 'L' : null,
    state.naturalSciences ? 'N' : null,
    state.physicalSciences ? 'P' : null,
    state.socialSciences ? 'S' : null,
  ].filter(notNull);

  if (relevant.length > 0) {
    return [{ terms: { 'breadths.code': relevant } }];
  } else {
    return [];
  }
};

const levelFilters = (state: FormState): unknown[] => {
  const relevant: string[] = [
    state.elementary ? 'E' : null,
    state.intermediate ? 'I' : null,
    state.advanced ? 'A' : null,
  ].filter(notNull);

  if (relevant.length > 0) {
    return [{ terms: { 'levels.code': relevant } }];
  } else {
    return [];
  }
};

const languageFilters = (state: FormState): unknown[] => {
  if (state.language !== 'all') {
    const ENCODING = {
      first: 'FL1',
      second: 'FL2',
      third: 'FL3',
      fourth: 'FL4',
      fifth: 'FL5',
    };

    return [
      {
        query: { match: { 'foreignLanguage.code': ENCODING[state.language] } },
      },
    ];
  } else {
    return [];
  }
};

const honorsFilters = (state: FormState): unknown[] => {
  const relevant: string[] = [
    state.honorsOnly ? 'HONORS_ONLY' : null,
    state.acceleratedHonors ? 'HONORS_LEVEL' : null,
    state.honorsOptional ? 'INSTRUCTOR_APPROVED' : null,
  ].filter(notNull);

  if (relevant.length > 0) {
    return [
      {
        or: [
          { terms: { honors: relevant } },
          {
            has_child: {
              type: 'class',
              query: { terms: { honors: relevant } },
            },
          },
        ],
      },
    ];
  } else {
    return [];
  }
};

const reservedSectionsFilters = (state: FormState): unknown[] => {
  if (state.reservedSections !== null) {
    return [
      {
        has_child: {
          type: 'class',
          filter: {
            term: {
              'classAttributes.valueCode': state.reservedSections.value.code,
            },
          },
        },
      },
    ];
  } else {
    return [];
  }
};

const modeOfInstructionFilters = (state: FormState): unknown[] => {
  const modeFilters: Record<ModeOfInstruction, unknown> = {
    all: null,
    classroom: {
      must: [{ match: { modesOfInstruction: 'Instruction' } }],
      must_not: [
        { match: { modesOfInstruction: 'some' } },
        { match: { modesOfInstruction: 'Only' } },
        { match: { modesOfInstruction: 'EMPTY' } },
      ],
    },
    hybrid: {
      must: [{ match: { modesOfInstruction: 'some' } }],
      must_not: [
        { match: { modesOfInstruction: 'Instruction' } },
        { match: { modesOfInstruction: 'Only' } },
        { match: { modesOfInstruction: 'EMPTY' } },
      ],
    },
    async: {
      must: [
        { match: { modesOfInstruction: 'Only' } },
        { match: { isAsynchronous: true } },
      ],
      must_not: [
        { match: { modesOfInstruction: 'Instruction' } },
        { match: { modesOfInstruction: 'some' } },
        { match: { isAsynchronous: false } },
        { match: { modesOfInstruction: 'EMPTY' } },
      ],
    },
    sync: {
      must: [
        { match: { modesOfInstruction: 'Only' } },
        { match: { isAsynchronous: false } },
      ],
      must_not: [
        { match: { modesOfInstruction: 'Instruction' } },
        { match: { modesOfInstruction: 'some' } },
        { match: { isAsynchronous: true } },
        { match: { modesOfInstruction: 'EMPTY' } },
      ],
    },
    either: {
      must: [{ match: { modesOfInstruction: 'Only' } }],
      must_not: [
        { match: { modesOfInstruction: 'Instruction' } },
        { match: { modesOfInstruction: 'some' } },
        { match: { modesOfInstruction: 'EMPTY' } },
      ],
    },
  };

  const chosenFilters = modeFilters[state.modeOfInstruction];

  if (!chosenFilters) {
    return [];
  }

  return [
    {
      has_child: {
        type: 'enrollmentPackage',
        query: { bool: chosenFilters },
      },
    },
  ];
};

const sustainabilityFilter = (state: FormState): unknown[] => {
  return state.sustainability
    ? [{ query: { exists: { field: 'sustainability' } } }]
    : [];
};

const gradCourseworkFilter = (state: FormState): unknown[] => {
  return state.graduateCourseworkRequirement
    ? [{ term: { gradCourseWork: true } }]
    : [];
};

const workplaceExperienceFilter = (state: FormState): unknown[] => {
  return state.workplaceExperience
    ? [{ query: { exists: { field: 'workplaceExperience' } } }]
    : [];
};

const communityLearningFilter = (state: FormState): unknown[] => {
  return state.communityBasedLearning
    ? [
        {
          has_child: {
            type: 'class',
            filter: { term: { 'classAttributes.valueCode': '25 PLUS' } },
          },
        },
      ]
    : [];
};

const minCreditsFilter = (state: FormState): unknown[] => {
  if (state.credits.min !== null) {
    return [{ range: { minimumCredits: { gte: state.credits.min } } }];
  } else {
    return [];
  }
};

const maxCreditsFilter = (state: FormState): unknown[] => {
  if (state.credits.max !== null) {
    return [{ range: { maximumCredits: { lte: state.credits.max } } }];
  } else {
    return [];
  }
};

const sessionFilter = (state: FormState): unknown[] => {
  if (state.session !== null) {
    return [
      {
        has_child: {
          type: 'class',
          query: { term: { sessionCode: state.session.value.code } },
        },
      },
    ];
  } else {
    return [];
  }
};

const orderByFilter = (orderBy: OrderBy) => {
  switch (orderBy) {
    case 'subject':
      return 'SUBJECT';
    case 'catalog-number':
      return 'CATALOG_NUMBER';
    case 'relevance':
    default:
      return 'SCORE';
  }
};
