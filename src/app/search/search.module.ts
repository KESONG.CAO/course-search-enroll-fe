import { NgModule } from '@angular/core';
import { SearchViewComponent } from './search-view/search-view.component';
import { SharedModule } from '@app/shared/shared.module';
import { SearchFiltersComponent } from './search-filters/search-filters.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { FilterGroupComponent } from './search-filters/filter-group/filter-group.component';
import { EffectsModule } from '@ngrx/effects';
import { SearchEffects } from './store/effects';
import { StoreModule } from '@ngrx/store';
import { reducer } from '@search/store/reducer';
import { NgrxFormsModule } from 'ngrx-forms';
import { MaterialModule } from '../../lib/material';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { Routes, RouterModule } from '@angular/router';
import { AddToPlanDialogComponent } from './add-to-plan-dialog/add-to-plan-dialog.component';
import { AddByClassNumDialogComponent } from './add-by-class-num-dialog/add-by-class-num-dialog.component';

const routes: Routes = [
  {
    path: '',
    component: SearchViewComponent,
  },
];

@NgModule({
  declarations: [
    SearchViewComponent,
    AddToPlanDialogComponent,
    SearchFiltersComponent,
    SearchResultsComponent,
    FilterGroupComponent,
    AddByClassNumDialogComponent,
  ],
  imports: [
    MaterialModule,
    SharedModule,
    NgrxFormsModule,
    NgxMatSelectSearchModule,
    StoreModule.forFeature('search', reducer),
    EffectsModule.forFeature([SearchEffects]),
    RouterModule.forChild(routes),
  ],
})
export class SearchModule {}
