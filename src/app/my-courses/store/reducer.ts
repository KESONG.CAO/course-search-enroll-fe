import { createReducer, createAction, on, On, props } from '@ngrx/store';
import {
  RoadmapCourse,
  CurrentCourse,
  CourseRef,
  SavedCourse,
} from '@app/types/courses';
import { produce } from 'immer';
import * as actions from './actions';
import * as globalActions from '@app/actions';
import { MyCourses, EMPTY_STATE } from './state';

export const noop = createAction('[MY COURSES] noop');

namespace lifecycle {
  export const init: On<MyCourses> = on(actions.lifecycle.init, state => {
    return produce(state, draft => {
      if (draft.wasInitialized === 'ready') {
        draft.wasInitialized = 'pending';
      }
    });
  });

  export const setTerm: On<MyCourses> = on(
    globalActions.currentTermCode.set,
    state => {
      return produce(state, draft => {
        draft.wasInitialized = 'done';
      });
    },
  );
}

export const showCart = createAction('[MY COURSES] show cart');
const onShowCart: On<MyCourses> = on(
  showCart,
  (state): MyCourses => {
    return produce(state, draft => {
      if (state.chosenCategory !== 'cart') {
        draft.chosenCategory = 'cart';
        draft.areDetailsShown = null;
        draft.checked = [];
      }
    });
  },
);

export const showEnrolled = createAction('[MY COURSES] show enrolled');
const onShowEnrolled: On<MyCourses> = on(
  showEnrolled,
  (state): MyCourses => {
    return produce(state, draft => {
      if (state.chosenCategory !== 'enrolled') {
        draft.chosenCategory = 'enrolled';
        draft.areDetailsShown = null;
        draft.checked = [];
      }
    });
  },
);

export const showWaitlisted = createAction('[MY COURSES] show waitlisted');
const onShowWaitlisted: On<MyCourses> = on(
  showWaitlisted,
  (state): MyCourses => {
    return produce(state, draft => {
      if (state.chosenCategory !== 'waitlisted') {
        draft.chosenCategory = 'waitlisted';
        draft.areDetailsShown = null;
        draft.checked = [];
      }
    });
  },
);

export const showDropped = createAction('[MY COURSES] show dropped');
const onShowDropped: On<MyCourses> = on(
  showDropped,
  (state): MyCourses => {
    return produce(state, draft => {
      if (state.chosenCategory !== 'dropped') {
        draft.chosenCategory = 'dropped';
        draft.areDetailsShown = null;
        draft.checked = [];
      }
    });
  },
);

export const showSavedForLater = createAction(
  '[MY COURSES] show savedForLater',
);
const onShowSavedForLater: On<MyCourses> = on(
  showSavedForLater,
  (state): MyCourses => {
    return produce(state, draft => {
      if (state.chosenCategory !== 'saved') {
        draft.chosenCategory = 'saved';
        draft.areDetailsShown = null;
        draft.checked = [];
      }
    });
  },
);

export const showDetails = createAction(
  '[MY COURSES] show details',
  props<{ course: RoadmapCourse | CurrentCourse | SavedCourse }>(),
);
const onShowDetails: On<MyCourses> = on(showDetails, (state, { course }) => {
  return produce(state, draft => {
    draft.areDetailsShown = course.ref;
    draft.arePackagesShown = false;
  });
});

export const saveForLater = createAction(
  '[MY COURSES] save for later',
  props<{ course }>(),
);
const onSaveForLater: On<MyCourses> = on(
  saveForLater,
  (state): MyCourses => {
    // FIXME
    return state;
  },
);

namespace packages {
  export const show: On<MyCourses> = on(
    actions.packages.show,
    (state): MyCourses => {
      return produce(state, draft => {
        draft.arePackagesShown = true;
      });
    },
  );

  export const hide: On<MyCourses> = on(
    actions.packages.hide,
    (state): MyCourses => {
      return produce(state, draft => {
        draft.arePackagesShown = false;
      });
    },
  );
}

export const selectCourse = createAction(
  '[MY COURSES] select course',
  props<{ ref: CourseRef; isChecked: boolean }>(),
);
const onSelectCourse: On<MyCourses> = on(
  selectCourse,
  (state, { ref, isChecked }): MyCourses => {
    return produce(state, draft => {
      if (isChecked) {
        const isAlreadyChecked = state.checked.some(CourseRef.equalsCurry(ref));
        if (!isAlreadyChecked) {
          draft.checked.push(ref);
        }
      } else {
        draft.checked = draft.checked.filter(r => !CourseRef.equals(ref, r));
      }
    });
  },
);

const onSelectCourses: On<MyCourses> = on(
  actions.checked.set,
  (state, { refs }): MyCourses => {
    return produce(state, draft => {
      draft.checked = refs;
    });
  },
);

export const clearChecked = createAction('my-courses/clear-checked');

const onClearChecked: On<MyCourses> = on(clearChecked, state => {
  return produce(state, draft => {
    draft.checked = [];
  });
});

export const hideDetails = createAction('[MY COURSES] hide details');

namespace schedule {
  export const show: On<MyCourses> = on(
    actions.schedule.show,
    (state): MyCourses => {
      return produce(state, draft => {
        draft.isScheduleShown = true;
      });
    },
  );

  export const hide: On<MyCourses> = on(
    actions.schedule.hide,
    (state): MyCourses => {
      return produce(state, draft => {
        draft.isScheduleShown = false;
      });
    },
  );
}

const onCloseSidebars: On<MyCourses> = on(
  globalActions.courses.current.drop,
  hideDetails,
  (state): MyCourses => {
    return produce(state, draft => {
      draft.areDetailsShown = null;
      draft.arePackagesShown = false;
      draft.isScheduleShown = false;
    });
  },
);

export const reducer = createReducer(
  EMPTY_STATE,

  lifecycle.init,
  lifecycle.setTerm,

  onCloseSidebars,

  onShowCart,
  onShowEnrolled,
  onShowWaitlisted,
  onShowDropped,
  onShowSavedForLater,
  onSelectCourse,
  onSelectCourses,
  onClearChecked,
  onSaveForLater,

  onShowDetails,

  packages.show,
  packages.hide,

  schedule.show,
  schedule.hide,
);
