import { createAction, props, Action } from '@ngrx/store';
import {
  RoadmapCourse,
  CurrentCourse,
  SavedCourse,
  RoadmapCourseReady,
  CourseRef,
} from '@app/types/courses';

export namespace lifecycle {
  export const init = createAction('my-courses/lifecycle/init');
}

export namespace roadmap {
  export const moveToSaved = createAction(
    'my-courses/roadmap/moveToSaved',
    props<{ course: RoadmapCourse }>(),
  );
}

export namespace saved {
  export const moveToCart = createAction(
    'my-courses/saved/moveToCart',
    props<{ course: SavedCourse }>(),
  );
}

export namespace swap {
  export const start = createAction(
    'my-courses/swap/start',
    props<{ enroll: RoadmapCourseReady; drop: CurrentCourse }>(),
  );

  export const done = createAction(
    'my-courses/swap/done',
    props<{
      didEnroll: RoadmapCourseReady;
      didDrop: CurrentCourse;
    }>(),
  );

  export const error = createAction(
    'my-courses/swap/error',
    props<{ description: string }>(),
  );
}

/**
 * @deprecated
 */
export namespace polling {
  /**
   * Starts polling the `/api/pending` endpoint if there isn't any ongoing
   * polling. If there _is_ ongoing polling this action will be ignored.
   */
  export const start = createAction(
    'my-courses/polling/start',
    props<{ after: Action }>(),
  );

  /**
   * DON'T DISPATCH THIS ACTION MANUALLY.
   *
   * This action only works if the polling state machine is in the "starting" or
   * "polling" states. It will be ignored otherwise. If you want to start
   * polling, use the `my-courses/polling/start` action.
   */
  export const poll = createAction(
    'my-courses/polling/poll',
    props<{ after: Action }>(),
  );

  /**
   * Stops any ongoing polling immediately. This action will be automatically
   * dispatched if the `/api/pending` endpoint returns a non-InProgress response.
   */
  export const done = createAction(
    'my-courses/polling/done',
    props<{ after: Action }>(),
  );
}

export namespace packages {
  export const show = createAction('my-courses/packages/show');

  export const hide = createAction('my-courses/packages/hide');
}

export namespace schedule {
  export const show = createAction('my-courses/schedule/show');

  export const hide = createAction('my-courses/schedule/hide');
}

export namespace checked {
  export const set = createAction(
    'my-courses/checked/set',
    props<{
      refs: CourseRef[];
    }>(),
  );
}
