import { GlobalState } from '@app/state';
import { CourseRef } from '@app/types/courses';

export type CategoryName =
  | 'cart'
  | 'enrolled'
  | 'waitlisted'
  | 'dropped'
  | 'saved';

export interface MyCourses {
  wasInitialized: 'ready' | 'pending' | 'done';
  areDetailsShown: CourseRef | null;
  arePackagesShown: boolean;
  isScheduleShown: boolean;
  checked: CourseRef[];
  chosenCategory: CategoryName;
}

export interface State extends GlobalState {
  myCourses: MyCourses;
}

export const EMPTY_STATE: MyCourses = {
  wasInitialized: 'ready',
  areDetailsShown: null,
  arePackagesShown: false,
  isScheduleShown: false,
  checked: [],
  chosenCategory: 'cart',
};
