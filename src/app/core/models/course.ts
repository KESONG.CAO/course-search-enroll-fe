import { TermCode } from '@app/types/terms';

export interface CourseBase {
  id: number | null;
  courseId: string;
  termCode: string | null;
  topicId: number;
  title: string;
  subjectCode: string;
  subject: any;
  catalogNumber: string;
  credits?: number;
  creditMin?: number;
  creditMax?: number;
  grade?: any;
  classNumber: string | null;
  courseOrder: number;
  honors: string;
  waitlist: string;
  relatedClassNumber1?: any;
  relatedClassNumber2?: any;
  classPermissionNumber?: any;
  sessionCode?: any;
  validationResults: readonly any[];
  enrollmentResults: readonly any[];
  pendingEnrollments: readonly any[];
  details?: any;
  classMeetings?: any;
  enrollmentOptions?: any;
  packageEnrollmentStatus?: any;
  creditRange?: any;
  subjectDescription?: string;
  studentEnrollmentStatus:
    | null
    | 'Transfer'
    | 'Enrolled'
    | 'Waitlisted'
    | 'cart'
    | 'NOTOFFERED'
    | 'DOESNOTEXIST';
}

export interface Course extends CourseBase {
  termCode: string;
}

export interface SubjectCodesTo<T> {
  readonly [subjectCode: string]: T;
}

export interface SubjectDescription {
  readonly short: string;
  readonly long: string;
}

export function pickCreditAmountFromCourse(course: CourseBase): number {
  if (typeof course.creditMin === 'number') {
    if (
      typeof course.credits === 'number' &&
      course.credits >= course.creditMin
    ) {
      return course.credits;
    } else {
      return course.creditMin;
    }
  }

  if (typeof course.creditRange === 'string') {
    const matches = course.creditRange.match(/^(\d+)/);
    if (matches !== null && matches.length >= 1) {
      return parseInt(matches[1], 10);
    }
  }

  console.warn(
    `course ${course.courseId} has no credits value and no creditsMin value`,
  );
  return 0;
}

export abstract class CourseLike {
  public abstract title: string;
  public abstract catalog(): string;
  public abstract hasCredits(): boolean;
  public abstract showCredits(): string | null;
}

export abstract class CourseLikeWithTerm extends CourseLike {
  public abstract termCode: TermCode;
}
