export interface StudentInfo {
  personAttributes: {
    emplid: string;
    pvi: string;
    name: {
      first: string;
      last: string;
    };
    email: string;
    netid: string;
  };
  ferpaAttributes: {
    name: boolean;
    email: boolean;
  };
  primaryCareer: null | {
    careerCode: string;
    programName: string;
    enrollmentStatusCode: string;
    academicLevelDescription: string;
    academicLoadDescription: string;
    termCode: string;
  };
}
