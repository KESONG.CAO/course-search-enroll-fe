/**
 * These interfaces are a bit sparse, the real objects will have more fields but
 * these are the only fields needed to convert a flat list of enrollment packages
 * into a nested list where enrollment packages with the same lecture are grouped
 * together.
 *
 * Some enrollment packages don't have a lecture. Any enrollment packages WITHOUT
 * a lecture aren't grouped and are left alone.
 *
 * Call the `groupPackages` function to run the grouping algorithm.
 */

import { EnrollmentPackage, RawSection } from '@app/types/courses';

export type PackageGroup = NestedPackageGroup | ShallowPackageGroup;

export interface NestedPackageGroup {
  kind: 'nested';
  lecture: RawSection;
  packages: NestedPackage[];
}

export interface NestedPackage extends EnrollmentPackage {
  isNested: true;
  sectionsWithoutLecture: RawSection[];
}

export interface ShallowPackageGroup {
  kind: 'shallow';
  package: ShallowPackage;
}

export interface ShallowPackage extends EnrollmentPackage {
  isNested: false;
}

export function groupPackages(packs: EnrollmentPackage[]): PackageGroup[] {
  return [...findNested(packs), ...findShallow(packs)];
}

export function groupWithoutNesting(
  packs: EnrollmentPackage[],
): ShallowPackageGroup[] {
  return packs.map(pack => ({
    kind: 'shallow',
    package: { ...pack, isNested: false },
  }));
}

function findNested(packs: EnrollmentPackage[]): NestedPackageGroup[] {
  const mapping = packs.reduce<Record<string, NestedPackageGroup>>((m, p) => {
    const maybeLecture = hasOneLecture(p);

    if (maybeLecture) {
      const key = `${maybeLecture.lecture.type} ${maybeLecture.lecture.sectionNumber}`;
      m[key] = m[key] ? mergeNested(m[key], maybeLecture) : maybeLecture;
    }

    return m;
  }, {});

  const nested = Object.values(mapping);

  nested.sort((a, b) => {
    const aIndex = packs.findIndex(p => {
      return p.sections.some(s => {
        return s.type === 'LEC' && s.sectionNumber === a.lecture.sectionNumber;
      });
    });

    const bIndex = packs.findIndex(p => {
      return p.sections.some(s => {
        return s.type === 'LEC' && s.sectionNumber === b.lecture.sectionNumber;
      });
    });

    return bIndex - aIndex;
  });

  nested.forEach(({ packages }) => {
    packages.sort((a, b) => {
      const aIndex = packs.findIndex(p => p.id === a.id);
      const bIndex = packs.findIndex(p => p.id === b.id);
      return bIndex - aIndex;
    });
  });

  return nested;
}

function mergeNested(a: NestedPackageGroup, b: NestedPackageGroup) {
  return {
    ...a,
    packages: [...a.packages, ...b.packages],
  } as NestedPackageGroup;
}

const isLecture = (s: RawSection): boolean => s.type === 'LEC';
const isNotLecture = (s: RawSection): boolean => s.type !== 'LEC';

function hasOneLecture(pack: EnrollmentPackage): NestedPackageGroup | null {
  const areLectures = pack.sections.filter(isLecture);
  const areNotLectures = pack.sections.filter(isNotLecture);

  if (areLectures.length === 1 && areNotLectures.length > 0) {
    return {
      kind: 'nested',
      lecture: areLectures[0],
      packages: [
        {
          ...pack,
          isNested: true,
          sectionsWithoutLecture: areNotLectures,
        },
      ],
    };
  }

  return null;
}

function findShallow(packs: EnrollmentPackage[]): ShallowPackageGroup[] {
  return packs
    .filter(p => hasOneLecture(p) === null)
    .map(pack => {
      return {
        kind: 'shallow',
        package: { ...pack, isNested: false },
      };
    });
}
