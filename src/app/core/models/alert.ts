export interface Alert {
  key: string;
  title: string;
  message: string;
  callback?: () => void;
}

export class DisclaimerAlert implements Alert {
  public readonly key = 'disclaimerAlert';
  public readonly title = 'This is a planning tool.';
  public readonly message =
    'If you have questions about your plan or degree, please contact your advisor.';

  constructor(public callback: Alert['callback']) {}
}

export class DarsDisclaimerAlert implements Alert {
  public readonly key = 'disclaimerAlert';
  public readonly title = 'This is a planning tool.';
  public readonly message =
    'If you have questions about your plan or degree, please contact your advisor.';

  constructor(public callback: Alert['callback']) {}
}
