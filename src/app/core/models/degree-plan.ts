import * as s from '@app/types/schema';

export interface DegreePlan {
  roadmapId: number;
  pvi: string;
  name: string;
  primary: boolean;
}

export namespace DegreePlan {
  export const schema = s.object<DegreePlan>({
    roadmapId: s.number,
    pvi: s.string,
    name: s.string,
    primary: s.boolean,
  });
}
