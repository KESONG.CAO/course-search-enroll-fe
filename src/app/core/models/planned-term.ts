import { Course } from '@app/core/models/course';
import { WithEra } from '@app/types/terms';

export type PlannedTermNote =
  | { isLoaded: true; text: string; id: number }
  | { isLoaded: false; text: string };

export interface PlannedTerm {
  roadmapId: number;
  termCode: WithEra;
  note?: PlannedTermNote;
  plannedCourses: Course[];
  enrolledCourses: Course[];
  transferredCourses: Course[];
}
