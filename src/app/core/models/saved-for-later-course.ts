export interface SavedForLaterCourse {
  id: number | null;
  courseId: string;
  termCode: '0000';
  topicId: 0;
  subjectCode: string;
  title: string;
  catalogNumber: string;
  courseOrder: 0;
  shortCatalog?: string;
}
