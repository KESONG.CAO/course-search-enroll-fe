import { PlannedTerm } from '@app/core/models/planned-term';
import { YearCode } from '@app/types/terms';

export interface Year {
  yearCode: YearCode;
  isExpanded: boolean;
  fall: PlannedTerm;
  spring: PlannedTerm;
  summer: PlannedTerm;
}

export interface MutableYearMapping {
  [yearCode: string]: Year;
}

export interface YearMapping {
  readonly [yearCode: string]: Year;
}
