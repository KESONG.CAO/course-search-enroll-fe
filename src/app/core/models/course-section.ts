export interface CourseSection {
  active: boolean;
  addConsent: AddConsent;
  catalogNumber: string;
  classAttributes: any[];
  classMaterials: any[];
  classMeetings: any[];
  classUniqueId: ClassUniqueId;
  comB: boolean;
  courseId: string;
  creditRange: string;
  crossListing: CrossListing;
  dropConsent: DropConsent;
  endDate: number;
  enrollmentPackages: any[];
  enrollmentStatus: EnrollmentStatus;
  footnotes: any[];
  gradedComponent: boolean;
  honors: null;
  instructionMode: string;
  instructor: Instructor;
  instructorProvidedClassDetails?: null;
  instructors: any[];
  packageEnrollmentStatus: {};
  published: boolean;
  sectionNumber: string;
  sessionCode: string;
  show: boolean;
  startDate: number;
  subject: Subject;
  topic: Topic;
  type: string;
}

export interface Topic {
  id: number;
  longDescription: string;
  shortDescription: string;
  topicLastTaught: any;
}

export interface AddConsent {
  code: string;
  description: string;
}

export interface ClassUniqueId {
  classNumber: number;
  termCode: string;
}

export interface CrossListing {
  crossListedType: string;
  primaryClassNumber: number;
  primarySubject: PrimarySubject;
}

export interface DropConsent {
  code: string;
  description: string;
}

export interface EnrollmentStatus {
  aggregateCapacity: any;
  aggregateCurrentlyEnrolled: any;
  aggregateWaitlistCapacity: any;
  aggregateWaitlistCurrentSize: any;
  capacity: number;
  classUniqueId: ClassUniqueId;
  currentlyEnrolled: number;
  openSeats: number;
  openWaitlistSpots: number;
  waitlistCapacity: number;
  waitlistCurrentSize: number;
}

export interface PrimarySubject {
  classNumber: number;
  termCode: string;
}

export interface Subject {
  departmentOwnerAcademicOrgCode: string;
  departmentURI: string;
  description: string;
  footnotes: any[];
  formalDescription: string;
  graduateCatalogURI: string;
  schoolCollege: SchoolCollege;
  shortDescription: string;
  subjectCode: string;
  termCode: string;
  uddsFundingSource: string;
  undergraduateCatalogURI: string;
}

export interface SchoolCollege {
  academicGroupCode: string;
  academicOrgCode: string;
  formalDescription: string;
  schoolCollegeURI: string;
  shortDescription: string;
  uddsCode: string;
}

export interface Instructor {
  firstName: string;
  lastName: string;
}
