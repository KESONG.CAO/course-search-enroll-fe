export interface Note {
  id: number;
  termCode: string;
  note: string;
}
