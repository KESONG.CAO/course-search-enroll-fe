export interface CoursePackage {
  catalogNumber: string;
  classMeetings: ClassMeeting;
  classPermissionNumberEnabled: boolean;
  courseId: string;
  creditRange: string;
  docId: string;
  enrollmentClassNumber: number;
  enrollmentOptions: EnrollmentOptions;
  enrollmentRequirementGroups: EnrollmentRequirementGroups;
  enrollmentStatus: EnrollmentStatus;
  id: string;
  instructorProvidedClassDetails: InstructorProvidedClassDetails;
  lastUpdated: number;
  meetingMap: MeetingMap;
  nestedClassMeetings: NestedClassMeeting;
  onlineOnly: boolean;
  packageEnrollmentStatus: PackageEnrollmentStatus;
  published: boolean;
  sections: Section[];
  subjectCode: string;
  termCode: string;
  topic: Topic;
  type: string;
}

export interface PackageEnrollmentStatus {
  availableSeats: number;
  waitlistTotal: number;
  status: string;
}

export interface Building {
  buildingCode: string;
  buildingName: string;
  streetAddress: string;
  latitude: number;
  longitude: number;
  location: number[];
}

export interface ClassMeeting {
  meetingOrExamNumber: string;
  meetingType: string;
  meetingTimeStart: number;
  meetingTimeEnd: number;
  meetingDays: string;
  meetingDaysList: string[];
  building: Building;
  room: string;
  examDate?: number;
  monday: boolean;
  tuesday: boolean;
  wednesday: boolean;
  thursday: boolean;
  friday: boolean;
  saturday: boolean;
  sunday: boolean;
  startDate: any;
  endDate: any;
}

export interface Building2 {
  buildingCode: string;
  buildingName: string;
  streetAddress: string;
  latitude: number;
  longitude: number;
  location: number[];
}

export interface NestedClassMeeting {
  meetingOrExamNumber: string;
  meetingType: string;
  meetingTimeStart: number;
  meetingTimeEnd: number;
  meetingDays: string;
  meetingDaysList: string[];
  building: Building2;
  room: string;
  examDate?: number;
  monday: boolean;
  tuesday: boolean;
  wednesday: boolean;
  thursday: boolean;
  friday: boolean;
  saturday: boolean;
  sunday: boolean;
  startDate: any;
  endDate: any;
}

export interface ClassUniqueId {
  termCode: string;
  classNumber: number;
}

export interface InstructorProvidedClassDetails {
  classUniqueId: ClassUniqueId;
  instructorDescription: string;
  typicalTopicsAndOrSchedule: string;
  format: string;
  learningOutcome: string;
  keywords: string[];
  labeledURIs: any[];
  lastUpdated: number;
}

export interface ClassUniqueId2 {
  termCode: string;
  classNumber: number;
}

export interface Topic {
  shortDescription: string;
  longDescription: string;
  id: number;
  topicLastTaught: string;
}

export interface SchoolCollege {
  academicOrgCode: string;
  academicGroupCode: string;
  shortDescription: string;
  formalDescription: string;
  uddsCode?: any;
  schoolCollegeURI: string;
}

export interface Subject {
  termCode: string;
  subjectCode: string;
  description: string;
  shortDescription: string;
  formalDescription: string;
  undergraduateCatalogURI: string;
  graduateCatalogURI?: any;
  departmentURI: string;
  uddsFundingSource: string;
  schoolCollege: SchoolCollege;
  footnotes: string[];
  departmentOwnerAcademicOrgCode: string;
}

export interface AddConsent {
  code: string;
  description: string;
}

export interface DropConsent {
  code: string;
  description: string;
}

export interface Building3 {
  buildingCode: string;
  buildingName: string;
  streetAddress: string;
  latitude: number;
  longitude: number;
}

export interface ClassMeeting2 {
  meetingOrExamNumber: string;
  meetingType: string;
  meetingTimeStart: number;
  meetingTimeEnd: number;
  meetingDays: string;
  meetingDaysList: string[];
  building: Building3;
  room: string;
  examDate?: number;
}

export interface ClassAttribute {
  attributeCode: string;
  attributeDisplayName: string;
  valueCode: string;
  valueShortName: string;
  valueDescription: string;
}

export interface ClassUniqueId3 {
  termCode: string;
  classNumber: number;
}

export interface EnrollmentStatus {
  classUniqueId: ClassUniqueId3;
  capacity: number;
  currentlyEnrolled: number;
  waitlistCapacity: number;
  waitlistCurrentSize: number;
  openSeats: number;
  openWaitlistSpots: number;
  aggregateCapacity?: any;
  aggregateCurrentlyEnrolled?: any;
  aggregateWaitlistCapacity?: any;
  aggregateWaitlistCurrentSize?: any;
}

export interface ClassUniqueId4 {
  termCode: string;
  classNumber: number;
}

export interface Textbook {
  title: string;
  isbn: string;
  publisher: string;
  author: string;
  year: string;
  edition: string;
  materialRequirement: string;
  notes?: any;
}

export interface ClassMaterial {
  classUniqueId: ClassUniqueId4;
  materialsDefined: boolean;
  noMaterialsInstructorMessage?: any;
  sectionNotes: string;
  lastUpdate: number;
  relatedUrls: any[];
  textbooks: Textbook[];
  otherMaterials: any[];
}

export interface Name {
  first: string;
  middle?: any;
  last: string;
  legalFirst?: any;
  legalMiddle?: any;
}

export interface Instructor {
  emplid: string;
  pvi: string;
  name: Name;
  email: string;
  netid: string;
  campusid?: any;
  office365PrimaryEmail?: any;
}

export interface Name2 {
  first: string;
  middle?: any;
  last: string;
  legalFirst?: any;
  legalMiddle?: any;
}

export interface PersonAttributes {
  emplid: string;
  pvi: string;
  name: Name2;
  email: string;
  netid: string;
  campusid?: any;
  office365PrimaryEmail?: any;
}

export interface Instructor2 {
  personAttributes: PersonAttributes;
}

export interface Section {
  classUniqueId: ClassUniqueId2;
  published: boolean;
  topic: Topic;
  startDate: number;
  endDate: number;
  active: boolean;
  sessionCode: string;
  subject: Subject;
  catalogNumber: string;
  courseId: string;
  type: string;
  sectionNumber: string;
  honors?: any;
  comB: boolean;
  gradedComponent: boolean;
  instructionMode: string;
  addConsent: AddConsent;
  dropConsent: DropConsent;
  crossListing?: any;
  classMeetings: ClassMeeting2[];
  classAttributes: ClassAttribute[];
  enrollmentStatus: EnrollmentStatus;
  footnotes: string[];
  classMaterials: ClassMaterial[];
  instructors: Instructor[];
  instructor: Instructor2;
}

export interface EnrollmentOptions {
  classPermissionNumberNeeded: boolean;
  relatedClasses: any[];
  waitlist: boolean;
  relatedClassNumber: boolean;
}

export interface ClassUniqueId5 {
  termCode: string;
  classNumber: number;
}

export interface EnrollmentStatus2 {
  classUniqueId: ClassUniqueId5;
  capacity: number;
  currentlyEnrolled: number;
  waitlistCapacity: number;
  waitlistCurrentSize: number;
  openSeats: number;
  openWaitlistSpots: number;
  aggregateCapacity?: any;
  aggregateCurrentlyEnrolled?: any;
  aggregateWaitlistCapacity?: any;
  aggregateWaitlistCurrentSize?: any;
}

export interface MeetingMap {
  monday: boolean;
  tuesday: boolean;
  wednesday: boolean;
  thursday: boolean;
  friday: boolean;
  saturday: boolean;
  sunday: boolean;
}

export interface CatalogRequirementGroup {
  code: string;
  description: string;
}

export interface ClassAssociationRequirementGroup {
  code: string;
  description: string;
  alsoUseCatalogRequisite: boolean;
}

export interface EnrollmentRequirementGroups {
  catalogRequirementGroups: CatalogRequirementGroup[];
  classAssociationRequirementGroups: ClassAssociationRequirementGroup[];
}

export interface Topic2 {
  shortDescription: string;
  longDescription: string;
  id: number;
  topicLastTaught: string;
}

export interface RootObject {
  id: string;
  termCode: string;
  subjectCode: string;
  courseId: string;
  catalogNumber: string;
  enrollmentClassNumber: number;
  packageEnrollmentStatus: PackageEnrollmentStatus;
  creditRange: string;
  classMeetings: ClassMeeting[];
  nestedClassMeetings: NestedClassMeeting[];
  instructorProvidedClassDetails: InstructorProvidedClassDetails;
  published: boolean;
  classPermissionNumberEnabled: boolean;
  sections: Section[];
  enrollmentOptions: EnrollmentOptions;
  lastUpdated: number;
  enrollmentStatus: EnrollmentStatus2;
  meetingMap: MeetingMap;
  onlineOnly: boolean;
  enrollmentRequirementGroups: EnrollmentRequirementGroups;
  topic: Topic2;
  docId: string;
}
