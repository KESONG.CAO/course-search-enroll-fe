import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent as DegreePlannerHeaderComponent } from '@degree-planner/header/header.component';
import { UnknownRouteComponent } from './components/unknown-route.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'search',
    pathMatch: 'full',
  },
  {
    path: 'search',
    data: {
      name: 'Course Search',
      jumpLinks: [
        { href: '/search#maincontent', text: 'Skip to main content' },
      ],
    },
    loadChildren: async () => {
      const { SearchModule } = await import('@search/search.module');
      return SearchModule;
    },
  },
  {
    path: 'my-courses',
    data: {
      name: 'My Courses',
      jumpLinks: [
        { href: '/my-courses#maincontent', text: 'Skip to main content' },
      ],
    },
    loadChildren: async () => {
      const { MyCoursesModule } = await import('@my-courses/my-courses.module');
      return MyCoursesModule;
    },
  },
  {
    path: 'scheduler',
    data: {
      name: 'Scheduler',
      jumpLinks: [
        { href: '/scheduler#maincontent', text: 'Skip to main content' },
      ],
    },
    loadChildren: async () => {
      const { SchedulerModule } = await import('@scheduler/scheduler.module');
      return SchedulerModule;
    },
  },
  {
    path: 'degree-planner',
    data: {
      name: 'Degree Planner',
      jumpLinks: [
        {
          href: '/degree-planner#maincontent',
          text: 'Skip to main content',
        },
        {
          href: '/degree-planner#utilityMenu',
          text: 'Skip to utility menu',
        },
      ],
    },
    children: [
      {
        path: '',
        loadChildren: async () => {
          // prettier-ignore
          const { DegreePlannerModule } = await import('@degree-planner/degree-planner.module');
          return DegreePlannerModule;
        },
      },
      {
        path: '',
        component: DegreePlannerHeaderComponent,
        outlet: 'header-buttons',
      },
    ],
  },
  {
    path: 'dars',
    data: {
      name: 'Degree Audit (DARS)',
      scrollStrategy: 'normal',
      jumpLinks: [
        { href: '/dars/#dars-container', text: 'Skip to main content' },
      ],
    },
    loadChildren: async () => {
      const { DARSModule } = await import('@dars/dars.module');
      return DARSModule;
    },
  },
  {
    path: '**',
    component: UnknownRouteComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      anchorScrolling: 'enabled',
      relativeLinkResolution: 'legacy',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
