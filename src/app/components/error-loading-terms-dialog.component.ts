import { Component } from '@angular/core';

@Component({
  template: `
    <cse-dialog headline="Unable to load data">
      <cse-dialog-body class="cse-typeset">
        <cse-bucky-emote emote="sad"></cse-bucky-emote>
        <p style="margin-top:1rem">
          There was an error loading the current terms and subjects. This issue
          was most likely caused by a network interruption or an app outage.
        </p>
        <p>
          Please try reloading the app. If the issue persists, you can report an
          outage
          <a href="https://outages.doit.wisc.edu" target="_blank">here</a>.
        </p>
      </cse-dialog-body>
      <cse-dialog-actions>
        <a
          mat-stroked-button
          href="https://outages.doit.wisc.edu"
          target="_blank"
        >
          Outages
        </a>
        <div class="spacer"></div>
        <button
          mat-raised-button
          color="primary"
          (click)="onReload()"
          cdkFocusinitial
        >
          Reload
        </button>
      </cse-dialog-actions>
    </cse-dialog>
  `,
})
export class ErrorLoadingTermsDialogComponent {
  onReload(): void {
    window.location.reload();
  }
}
