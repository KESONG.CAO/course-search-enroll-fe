import { Component } from '@angular/core';

@Component({
  selector: 'cse-help-dialog',
  template: `
    <cse-dialog headline="Need more help?">
      <cse-dialog-body class="cse-typeset">
        <p>
          <a href="https://it.wisc.edu/help/" target="_blank">Support</a>
        </p>
        <p>
          <a
            href="https://kb.wisc.edu/helpdesk/search.php?q=&cat=8136"
            target="_blank"
            >Frequently asked questions</a
          >
        </p>
        <p>
          <a
            href="https://kb.wisc.edu/registrar/page.php?id=86702"
            target="_blank"
            >Recent changes</a
          >
        </p>
      </cse-dialog-body>
      <cse-dialog-actions>
        <div class="spacer"></div>
        <button mat-raised-button color="primary" mat-dialog-close>
          Close
        </button>
      </cse-dialog-actions>
    </cse-dialog>
  `,
})
export class HelpDialogComponent {}
