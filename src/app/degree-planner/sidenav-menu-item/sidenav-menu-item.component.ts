import { Component, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, Subscription } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { State } from '@app/degree-planner/store/state';
import { Store, select } from '@ngrx/store';
import { createPlan } from '@app/degree-planner/store/actions/plan.actions';
import * as selectors from '@app/degree-planner/store/selectors';
import * as uiActions from '../store/actions/ui.actions';
import { isntUndefined } from '@app/core/utils';
import { prefs as prefSelectors } from '@app/selectors';
import { DialogService } from '@app/shared/services/dialog.service';
import { AddPlanDialogComponent } from '../dialogs/add-plan-dialog.component';

@Component({
  selector: 'cse-sidenav-menu-item',
  templateUrl: './sidenav-menu-item.component.html',
  styleUrls: ['./sidenav-menu-item.component.scss'],
})
export class SidenavMenuItemComponent implements OnDestroy {
  public inputForm: FormGroup;
  public yearCodes$: Observable<string[]>;
  public activeRoadmapId: Subscription;
  public planId: number;
  public showGrades$: Observable<boolean>;
  constructor(
    private store: Store<State>,
    private dialog2: DialogService,
    private snackBar: MatSnackBar,
  ) {
    this.activeRoadmapId = this.store
      .pipe(
        select(selectors.selectVisibleDegreePlan),
        filter(isntUndefined),
        map(plan => plan.roadmapId),
      )
      .subscribe(planId => {
        this.planId = planId;
      });
    this.showGrades$ = this.store
      .select(prefSelectors.getKey, 'degreePlannerGradesVisibility')
      .pipe(map(val => val === true));
  }
  // Unsubscribe from subs to prevent memeory leaks
  public ngOnDestroy() {
    this.activeRoadmapId.unsubscribe();
  }

  public onAddAcademicYear() {
    this.store.dispatch(uiActions.addAcademicYear());
    this.snackBar.open('New academic year has been created');
  }

  public onCreatePlanClick() {
    this.dialog2.small(AddPlanDialogComponent).then(name => {
      if (typeof name === 'string' && name.length > 0) {
        this.store.dispatch(createPlan({ name, primary: false }));
      }
    });
  }
}
