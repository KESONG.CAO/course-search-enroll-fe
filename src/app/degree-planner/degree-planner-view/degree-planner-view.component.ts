import {
  filter,
  map,
  distinctUntilChanged,
  tap,
  delay,
  take,
} from 'rxjs/operators';
import { OnInit, ViewChild, OnDestroy, Component } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { MatSelectChange } from '@angular/material/select';
import { MatSidenavContent } from '@angular/material/sidenav';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DegreePlan } from '@app/core/models/degree-plan';
import { Year } from '@app/core/models/year';
import * as selectors from '@app/degree-planner/store/selectors';
import * as utils from '@app/degree-planner/shared/utils';
import * as planActions from '@app/degree-planner/store/actions/plan.actions';
import * as uiActions from '../store/actions/ui.actions';
import { YearCode, TermCode } from '@app/types/terms';
import { IE11WarningDialogComponent } from '../dialogs/ie11-warning-dialog/ie11-warning-dialog.component';
import { Alert } from '@app/core/models/alert';
import { Actions, ofType } from '@ngrx/effects';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { isntUndefined } from '@app/core/utils';
import { prefs as prefActions } from '@app/actions';
import { prefs as prefSelectors } from '@app/selectors';
import { State } from '@app/degree-planner/store/state';
import { DialogService } from '@app/shared/services/dialog.service';
import { AddPlanDialogComponent } from '../dialogs/add-plan-dialog.component';
import { RenamePlanDialogComponent } from '../dialogs/rename-plan-dialog.component';
import { environment } from '@env/environment';
import { Router } from '@angular/router';

// From: https://stackoverflow.com/a/21825207
const isIE11 =
  !!(window as any).MSInputMethodContext && !!(document as any).documentMode;

@Component({
  selector: 'cse-degree-planner',
  templateUrl: './degree-planner-view.component.html',
  styleUrls: ['./degree-planner-view.component.scss'],
})
export class DegreePlannerViewComponent implements OnInit, OnDestroy {
  @ViewChild('degreePlanWrapper')
  public degreePlanWrapper: MatSidenavContent | null;

  public termsByAcademicYear: Object;
  public mobileView: MediaQueryList;
  public coursesData$: any;
  public showGrades$: Observable<boolean>;
  public degreePlan$: Observable<DegreePlan | undefined>;
  public allDegreePlans$: Observable<ReadonlyArray<DegreePlan>>;
  public termsByYear$: Observable<ReadonlyArray<Year>>;
  public yearCodes$: Observable<ReadonlyArray<YearCode>>;
  public isCourseSearchOpen$: Observable<boolean>;
  public isLoadingPlan$: Observable<boolean>;
  public isSidenavOpen$: Observable<boolean>;
  public version: string;
  public alerts$: Observable<Alert[]>;

  private addAcademicYearSubscription: Subscription;

  constructor(
    private actions$: Actions,
    private store: Store<State>,
    public mediaMatcher: MediaMatcher,
    private dialog: DialogService,
    private snackBar: MatSnackBar,
    private announcer: LiveAnnouncer,
    router: Router,
  ) {
    this.mobileView = mediaMatcher.matchMedia('(max-width: 959px)');
    this.version = environment.version;

    const navState = router.getCurrentNavigation()?.extras?.state;
    const showPlan: unknown = navState?.showPlan;
    if (typeof showPlan === 'number') {
      this.store.dispatch(
        planActions.switchPlan({
          newVisibleRoadmapId: showPlan,
        }),
      );
    }
  }

  public announce(btn) {
    btn === 'expand'
      ? this.announcer.announce('Expanded All Academic Years', 'assertive')
      : this.announcer.announce('Collapsed All Academic Years', 'assertive');
  }

  public ngOnInit() {
    this.store.dispatch(planActions.initialLoadRequest());

    this.degreePlan$ = this.store.pipe(
      delay(0), // Stops annoying value changed js error
      select(selectors.selectVisibleDegreePlan),
      filter(isntUndefined),
    );

    this.showGrades$ = this.store
      .select(prefSelectors.getKey, 'degreePlannerGradesVisibility')
      .pipe(map(val => (val === false ? false : true)));

    this.allDegreePlans$ = this.store.pipe(
      select(selectors.selectAllDegreePlans),
    );

    // Get observable for the search open state
    this.isCourseSearchOpen$ = this.store.pipe(
      select(selectors.isCourseSearchOpen),
    );

    this.isLoadingPlan$ = this.store.pipe(select(selectors.isLoadingPlan));

    this.isSidenavOpen$ = this.store.pipe(select(selectors.isSidenavOpen)).pipe(
      map(isSidenavOpen => {
        if (isSidenavOpen === 'defer') {
          return !this.mobileView.matches;
        }

        return isSidenavOpen;
      }),
    );

    this.yearCodes$ = this.store.pipe(
      select(selectors.selectAllVisibleYears),
      map(years => Object.keys(years).sort()),
      distinctUntilChanged(utils.compareStringArrays),
      map(ycs => ycs.map(YearCode.decodeOrThrow)),
    );

    this.store
      .select(prefSelectors.getKey, 'degreePlannerHasDismissedIEWarning')
      .pipe(map(val => val === true))
      .toPromise()
      .then(hasDismissedIEWarning => {
        if (isIE11 && hasDismissedIEWarning) {
          return this.dialog.small(IE11WarningDialogComponent).then(() => {
            this.store.dispatch(
              prefActions.setKey({
                key: 'degreePlannerHasDismissedIEWarning',
                value: true,
              }),
            );
          });
        }
      });

    this.alerts$ = this.store.select(selectors.alerts);

    this.addAcademicYearSubscription = this.actions$
      .pipe(
        ofType(uiActions.addAcademicYear),
        delay(20), // The delay makes sure the scrolling takes place *after* the academic year has been rendered
        tap(() => {
          if (this.degreePlanWrapper) {
            this.degreePlanWrapper.scrollTo({ bottom: 0 });
          }
        }),
      )
      .subscribe();
  }

  public ngOnDestroy() {
    this.addAcademicYearSubscription.unsubscribe();
  }

  public onDismissAlert(key: string) {
    this.store.dispatch(uiActions.dismissAlert({ key }));
  }

  public openSidenav() {
    this.store.dispatch(uiActions.openSidenav());
  }

  public closeSidenav() {
    this.store.dispatch(uiActions.closeSidenav());
  }

  public handleDegreePlanChange(event: MatSelectChange): void {
    if (typeof event.value === 'number') {
      this.store.dispatch(
        planActions.switchPlan({ newVisibleRoadmapId: event.value }),
      );
    }
  }

  public onCreatePlanClick() {
    this.dialog.small(AddPlanDialogComponent).then(name => {
      if (typeof name === 'string' && name.length > 0) {
        this.store.dispatch(planActions.createPlan({ name, primary: false }));
      }
    });
  }

  public onAddAcademicYear() {
    this.store.dispatch(uiActions.addAcademicYear());
    this.snackBar.open('New academic year has been created');
  }

  public onRenamePlanClick(currentPlan: DegreePlan) {
    this.dialog
      .small(RenamePlanDialogComponent, currentPlan.name)
      .then(newName => {
        console.log({ newName });
        if (typeof newName === 'string' && newName.length > 0) {
          this.store.dispatch(
            planActions.changePlanName({
              roadmapId: currentPlan.roadmapId,
              newName,
            }),
          );
        }
      });
  }

  public async onMakePrimayClick(_currentPlan: DegreePlan) {
    const active = await this.store
      .select(selectors.getActiveTermCodes)
      .pipe(take(1))
      .toPromise();

    const shouldMakePrimary = await this.dialog.confirm({
      headline: 'Are you sure?',
      reasoning: [
        `This will change your primary plan and replace the current ` +
          `courses in your cart with the courses in this plan's ` +
          `${TermCode.describe(active[0])} term.`,
      ],
      affirmative: 'Change plan',
    });

    if (shouldMakePrimary) {
      this.store.dispatch(planActions.makePlanPrimary());
    }
  }

  public async onDeletePlanClick(currentPlan: DegreePlan) {
    if (currentPlan.primary) {
      this.snackBar.open('The primary degree plan cannot be deleted');
      return;
    }

    const shouldDelete = await this.dialog.confirm({
      headline: 'Are you sure?',
      reasoning: [
        'This will delete this plan and course information related to this plan.',
      ],
      affirmative: 'Delete',
    });

    if (shouldDelete) {
      const primaryPlan = (
        await this.store
          .select(selectors.selectAllDegreePlans)
          .pipe(take(1))
          .toPromise()
      ).find(plan => plan.primary);

      this.store.dispatch(
        planActions.deletePlan({
          roadmapId: currentPlan.roadmapId,
        }),
      );
      this.store.dispatch(
        planActions.switchPlan({
          newVisibleRoadmapId: primaryPlan!.roadmapId,
        }),
      );
    }
  }

  public getTermDropZone() {
    const termCodes = ['favoriteCourse-dropZone'];

    for (const yearCode in this.termsByAcademicYear) {
      if (this.termsByAcademicYear[yearCode]) {
        const year = this.termsByAcademicYear[yearCode];
        for (const termKey in year.terms) {
          if (year.terms[termKey]) {
            const term = year.terms[termKey];
            termCodes.push('term-' + term.termCode);
          }
        }
      }
    }

    return termCodes;
  }

  public changeGradeVisibility(event: MatSlideToggleChange) {
    this.store.dispatch(
      prefActions.setKey({
        key: 'degreePlannerGradesVisibility',
        value: event.checked,
      }),
    );
  }

  public closeCourseSearch() {
    this.store.dispatch(uiActions.closeCourseSearch());
  }

  public trackYearCodes(_index: number, yearCode: YearCode) {
    return YearCode.encode(yearCode);
  }

  public toggleAllYears(expand: boolean) {
    const event = expand
      ? uiActions.expandAcademicYear({})
      : uiActions.collapseAcademicYear({});
    this.store.dispatch(event);
  }
}
