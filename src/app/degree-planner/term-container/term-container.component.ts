import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Observable, Subscription } from 'rxjs';
import {
  filter,
  map,
  distinctUntilChanged,
  pairwise,
  withLatestFrom,
} from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { DegreePlannerState } from '@app/degree-planner/store/state';
import * as uiActions from '@app/degree-planner/store/actions/ui.actions';
import * as actions from '@app/degree-planner/store/actions/course.actions';
import * as selectors from '@app/degree-planner/store/selectors';
import { PlannedTerm, PlannedTermNote } from '@app/core/models/planned-term';
import { Course, pickCreditAmountFromCourse } from '@app/core/models/course';
import * as utils from '@app/degree-planner/shared/utils';
import { TermCode, Era, SUMMER, WithEra } from '@app/types/terms';
import { MediaMatcher } from '@angular/cdk/layout';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import {
  flatFilter,
  ifSuccessThenUnwrap,
  isntUndefined,
  unsubscribeAll,
} from '@app/core/utils';
import { DialogService } from '@app/shared/services/dialog.service';
import { AddNoteDialogComponent } from '../dialogs/add-note-dialog.component';
import * as noteActions from '../store/actions/note.actions';
import { EditNoteDialogComponent } from '../dialogs/edit-note-dialog.component';
import * as globalSelectors from '@app/selectors';
import * as s from '@app/types/schema';

const sumCredits = (
  courses: ReadonlyArray<{ creditMin?: number; creditMax?: number }>,
) => {
  const min = courses.reduce((sum, course) => {
    return sum + (course.creditMin !== undefined ? course.creditMin : 0);
  }, 0);

  const max = courses.reduce((sum, course) => {
    return sum + (course.creditMax !== undefined ? course.creditMax : 0);
  }, 0);

  return { min, max };
};

// Both the summer and fall/spring undergrad credit limits are INCLUSIVE.
const SUMMER_CREDIT_LIMIT = 12;
const FALL_SPRING_CREDIT_LIMIT = 18;

const maximumAllowedCreditsForTerm = (termCode: TermCode) => {
  switch (termCode.term) {
    case SUMMER:
      return SUMMER_CREDIT_LIMIT;
    default:
      return FALL_SPRING_CREDIT_LIMIT;
  }
};

// If the user is an undergrad, their student info
// object should satisfy these constraints:
const undergradSchema = s.object({
  primaryCareer: s.object({
    careerCode: s.constant('UGRD'),
  }),
});

@Component({
  selector: 'cse-term-container',
  templateUrl: './term-container.component.html',
  styleUrls: ['./term-container.component.scss'],
})
export class TermContainerComponent implements OnInit, OnDestroy {
  @Input() termCode: WithEra;

  public term$: Observable<PlannedTerm>;
  public note$: Observable<PlannedTermNote | undefined>;
  public dropZoneIds$: Observable<string[]>;
  public tooManyCredits$: Observable<boolean>;

  public activeTermHasNotOffered: boolean;
  // List of courses pulled for the Observable
  public isPrimaryPlan: boolean;
  public plannedCourses: ReadonlyArray<Course>;
  public enrolledCourses: ReadonlyArray<Course>;
  public transferredCourses: ReadonlyArray<Course>;
  public transferredCredits: number;
  public hasItemDraggedOver: boolean;
  public plannedCredits: string;
  public enrolledCredits: number;
  public visibleCredits: 'enrolled' | 'planned' | 'transferred';
  public courseNotOfferedInTerm: ReadonlyArray<Course>;
  public mobileView: MediaQueryList;
  public maxAllowedCredits: number;
  private subscriptions: Subscription[];

  private isUndergrad$ = this.store
    .select(globalSelectors.student.getAll)
    .pipe(ifSuccessThenUnwrap(), map(undergradSchema.matches));

  constructor(
    private dialog: DialogService,
    private store: Store<{ degreePlanner: DegreePlannerState }>,
    private announcer: LiveAnnouncer,
    mediaMatcher: MediaMatcher,
  ) {
    this.mobileView = mediaMatcher.matchMedia('(max-width: 900px)');
  }

  public ngOnInit() {
    this.hasItemDraggedOver = false;

    this.term$ = this.store.pipe(
      select(selectors.selectVisibleTerm, { termCode: this.termCode }),
      filter(isntUndefined),
      distinctUntilChanged(),
    );

    this.tooManyCredits$ = this.term$.pipe(
      withLatestFrom(this.isUndergrad$),
      map(([term, isUndergrad]) => {
        if (isUndergrad) {
          const credits = sumCredits(term.plannedCourses);
          this.maxAllowedCredits = maximumAllowedCreditsForTerm(term.termCode);
          return credits.min >= this.maxAllowedCredits;
        } else {
          return false;
        }
      }),
    );

    this.subscriptions = [
      this.store
        .pipe(
          select(selectors.selectVisibleDegreePlan),
          filter(isntUndefined),
          map(plan => plan.primary),
        )
        .subscribe(plan => (this.isPrimaryPlan = plan)),

      this.term$
        .pipe(
          // Condition #1
          flatFilter(() => this.isUndergrad$),

          // Don't compare two terms if they are different terms
          pairwise(),
          filter(([prev, curr]) => {
            return (
              prev.roadmapId === curr.roadmapId &&
              TermCode.equals(prev.termCode, curr.termCode)
            );
          }),
        )
        .subscribe(([prev, curr]) => {
          const prevCredits = sumCredits(prev.plannedCourses);
          const currCredits = sumCredits(curr.plannedCourses);
          const maxAllowedCredits = maximumAllowedCreditsForTerm(curr.termCode);
          const prevWasOverLimit = prevCredits.min >= maxAllowedCredits;
          const currIsUnderLimit = currCredits.min < maxAllowedCredits;
          const currHasFewerCreditsThanPrev = currCredits.min < prevCredits.min;

          if (prevWasOverLimit || currHasFewerCreditsThanPrev) {
            // Failed condition #2
            return;
          }

          if (currIsUnderLimit) {
            // Failed condition #3
            return;
          }

          this.dialog.acknowledge({
            headline: 'Credit overload',
            reasoning: [
              `Undergraduate students who wish to take more than ${maxAllowedCredits} ` +
                `during the ${TermCode.lowerTermName(
                  curr.termCode,
                )} semester ` +
                `must receive approval from their School or College.`,
            ],
            moreInfo: {
              label: 'More information',
              href: 'https://registrar.wisc.edu/enrollment-related-info',
            },
          });
        }),

      this.term$.subscribe(term => {
        this.plannedCourses = term.plannedCourses;
        this.plannedCredits = this.sumCreditRange(term.plannedCourses);

        this.enrolledCourses = term.enrolledCourses;
        this.enrolledCredits = this.sumFixedCredits(term.enrolledCourses);

        this.transferredCourses = term.transferredCourses;
        this.transferredCredits = this.sumFixedCredits(term.transferredCourses);

        if (term.termCode.era === Era.Past) {
          this.visibleCredits = 'enrolled';
        } else if (term.termCode.era === Era.Active) {
          if (this.enrolledCourses.length === 0) {
            this.visibleCredits = 'planned';
          } else {
            this.visibleCredits = 'enrolled';
          }
        } else {
          this.visibleCredits = 'planned';
        }
      }),
    ];

    this.note$ = this.term$.pipe(
      map(term => term.note),
      distinctUntilChanged(),
    );

    this.dropZoneIds$ = this.store.pipe(
      select(selectors.selectAllVisibleYears),
      utils.yearsToDropZoneIds(),
      distinctUntilChanged(utils.compareStringArrays),
    );
  }

  ngOnDestroy() {
    unsubscribeAll(this.subscriptions);
  }

  openNotesDialog(note?: PlannedTermNote) {
    if (note === undefined || note.isLoaded) {
      const termCode = this.termCode;
      if (note) {
        this.dialog.small(EditNoteDialogComponent, note.text).then(response => {
          if (response && typeof response === 'object') {
            if (response['delete'] === true) {
              this.store.dispatch(
                noteActions.deleteNote({ termCode, noteId: note.id }),
              );
            } else if (typeof response['save'] === 'string') {
              this.store.dispatch(
                noteActions.writeNote({ termCode, noteText: response['save'] }),
              );
            }
          }
        });
      } else {
        this.dialog.small(AddNoteDialogComponent).then(noteText => {
          if (typeof noteText === 'string' && noteText.length > 0) {
            this.store.dispatch(noteActions.writeNote({ termCode, noteText }));
          }
        });
      }
    }
  }

  openCourseSearch() {
    this.store.dispatch(
      uiActions.openCourseSearch({ termCode: this.termCode }),
    );
  }

  changeVisibleCredits(event) {
    switch (event.index) {
      case 0:
        this.visibleCredits = 'enrolled';
        break;
      case 1:
        this.visibleCredits = 'planned';
        break;
      default:
        this.visibleCredits = 'transferred';
    }
  }

  startDrag(event) {
    const touchedCourse =
      event.source.data.subjectDescription +
      ' ' +
      event.source.data.catalogNumber;
    this.announcer.announce(`Dragging ${touchedCourse} course`, 'assertive');
  }

  drop(event: CdkDragDrop<WithEra>) {
    const newContainer = event.container.id;
    const previousContainer = event.previousContainer.id;
    const { courseId } = event.item.data as Course;
    const isCourseInPlannedCourses = this.plannedCourses.some(
      course => course.courseId === courseId,
    );

    this.hasItemDraggedOver = false;
    this.announcer.announce('Dropped course', 'assertive');

    if (newContainer !== previousContainer && isCourseInPlannedCourses) {
      this.dialog.acknowledge({
        headline: `Can't add course to term`,
        reasoning: ['This course already exists in the selected term'],
      });
      return;
    }

    if (newContainer === previousContainer) {
      const newIndex = event.currentIndex;
      const termCode = event.container.data;
      const { id: recordId } = event.item.data as Course;

      if (recordId !== null) {
        const action = actions.moveCourseInsideTerm({
          termCode,
          recordId,
          newIndex,
        });
        this.store.dispatch(action);
      }
    } else if (previousContainer.indexOf('term-') === 0) {
      // If moving from term to term

      // Get the pervious and new term code, and the record ID
      const from = event.previousContainer.data;
      const to = event.container.data;
      // FIXME: if `event.item.data` is a Course, the `id` property could be null
      const { id, courseId, subjectCode } = event.item.data;
      const newIndex = event.currentIndex;
      const { classNumber } = event.item.data as Course;

      if (classNumber !== null) {
        // If moving course with packages to future term
        this.dialog
          .confirm({
            headline: 'Are you sure?',
            reasoning: [
              `Moving this course to a future term will remove your selected section`,
            ],
            affirmative: 'Move course',
          })
          .then(canMove => {
            if (canMove) {
              this.store.dispatch(
                actions.moveCourseBetweenTerms({
                  to,
                  from,
                  id,
                  newIndex,
                  courseId,
                  subjectCode,
                  credits: pickCreditAmountFromCourse(event.item.data),
                }),
              );
            }
          });
      } else {
        // Dispatch a new change request
        this.store.dispatch(
          actions.moveCourseBetweenTerms({
            to,
            from,
            id,
            newIndex,
            courseId,
            subjectCode,
            credits: pickCreditAmountFromCourse(event.item.data),
          }),
        );
      }
    } else if (previousContainer === 'saved-courses') {
      // If moving from saved courses to term

      // Get the term code from the new term dropzone's ID
      const termCode = event.container.data;
      const newIndex = event.currentIndex;
      // Pull the course data from the moved item
      const { subjectCode, courseId, title, catalogNumber } = event.item.data;
      this.store.dispatch(
        actions.addCourse({
          courseId,
          termCode,
          subjectCode,
          title,
          catalogNumber,
          newIndex,
        }),
      );
      this.store.dispatch(
        actions.removeSaveForLater({ subjectCode, courseId }),
      );
    } else if (
      previousContainer === 'queried-courses-list' &&
      newContainer.indexOf('term-') === 0
    ) {
      const termCode = event.container.data;
      const newIndex = event.currentIndex;
      this.store.dispatch(
        actions.addCourse({
          courseId: event.item.data.courseId,
          termCode,
          subjectCode: event.item.data.subjectCode,
          title: event.item.data.title,
          catalogNumber: event.item.data.catalogNumber,
          newIndex,
        }),
      );

      if (this.mobileView.matches) {
        this.store.dispatch(uiActions.closeCourseSearch());
      }
    }
  }

  dragEnter(_event, _item) {
    this.hasItemDraggedOver = true;
  }

  dragExit(_event, _item) {
    this.hasItemDraggedOver = false;
  }

  sumFixedCredits(courses: ReadonlyArray<Course>): number {
    return courses.reduce(
      (sum, course) => sum + (course.credits ? course.credits : 0),
      0,
    );
  }

  sumCreditRange(courses: ReadonlyArray<Course>): string {
    const credits = { min: 0, max: 0 };
    courses.forEach(course => {
      if (course.creditMin !== undefined && course.creditMax !== undefined) {
        credits.min = credits.min + course.creditMin;
        credits.max = credits.max + course.creditMax;
      }
    });

    return credits.min === credits.max
      ? credits.min.toString()
      : `${credits.min}-${credits.max}`;
  }
}
