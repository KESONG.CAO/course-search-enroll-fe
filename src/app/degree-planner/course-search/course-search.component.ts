import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, map, mergeMap, startWith } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { FormControl } from '@angular/forms';
import { Era, Subject, TermCode, TermCodeOrZero, ZERO } from '@app/types/terms';
import * as viewSelectors from '@degree-planner/store/selectors';
import * as globalSelectors from '@app/selectors';
import { ifSuccessThenUnwrap, unsubscribeAll } from '@app/core/utils';
import { GlobalState } from '@app/state';
import { fuzzy } from '@app/search/fuzzy-search';
import { failed, Loadable, LOADING, success } from 'loadable.ts';
import { ApiService, SearchParameters } from '@app/services/api.service';
import { Course } from '@app/core/models/course';
import { CdkDragStart } from '@angular/cdk/drag-drop';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Breakpoints } from '@app/shared/breakpoints';
import { compareStringArrays, yearsToDropZoneIds } from '../shared/utils';

@Component({
  selector: 'cse-course-search',
  templateUrl: './course-search.component.html',
  styleUrls: ['./course-search.component.scss'],
})
export class CourseSearchComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];

  public termCodeCtrl = new FormControl(null);
  public subjectFilterCtrl = new FormControl(null);
  public subjectCtrl = new FormControl(null);
  public keywordsCtrl = new FormControl(null);

  public termLabel$ = this.termCodeCtrl.valueChanges.pipe(
    startWith(null),
    map(termCode => (termCode ? 'Term' : 'All terms')),
  );

  public activeTermCodes$ = this.store
    .select(globalSelectors.terms.getActiveTermCodes)
    .pipe(ifSuccessThenUnwrap());

  public subjectLabel$ = this.subjectCtrl.valueChanges.pipe(
    startWith(null),
    map(subject => (subject ? 'Subject' : 'All subjects')),
  );

  public subjectsForTerm$ = this.termCodeCtrl.valueChanges.pipe(
    startWith(null),
    map(termCode => (TermCode.is(termCode) ? termCode : ZERO)),
    mergeMap(termCode =>
      this.store.select(globalSelectors.terms.getSubjectsForTerm, termCode),
    ),
    ifSuccessThenUnwrap(),
    map(subjects => [null, ...subjects]),
  );

  public filteredSubjectsForTerm$ = combineLatest([
    this.subjectFilterCtrl.valueChanges.pipe(startWith(null)),
    this.subjectsForTerm$,
  ]).pipe(
    map(
      ([value, allSubjects]): Array<Subject | null> => {
        const filter = typeof value === 'string' ? value.trim() : '';
        if (filter === '') {
          return allSubjects;
        } else {
          return fuzzy<null | Subject>(filter, allSubjects, subjectOrNull => {
            return subjectOrNull?.description ?? 'All subjects';
          });
        }
      },
    ),
  );

  public results$ = new BehaviorSubject<Loadable<Course[]> | null>(null);

  public isMobile$: Observable<boolean>;

  public dropZoneIds$ = this.store
    .select(viewSelectors.selectAllVisibleYears)
    .pipe(yearsToDropZoneIds(), distinctUntilChanged(compareStringArrays));

  constructor(
    private store: Store<GlobalState>,
    private api: ApiService,
    private announcer: LiveAnnouncer,
    breakpointObserver: BreakpointObserver,
  ) {
    this.subscriptions.push(
      this.subjectsForTerm$.pipe(distinctUntilChanged()).subscribe(() => {
        this.subjectCtrl.setValue(null);
      }),
    );

    this.isMobile$ = breakpointObserver
      .observe(Breakpoints.Mobile)
      .pipe(map(({ matches: isMobile }) => isMobile));
  }

  ngOnInit() {
    this.subscriptions.push(
      this.store
        .select(viewSelectors.getSelectedSearchTerm)
        .subscribe(termCode => {
          if (termCode && termCode.era === Era.Active) {
            this.termCodeCtrl.setValue(termCode);
          } else {
            this.termCodeCtrl.setValue(null);
          }
        }),
    );
  }

  ngOnDestroy() {
    unsubscribeAll(this.subscriptions);
  }

  async onSubmit() {
    const termCode: TermCode | null = this.termCodeCtrl.value || null;
    const subject: Subject | null = this.subjectCtrl.value || null;
    const keywords: string | null = this.keywordsCtrl.value || null;

    this.results$.next(LOADING);

    try {
      const params = this.formInputsToParams(termCode, subject, keywords);
      const newResults = await this.api
        .postSearch(params)
        .pipe(
          map(results =>
            results.hits.map(course => {
              return ({
                ...course,
                subjectCode: course.subject.subjectCode,
              } as any) as Course;
            }),
          ),
        )
        .toPromise();
      this.results$.next(success(newResults));
    } catch {
      this.results$.next(failed(null));
    }
  }

  compareSubjects(s1: unknown, s2: unknown): boolean {
    if (s1 === s2) {
      return true;
    } else if (Subject.schema.matches(s1) && Subject.schema.matches(s2)) {
      return s1.code === s2.code;
    } else {
      return false;
    }
  }

  compareTermCodes(t1: unknown, t2: unknown): boolean {
    return (
      TermCodeOrZero.is(t1) &&
      TermCodeOrZero.is(t2) &&
      TermCodeOrZero.equals(t1, t2)
    );
  }

  startDrag(event: CdkDragStart<Course>) {
    const touchedCourse =
      event.source.data.title + ' ' + event.source.data.catalogNumber;
    this.announcer.announce(`Dragging ${touchedCourse} course`, 'assertive');
  }

  private formInputsToParams(
    termCode: TermCode | null,
    subject: Subject | null,
    keywords: string | null,
  ): SearchParameters {
    const selectedTerm = TermCodeOrZero.encode(termCode ?? ZERO);

    const queryString =
      keywords === null || keywords.trim() === '' ? '*' : keywords.trim();

    const filters: unknown[] = [];

    if (subject) {
      filters.push({ term: { 'subject.subjectCode': subject.code } });
    }

    if (termCode) {
      // We want to make sure we search for ALL classes regardless of status
      filters.push({
        has_child: {
          type: 'enrollmentPackage',
          query: {
            match: {
              'packageEnrollmentStatus.status': 'OPEN WAITLISTED CLOSED',
            },
          },
        },
      });
    }

    return {
      selectedTerm,
      queryString,
      filters,
      page: 1,
      pageSize: 100,
      sortOrder: 'SCORE',
    };
  }
}
