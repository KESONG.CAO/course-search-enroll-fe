import { createAction, props } from '@ngrx/store';
import { Note } from '@app/core/models/note';
import { TermCode } from '@app/types/terms';

export const writeNote = createAction(
  'note/writeNote',
  props<{ termCode: TermCode; noteText: string }>(),
);

export const writeNoteSuccess = createAction(
  'note/writeNoteSuccess',
  props<{ termCode: TermCode; updatedNote: Note }>(),
);

export const deleteNote = createAction(
  'note/deleteNote',
  props<{ termCode: TermCode; noteId: number }>(),
);

export const deleteNoteSuccess = createAction(
  'note/deleteNoteSuccess',
  props<{ termCode: TermCode }>(),
);

export const noteError = createAction(
  'note/noteError',
  props<{ message: string; duration: number; error: any }>(),
);
