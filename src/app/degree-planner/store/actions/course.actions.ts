import { createAction, props } from '@ngrx/store';
import { Course, CourseBase } from '@app/core/models/course';
import { TermCode, WithEra } from '@app/types/terms';

export const moveCourseInsideTerm = createAction(
  'planner/moveCourseInsideTerm',
  props<{ termCode: TermCode; recordId: number; newIndex: number }>(),
);

export const moveCourseInsideSFL = createAction(
  'planner/moveCourseInsideSFL',
  props<{ courseId: string; newIndex: number }>(),
);

export const moveCourseBetweenTerms = createAction(
  'planner/moveCourseBetweenTerms',
  props<{
    to: WithEra;
    from: TermCode;
    id: number;
    newIndex?: number;
    courseId: string;
    subjectCode: string;
    credits: number;
  }>(),
);

export const moveCourseBetweenTermsSuccess = createAction(
  'planner/moveCourseBetweenTermsSuccess',
  props<{
    to: TermCode;
    from: TermCode;
    id: number;
    newIndex?: number;
    courseId: string;
    subjectCode: string;
    studentEnrollmentStatus: CourseBase['studentEnrollmentStatus'];
  }>(),
);

export const addCourse = createAction(
  'planner/addCourse',
  props<{
    courseId: string;
    termCode: WithEra;
    subjectCode: string;
    title: string;
    catalogNumber: string;
    newIndex?: number;
  }>(),
);

export const addCourseSuccess = createAction(
  'planner/addCourseSuccess',
  props<{
    termCode: TermCode;
    course: Course;
    newIndex?: number;
  }>(),
);

export const removeCourse = createAction(
  'planner/removeCourse',
  props<{ fromTermCode: TermCode; recordId: number }>(),
);

export const removeCourseSuccess = createAction(
  'planner/removeCourseSuccess',
  props<{ fromTermCode: TermCode; recordId: number }>(),
);

export const addSaveForLater = createAction(
  'planner/addSaveForLater',
  props<{
    subjectCode: string;
    courseId: string;
    title: string;
    catalogNumber: string;
    newIndex: number;
  }>(),
);

export const addSaveForLaterSuccess = createAction(
  'planner/addSaveForLaterSuccess',
  props<{
    subjectCode: string;
    courseId: string;
    title: string;
    catalogNumber: string;
    newIndex: number;
  }>(),
);

export const removeSaveForLater = createAction(
  'planner/removeSaveForLater',
  props<{ subjectCode: string; courseId: string }>(),
);

export const removeSaveForLaterSuccess = createAction(
  'planner/removeSaveForLaterSuccess',
  props<{ subjectCode: string; courseId: string }>(),
);

export const courseError = createAction(
  'planner/courseError',
  props<{ message: string; error: any }>(),
);
