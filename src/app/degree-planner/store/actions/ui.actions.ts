import { createAction, props } from '@ngrx/store';
import { YearCode, WithEra } from '@app/types/terms';

export const toggleAcademicYear = createAction(
  'ui/toggleAcademicYear',
  props<{ yearCode: YearCode }>(),
);

export const expandAcademicYear = createAction(
  'ui/expandAcademicYear',
  props<{ yearCode?: YearCode }>(),
);

export const collapseAcademicYear = createAction(
  'ui/collapseAcademicYear',
  props<{ yearCode?: YearCode }>(),
);

export const openCourseSearch = createAction(
  'ui/openCourseSearch',
  props<{ termCode?: WithEra }>(),
);

export const closeCourseSearch = createAction('ui/closeCourseSearch');

export const toggleCourseSearch = createAction(
  'ui/toggleCourseSearch',
  props<{ termCode?: WithEra }>(),
);

export const updateSearchTermCode = createAction(
  'ui/updateSearchTermCode',
  props<{ termCode: WithEra }>(),
);

export const openSidenav = createAction('ui/openSidenav');

export const closeSidenav = createAction('ui/closeSidenav');

export const addAcademicYear = createAction('ui/addAcademicYear');

export const dismissAlert = createAction(
  'ui/dismissAlert',
  props<{ key: string }>(),
);
