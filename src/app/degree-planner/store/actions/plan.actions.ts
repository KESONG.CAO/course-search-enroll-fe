import { createAction, props } from '@ngrx/store';
import { DegreePlan } from '@app/core/models/degree-plan';
import { DegreePlannerState } from '@app/degree-planner/store/state';
import { YearMapping } from '@app/core/models/year';

export const initialLoadRequest = createAction('plan/initialLoadRequest');

export const initialLoadSuccess = createAction(
  'plan/initialLoadSuccess',
  props<{ payload: DegreePlannerState }>(),
);

export const switchPlan = createAction(
  'plan/switchPlan',
  props<{ newVisibleRoadmapId: number }>(),
);

export const switchPlanSuccess = createAction(
  'plan/switchPlanSuccess',
  props<{ visibleDegreePlan: DegreePlan; visibleYears: YearMapping }>(),
);

export const createPlan = createAction(
  'plan/createPlan',
  props<{ name: string; primary: boolean }>(),
);

export const createPlanSuccess = createAction(
  'plan/createPlanSuccess',
  props<{ newPlan: DegreePlan; newYears: YearMapping }>(),
);

export const deletePlan = createAction(
  'plan/deletePlan',
  props<{ roadmapId: number }>(),
);

export const deletePlanSuccess = createAction(
  'plan/deletePlanSuccess',
  props<{ roadmapId: number }>(),
);

export const planError = createAction(
  'plan/planError',
  props<{ message: string; duration: number; error: any }>(),
);

export const makePlanPrimary = createAction('plan/makePlanPrimary');

export const makePlanPrimarySuccess = createAction(
  'plan/makePlanPrimarySuccess',
);

export const makePlanPrimaryFailure = createAction(
  'plan/makePlanPrimaryFailure',
);

export const changePlanName = createAction(
  'plan/changePlanName',
  props<{ roadmapId: number; newName: string }>(),
);

export const changePlanNameSuccess = createAction(
  'plan/changePlanNameSuccess',
  props<{ roadmapId: number; newName: string }>(),
);

export const changePlanNameFailure = createAction(
  'plan/changePlanNameFailure',
  props<{ roadmapId: number; oldName: string }>(),
);
