import { YearMapping } from '@app/core/models/year';
import { DegreePlan } from '@app/core/models/degree-plan';
import { SavedForLaterCourse } from '@app/core/models/saved-for-later-course';
import { SubjectCodesTo, SubjectDescription } from '@app/core/models/course';
import { WithEra, TermCode } from '@app/types/terms';
import { Alert } from '@app/core/models/alert';
import { GlobalState } from '@app/state';

export interface DegreePlannerState {
  activeTermCodes: TermCode[];
  visibleDegreePlan: DegreePlan | undefined;
  visibleYears: YearMapping;
  savedForLaterCourses: Array<SavedForLaterCourse>;
  allDegreePlans: Array<DegreePlan>;
  subjectDescriptions: SubjectCodesTo<SubjectDescription>;
  search: { visible: boolean; selectedTerm?: WithEra };
  isLoadingPlan: boolean;
  isSidenavOpen: 'defer' | boolean;
  alerts: Alert[];
}

export interface State extends GlobalState {
  degreePlanner: DegreePlannerState;
}

export const INITIAL_DEGREE_PLANNER_STATE: DegreePlannerState = {
  activeTermCodes: [],
  visibleDegreePlan: undefined,
  visibleYears: {},
  savedForLaterCourses: [],
  allDegreePlans: [],
  subjectDescriptions: {},
  search: { visible: false },
  isLoadingPlan: true,
  isSidenavOpen: 'defer',
  alerts: [],
};
