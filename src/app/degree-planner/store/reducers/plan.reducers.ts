import { on, On } from '@ngrx/store';
import produce from 'immer';
import { DegreePlannerState } from '../state';
import * as actions from '../actions/plan.actions';

export const planError: On<DegreePlannerState> = on(
  actions.planError,
  state => {
    return produce(state, draft => {
      draft.isLoadingPlan = false;
    });
  },
);

export const createPlan: On<DegreePlannerState> = on(
  actions.createPlan,
  state => {
    return produce(state, draft => {
      draft.isLoadingPlan = true;
    });
  },
);

export const initialLoadSuccess: On<DegreePlannerState> = on(
  actions.initialLoadSuccess,
  (_state, action) => {
    return { ...action.payload };
  },
);

export const switchPlan: On<DegreePlannerState> = on(
  actions.switchPlan,
  (state, {}) => {
    return produce(state, draft => {
      draft.isLoadingPlan = true;
    });
  },
);

export const switchPlanSuccess: On<DegreePlannerState> = on(
  actions.switchPlanSuccess,
  (state, { visibleDegreePlan, visibleYears }) => {
    return produce(state, draft => {
      draft.isLoadingPlan = false;
      draft.visibleDegreePlan = visibleDegreePlan;
      (draft.visibleYears as any) = visibleYears;
    });
  },
);

export const createPlanSuccess: On<DegreePlannerState> = on(
  actions.createPlanSuccess,
  (state, { newPlan, newYears }) => {
    return produce(state, draft => {
      draft.visibleDegreePlan = newPlan;
      (draft.visibleYears as any) = newYears;
      draft.allDegreePlans.push(newPlan);
      draft.isLoadingPlan = false;
    });
  },
);

export const makePlanPrimarySuccess: On<DegreePlannerState> = on(
  actions.makePlanPrimarySuccess,
  (state, {}) => {
    return produce(state, draft => {
      if (!draft.visibleDegreePlan) {
        return;
      }

      draft.visibleDegreePlan.primary = true;
      draft.allDegreePlans.forEach(plan => {
        if (plan.roadmapId === draft.visibleDegreePlan?.roadmapId) {
          plan.primary = true;
        }
      });
    });
  },
);

export const changePlanNameSuccess: On<DegreePlannerState> = on(
  actions.changePlanNameSuccess,
  (state, { newName, roadmapId }) => {
    return produce(state, draft => {
      if (draft.visibleDegreePlan?.roadmapId === roadmapId) {
        draft.visibleDegreePlan.name = newName;
      }

      draft.allDegreePlans.forEach(plan => {
        if (plan.roadmapId === draft.visibleDegreePlan?.roadmapId) {
          plan.name = newName;
        }
      });
    });
  },
);

export const changePlanNameFailure: On<DegreePlannerState> = on(
  actions.changePlanNameFailure,
  (state, { oldName, roadmapId }) => {
    return produce(state, draft => {
      if (draft.visibleDegreePlan?.roadmapId === roadmapId) {
        draft.visibleDegreePlan.name = oldName;
      }

      draft.allDegreePlans.forEach(plan => {
        if (plan.roadmapId === draft.visibleDegreePlan?.roadmapId) {
          plan.name = oldName;
        }
      });
    });
  },
);

export const deletePlanSuccess: On<DegreePlannerState> = on(
  actions.deletePlanSuccess,
  (state, { roadmapId }) => {
    return produce(state, draft => {
      const planIndex = draft.allDegreePlans.findIndex(
        p => p.roadmapId === roadmapId,
      );
      draft.allDegreePlans.splice(planIndex, 1);
    });
  },
);
