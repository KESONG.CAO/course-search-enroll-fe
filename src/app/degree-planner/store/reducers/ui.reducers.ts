import { on, On } from '@ngrx/store';
import produce from 'immer';
import { DegreePlannerState } from '../state';
import * as actions from '../actions/ui.actions';
import { Era, TermCode, WithEra, YearCode } from '@app/types/terms';
import { Year } from '@app/core/models/year';
import { PlannedTerm } from '@app/core/models/planned-term';

const emptyTerm = (roadmapId: number, termCode: WithEra): PlannedTerm => {
  return {
    roadmapId,
    termCode,
    plannedCourses: [],
    enrolledCourses: [],
    transferredCourses: [],
  };
};

const emptyYear = (
  roadmapId: number,
  yearCode: YearCode,
  active: TermCode[],
): Year => {
  const { fall, spring, summer } = WithEra.fromYearCode(active, yearCode);
  return {
    yearCode,
    isExpanded: !(
      fall.era === Era.Past &&
      spring.era === Era.Past &&
      summer.era === Era.Past
    ),
    fall: emptyTerm(roadmapId, fall),
    spring: emptyTerm(roadmapId, spring),
    summer: emptyTerm(roadmapId, summer),
  };
};

export const addAcademicYear: On<DegreePlannerState> = on(
  actions.addAcademicYear,
  state => {
    return produce(state, draft => {
      const currentYearCodes = Object.keys(draft.visibleYears);
      const largestYearCode = Math.max(
        ...currentYearCodes.map(yearCode => {
          return parseInt(yearCode, 10);
        }),
      );
      const nextYearCode = YearCode.next(
        YearCode.decodeOrThrow(`${largestYearCode}`),
      );
      const nextYear = emptyYear(
        draft.visibleDegreePlan!.roadmapId,
        nextYearCode,
        draft.activeTermCodes,
      );
      (draft.visibleYears[YearCode.encode(nextYearCode)] as any) = nextYear;
    });
  },
);

export const expandAcademicYear: On<DegreePlannerState> = on(
  actions.expandAcademicYear,
  (state, { yearCode }) => {
    return produce(state, draft => {
      if (yearCode === undefined) {
        Object.values(draft.visibleYears).forEach(year => {
          year.isExpanded = true;
        });
      } else {
        draft.visibleYears[YearCode.encode(yearCode)].isExpanded = true;
      }
    });
  },
);

export const collapseAcademicYear: On<DegreePlannerState> = on(
  actions.collapseAcademicYear,
  (state, { yearCode }) => {
    return produce(state, draft => {
      if (yearCode === undefined) {
        Object.values(draft.visibleYears).forEach(year => {
          year.isExpanded = false;
        });
      } else {
        draft.visibleYears[YearCode.encode(yearCode)].isExpanded = false;
      }
    });
  },
);

export const toggleCourseSearch: On<DegreePlannerState> = on(
  actions.toggleCourseSearch,
  (state, { termCode }) => {
    return produce(state, draft => {
      draft.search.visible = !draft.search.visible;

      if (termCode) {
        draft.search.selectedTerm = termCode;
      }
    });
  },
);

export const openCourseSearch: On<DegreePlannerState> = on(
  actions.openCourseSearch,
  (state, { termCode }) => {
    return produce(state, draft => {
      draft.search.visible = true;

      if (termCode) {
        draft.search.selectedTerm = termCode;
      }
    });
  },
);

export const closeCourseSearch: On<DegreePlannerState> = on(
  actions.closeCourseSearch,
  state => {
    return produce(state, draft => {
      draft.search.visible = false;
    });
  },
);

export const updateSearchTermCode: On<DegreePlannerState> = on(
  actions.updateSearchTermCode,
  (state, { termCode }) => {
    return produce(state, draft => {
      draft.search.selectedTerm = termCode;
    });
  },
);

export const openSidenav: On<DegreePlannerState> = on(
  actions.openSidenav,
  state => {
    return produce(state, draft => {
      draft.isSidenavOpen = true;
    });
  },
);

export const closeSidenav: On<DegreePlannerState> = on(
  actions.closeSidenav,
  state => {
    return produce(state, draft => {
      draft.isSidenavOpen = false;
    });
  },
);

export const dismissAlert: On<DegreePlannerState> = on(
  actions.dismissAlert,
  (state, { key }) => {
    return produce(state, draft => {
      const alertIndex = draft.alerts.findIndex(a => a.key === key);
      if (alertIndex !== -1) {
        draft.alerts.splice(alertIndex, 1);
      }
    });
  },
);
