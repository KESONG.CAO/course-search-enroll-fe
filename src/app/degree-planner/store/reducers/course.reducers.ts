import { Course } from '@app/core/models/course';
import { SavedForLaterCourse } from '@app/core/models/saved-for-later-course';
import { YearMapping } from '@app/core/models/year';
import { TermCode, YearCode } from '@app/types/terms';
import { On, on } from '@ngrx/store';
import produce from 'immer';
import * as actions from '../actions/course.actions';
import { DegreePlannerState } from '../state';

const findCourse = (
  years: YearMapping,
  termCode: TermCode,
  recordId: number,
) => {
  const year = years[YearCode.encode(termCode)];
  if (year) {
    const termName = TermCode.lowerTermName(termCode);
    const term = year[termName];
    return term.plannedCourses.find(course => course.id === recordId);
  }
};

const getPlannedCourses = (
  termCode: TermCode,
  years: YearMapping,
): Course[] => {
  const y = YearCode.encode(termCode);
  const t = TermCode.lowerTermName(termCode);
  return years[y][t].plannedCourses;
};

export const moveCourseInsideTerm: On<DegreePlannerState> = on(
  actions.moveCourseInsideTerm,
  (state, { termCode, recordId, newIndex }) => {
    return produce(state, draft => {
      const year = draft.visibleYears[YearCode.encode(termCode)];
      if (!year) {
        return;
      }

      const courses = year[TermCode.lowerTermName(termCode)].plannedCourses;
      const course = courses.find(c => c.id === recordId);
      if (!course) {
        return;
      }

      const oldIndex = courses.findIndex(c => c.id === recordId);

      // Remove course from old position
      courses.splice(oldIndex, 1);

      // Insert course in new position
      courses.splice(newIndex, 0, course);
    });
  },
);

export const moveCourseInsideSFL: On<DegreePlannerState> = on(
  actions.moveCourseInsideSFL,
  (state, { courseId, newIndex }) => {
    return produce(state, draft => {
      const courses = draft.savedForLaterCourses;
      const course = courses.find(c => c.courseId === courseId);
      if (!course) {
        return;
      }

      const oldIndex = courses.findIndex(c => c.courseId === courseId);

      // Remove course from old position
      courses.splice(oldIndex, 1);

      // Insert course in new position
      courses.splice(newIndex, 0, course);
    });
  },
);

export const moveCourseBetweenTerms: On<DegreePlannerState> = on(
  actions.moveCourseBetweenTerms,
  (state, { to: toTermCode, from: fromTermCode, id, newIndex }) => {
    return produce(state, draft => {
      const course = findCourse(draft.visibleYears, fromTermCode, id);
      if (!course || course.id === null) {
        return;
      }

      // Remove course from old term
      const oldCourses = getPlannedCourses(fromTermCode, draft.visibleYears);
      const oldIndex = oldCourses.findIndex(c => c.id === id);
      oldCourses.splice(oldIndex, 1);

      // Add course to the new term
      const newCourses = getPlannedCourses(toTermCode, draft.visibleYears);
      newCourses.splice(newIndex || newCourses.length, 0, {
        ...course,
        termCode: TermCode.encode(toTermCode),
        classNumber: null,
      });
    });
  },
);

export const moveCourseBetweenTermsSuccess: On<DegreePlannerState> = on(
  actions.moveCourseBetweenTermsSuccess,
  (state, { id, to, studentEnrollmentStatus }) => {
    return produce(state, draft => {
      const course = findCourse(draft.visibleYears, to, id);
      if (course) {
        course.studentEnrollmentStatus = studentEnrollmentStatus;
      }
    });
  },
);

export const addCourse: On<DegreePlannerState> = on(
  actions.addCourse,
  (
    state,
    { termCode, newIndex, courseId, subjectCode, title, catalogNumber },
  ) => {
    return produce(state, draft => {
      const courses = getPlannedCourses(termCode, draft.visibleYears);
      const course = {
        courseId,
        subjectCode,
        title,
        catalogNumber,
        termCode: TermCode.encode(termCode),
        id: null,
      } as Course;
      courses.splice(newIndex ?? courses.length, 0, course);
    });
  },
);

export const addCourseSuccess: On<DegreePlannerState> = on(
  actions.addCourseSuccess,
  (state, { termCode, course, newIndex }) => {
    return produce(state, draft => {
      const courses = getPlannedCourses(termCode, draft.visibleYears);
      courses[newIndex ?? courses.length - 1] = course;
    });
  },
);

export const removeCourse: On<DegreePlannerState> = on(
  actions.removeCourse,
  (state, { recordId, fromTermCode }) => {
    return produce(state, draft => {
      const courses = getPlannedCourses(fromTermCode, draft.visibleYears);
      const oldIndex = courses.findIndex(c => c.id === recordId);
      courses.splice(oldIndex, 1);
    });
  },
);

export const removeSaveForLater: On<DegreePlannerState> = on(
  actions.removeSaveForLater,
  (state, { courseId, subjectCode }) => {
    return produce(state, draft => {
      const courses = draft.savedForLaterCourses;
      const oldIndex = courses.findIndex(
        c => c.courseId === courseId && c.subjectCode === subjectCode,
      );
      courses.splice(oldIndex, 1);
    });
  },
);

export const addSaveForLater: On<DegreePlannerState> = on(
  actions.addSaveForLater,
  (state, { courseId, subjectCode, title, catalogNumber, newIndex }) => {
    return produce(state, draft => {
      const course: SavedForLaterCourse = {
        id: null,
        courseId,
        termCode: '0000',
        topicId: 0,
        subjectCode,
        title,
        catalogNumber,
        courseOrder: 0,
      };
      draft.savedForLaterCourses.splice(newIndex, 0, course);
    });
  },
);
