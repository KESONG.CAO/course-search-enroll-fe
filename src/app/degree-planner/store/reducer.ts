import { INITIAL_DEGREE_PLANNER_STATE } from '@app/degree-planner/store/state';
import { createReducer } from '@ngrx/store';

import * as courseReducers from './reducers/course.reducers';
import * as noteReducers from './reducers/note.reducers';
import * as planReducers from './reducers/plan.reducers';
import * as uiReducers from './reducers/ui.reducers';

export const degreePlannerReducer = createReducer(
  INITIAL_DEGREE_PLANNER_STATE,

  // Note reducer functions
  noteReducers.writeNote,
  noteReducers.writeNoteSuccess,
  noteReducers.deleteNote,

  // Plan reducer functions
  planReducers.planError,
  planReducers.createPlan,
  planReducers.initialLoadSuccess,
  planReducers.switchPlan,
  planReducers.switchPlanSuccess,
  planReducers.createPlanSuccess,
  planReducers.makePlanPrimarySuccess,
  planReducers.changePlanNameSuccess,
  planReducers.changePlanNameFailure,
  planReducers.deletePlanSuccess,

  // Course reducer functions
  courseReducers.moveCourseInsideTerm,
  courseReducers.moveCourseInsideSFL,
  courseReducers.moveCourseBetweenTerms,
  courseReducers.moveCourseBetweenTermsSuccess,
  courseReducers.addCourse,
  courseReducers.addCourseSuccess,
  courseReducers.removeCourse,
  courseReducers.removeSaveForLater,
  courseReducers.addSaveForLater,

  // UI reducer functions
  uiReducers.addAcademicYear,
  uiReducers.expandAcademicYear,
  uiReducers.collapseAcademicYear,
  uiReducers.toggleCourseSearch,
  uiReducers.openCourseSearch,
  uiReducers.closeCourseSearch,
  uiReducers.updateSearchTermCode,
  uiReducers.openSidenav,
  uiReducers.closeSidenav,
  uiReducers.dismissAlert,
);
