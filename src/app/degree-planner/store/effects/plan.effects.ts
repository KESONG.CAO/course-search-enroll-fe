import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { forkJoin, Observable, of } from 'rxjs';
import {
  tap,
  map,
  mergeMap,
  withLatestFrom,
  catchError,
  filter,
} from 'rxjs/operators';
import {
  State,
  INITIAL_DEGREE_PLANNER_STATE,
} from '@app/degree-planner/store/state';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DegreePlannerApiService } from '@app/degree-planner/services/api.service';
import * as selectors from '@app/degree-planner/store/selectors';
import * as planActions from '@app/degree-planner/store/actions/plan.actions';
import { DegreePlan } from '@app/core/models/degree-plan';
import { PlannedTerm, PlannedTermNote } from '@app/core/models/planned-term';
import { YearMapping, MutableYearMapping, Year } from '@app/core/models/year';
import { Note } from '@app/core/models/note';
import { CourseBase, Course } from '@app/core/models/course';
import { YearCode, TermCode, WithEra, Era } from '@app/types/terms';
import { Alert, DisclaimerAlert } from '@app/core/models/alert';
import { unique, forkJoinWithKeys } from '@app/core/utils';
import { prefs as prefActions } from '@app/actions';
import { prefs as prefSelectors } from '@app/selectors';
import { CourseDetails } from '@app/core/models/course-details';
import { SavedForLaterCourse } from '@app/core/models/saved-for-later-course';

const pickDegreePlanById = (
  roadmapId: number,
  plans: DegreePlan[],
): DegreePlan => {
  const plan = plans.find(p => p.roadmapId === roadmapId);
  return plan ? plan : plans[0];
};

const pickPrimaryDegreePlan = (plans: DegreePlan[]): DegreePlan => {
  const primary = plans.find(plan => plan.primary);
  return primary ? primary : plans[0];
};

const matchesTermCode = (termCode: TermCode) => (thing: {
  termCode: string;
}) => {
  return thing.termCode === TermCode.encode(termCode);
};

const buildTerm = (
  roadmapId: number,
  termCode: WithEra,
  notes: ReadonlyArray<Note>,
  courses: ReadonlyArray<{
    termCode: string;
    courses: ReadonlyArray<CourseBase>;
  }>,
): PlannedTerm => {
  const baseNote = notes.find(matchesTermCode(termCode));
  const note: PlannedTermNote | undefined = baseNote
    ? { isLoaded: true, text: baseNote.note, id: baseNote.id }
    : undefined;
  const group = courses.find(matchesTermCode(termCode));
  const formattedCourses = (group ? group.courses : []).map(course => {
    return { ...course, termCode: TermCode.encode(termCode) };
  });

  const plannedCourses: Course[] = [];
  const enrolledCourses: Course[] = [];
  const transferredCourses: Course[] = [];

  formattedCourses.forEach(course => {
    switch (course.studentEnrollmentStatus) {
      case 'Enrolled':
        enrolledCourses.push(course);
        break;
      case 'Transfer':
        transferredCourses.push(course);
        break;
      default:
        plannedCourses.push(course);
    }
  });

  return {
    roadmapId,
    termCode,
    note,
    plannedCourses,
    enrolledCourses,
    transferredCourses,
  };
};

const minYearCode = <T extends YearCode>(years: T[]): T => {
  let min = years[0];
  for (const year of years) {
    if (YearCode.comesBefore(year, min)) {
      min = year;
    }
  }
  return min;
};

const maxYearCode = <T extends YearCode>(years: T[]): T => {
  let max = years[0];
  for (const year of years) {
    if (YearCode.comesAfter(year, max)) {
      max = year;
    }
  }
  return max;
};

const yearCodeRange = (from: YearCode, to: YearCode): YearCode[] => {
  const years: YearCode[] = [];
  let year = from;
  while (YearCode.comesAfter(to, year) || YearCode.equals(to, year)) {
    years.push(year);
    year = YearCode.next(year);
  }

  return years;
};

const isEmptyPastTerm = (term: PlannedTerm): boolean => {
  const isPast = term.termCode.era === Era.Past;
  const isEmpty =
    !term.note &&
    term.plannedCourses.length === 0 &&
    term.enrolledCourses.length === 0;
  return isPast && isEmpty;
};

const isEmptyPastYear = (year: Year): boolean => {
  return (
    isEmptyPastTerm(year.fall) &&
    isEmptyPastTerm(year.spring) &&
    isEmptyPastTerm(year.summer)
  );
};

const loadPlanYears = (
  api: DegreePlannerApiService,
  roadmapId: number,
  active: TermCode[],
): Observable<YearMapping> => {
  const notesAndCourses$ = forkJoinWithKeys({
    notes: api.getAllNotes(roadmapId),
    courses: api.getAllTermCourses(roadmapId),
  });

  const allYearCodes$ = notesAndCourses$.pipe(
    map(({ notes, courses }) => {
      const noteTermCodes = notes.map(note => note.termCode);
      const courseTermCodes = courses.map(course => course.termCode);
      const allTermCodes = [
        ...noteTermCodes.map(TermCode.decodeOrThrow),
        ...courseTermCodes.map(TermCode.decodeOrThrow),
        ...active,
      ].map(WithEra.fromTermCodeCurry(active));
      const uniqueYearCodes = unique(allTermCodes.map(YearCode.encode)).map(
        YearCode.decodeOrThrow,
      );

      const first = minYearCode(uniqueYearCodes);
      const last = maxYearCode(uniqueYearCodes);
      const allYearCodes = yearCodeRange(first, last);

      return {
        allYearCodes,
        notes,
        courses,
      };
    }),
  );

  const visibleYears$ = allYearCodes$.pipe(
    map(({ allYearCodes, notes, courses }) => {
      const mapping: MutableYearMapping = {};
      allYearCodes
        .map(yearCode => {
          const { fall, spring, summer } = WithEra.fromYearCode(
            active,
            yearCode,
          );
          const isExpanded = !(
            fall.era === Era.Past &&
            spring.era === Era.Past &&
            summer.era === Era.Past
          );
          return {
            yearCode,
            isExpanded,
            fall: buildTerm(roadmapId, fall, notes, courses),
            spring: buildTerm(roadmapId, spring, notes, courses),
            summer: buildTerm(roadmapId, summer, notes, courses),
          } as Year;
        })
        .filter(year => !isEmptyPastYear(year))
        .forEach(year => (mapping[YearCode.encode(year.yearCode)] = year));

      return mapping as YearMapping;
    }),
  );

  return visibleYears$;
};

@Injectable()
export class DegreePlanEffects {
  constructor(
    private actions$: Actions,
    private api: DegreePlannerApiService,
    private store$: Store<State>,
    private snackBar: MatSnackBar,
  ) {}

  @Effect()
  init$ = this.actions$.pipe(
    ofType(planActions.initialLoadRequest),
    // Load the list of degree plans and data used by all degree plans.
    mergeMap(() => {
      return forkJoinWithKeys({
        allDegreePlans: this.api.getAllDegreePlans(),
        activeTermCodes: this.api.getActiveTermCodes(),
      });
    }),

    withLatestFrom(
      this.store$.select(prefSelectors.getKey, 'degreePlannerSelectedPlan'),
      this.store$.select(
        prefSelectors.getKey,
        'degreePlannerHasDismissedDisclaimer',
      ),
    ),
    mergeMap(
      ([
        { allDegreePlans, activeTermCodes },
        degreePlannerSelectedPlan,
        degreePlannerHasDismissedDisclaimer,
      ]) => {
        const savedForLaterCourses = this.api.getSavedForLaterCourses().pipe(
          mergeMap(courses => {
            const addShortCatalog = (saved: SavedForLaterCourse) => {
              return (details: CourseDetails): SavedForLaterCourse => {
                const shortDesc = details.subject.shortDescription;
                const shortCatalog = `${shortDesc} ${saved.catalogNumber}`;
                return { ...saved, shortCatalog };
              };
            };

            if (courses.length === 0) {
              return of([]);
            }

            return forkJoin(
              courses.map(course => {
                return this.api
                  .getCourseDetails(course.subjectCode, course.courseId)
                  .pipe(map(addShortCatalog(course)));
              }),
            );
          }),
        );

        const visibleDegreePlan =
          typeof degreePlannerSelectedPlan === 'number'
            ? pickDegreePlanById(degreePlannerSelectedPlan, allDegreePlans)
            : pickPrimaryDegreePlan(allDegreePlans);
        const visibleYears = loadPlanYears(
          this.api,
          visibleDegreePlan.roadmapId,
          activeTermCodes,
        );

        const alerts: Alert[] = [];

        if (degreePlannerHasDismissedDisclaimer !== true) {
          alerts.push(
            new DisclaimerAlert(() => {
              this.store$.dispatch(
                prefActions.setKey({
                  key: 'degreePlannerHasDismissedDisclaimer',
                  value: true,
                }),
              );
            }),
          );
        }

        return forkJoinWithKeys({
          visibleDegreePlan: of(visibleDegreePlan),
          visibleYears,
          savedForLaterCourses,
          allDegreePlans: of(allDegreePlans),
          alerts: of(alerts),
        }).pipe(
          map(payload => {
            return planActions.initialLoadSuccess({
              payload: {
                ...INITIAL_DEGREE_PLANNER_STATE,
                ...payload,
                activeTermCodes,
                isLoadingPlan: false,
              },
            });
          }),
        );
      },
    ),
    catchError(error => {
      console.error(error);
      return of(
        planActions.planError({
          message: 'Error loading data. Please reload to try again.',
          duration: 2000,
          error,
        }),
      );
    }),
  );

  @Effect()
  switch$ = this.actions$.pipe(
    ofType(planActions.switchPlan),
    withLatestFrom(
      this.store$.select(selectors.selectAllDegreePlans),
      this.store$.select(selectors.getActiveTermCodes),
    ),
    mergeMap(([action, allDegreePlans, activeTermCodes]) => {
      const visibleDegreePlan = allDegreePlans.find(plan => {
        return plan.roadmapId === action.newVisibleRoadmapId;
      }) as DegreePlan;

      const visibleYears = loadPlanYears(
        this.api,
        visibleDegreePlan.roadmapId,
        activeTermCodes,
      );

      return forkJoinWithKeys({
        visibleDegreePlan: of(visibleDegreePlan),
        visibleYears,
      });
    }),
    mergeMap(payload => [
      planActions.switchPlanSuccess(payload),
      prefActions.setKey({
        key: 'degreePlannerSelectedPlan',
        value: payload.visibleDegreePlan.roadmapId,
      }),
    ]),
    tap(state => {
      if (state.type === 'plan/switchPlanSuccess') {
        const touchedPlan = state.visibleDegreePlan.name;
        const message = `Switched to ${touchedPlan}`;
        this.snackBar.open(message, undefined, {});
      }
    }),
    catchError(error => {
      return of(
        planActions.planError({
          message: 'Unable to switch plan',
          duration: 2000,
          error,
        }),
      );
    }),
  );

  @Effect()
  MakePlanPrimary$ = this.actions$.pipe(
    ofType(planActions.makePlanPrimary),
    withLatestFrom(this.store$.select(selectors.selectVisibleDegreePlan)),
    filter(([_, visibleDegreePlan]) => visibleDegreePlan !== undefined),
    // Get term data for the degree plan specified by the roadmap ID.
    mergeMap(([_action, visibleDegreePlan]) => {
      const { roadmapId, name } = visibleDegreePlan as DegreePlan;
      return this.api.updatePlan(roadmapId, name, true);
    }),
    // // Wrap data in an Action for dispatch
    map(response => {
      if (response === 1) {
        return planActions.makePlanPrimarySuccess();
      } else {
        return planActions.makePlanPrimaryFailure();
      }
    }),
    tap(() => {
      const message = 'This plan has been set as the primary plan';
      this.snackBar.open(message, undefined, {});
    }),
    catchError(error => {
      return of(
        planActions.planError({
          message: 'Unable to make this plan primary',
          duration: 2000,
          error,
        }),
      );
    }),
  );

  @Effect()
  ChangePlanName$ = this.actions$.pipe(
    ofType(planActions.changePlanName),
    withLatestFrom(this.store$.select(selectors.selectAllDegreePlans)),
    mergeMap(([{ roadmapId, newName }, allDegreePlans]) => {
      const oldDegreePlan = allDegreePlans.find(plan => {
        return plan.roadmapId === roadmapId;
      }) as DegreePlan;
      const oldName = oldDegreePlan.name;

      return this.api
        .updatePlan(roadmapId, newName, oldDegreePlan.primary)
        .pipe(
          map(() => {
            return planActions.changePlanNameSuccess({ roadmapId, newName });
          }),
          tap(() => {
            const message = `Plan has been renamed to ${newName}`;
            this.snackBar.open(message, undefined, {});
          }),
          catchError(() => {
            return of(
              planActions.changePlanNameFailure({ roadmapId, oldName }),
            );
          }),
        );
    }),
  );

  @Effect()
  createPlan$ = this.actions$.pipe(
    ofType(planActions.createPlan),
    withLatestFrom(this.store$.select(selectors.getActiveTermCodes)),
    mergeMap(([{ name, primary }, activeTermCodes]) => {
      return this.api.createDegreePlan(name, primary).pipe(
        mergeMap(newPlan => {
          const newYears = loadPlanYears(
            this.api,
            newPlan.roadmapId,
            activeTermCodes,
          );

          return forkJoinWithKeys({
            newPlan: of(newPlan),
            newYears,
          });
        }),
        mergeMap(({ newPlan, newYears }) => [
          planActions.createPlanSuccess({ newPlan, newYears }),
          prefActions.setKey({
            key: 'degreePlannerSelectedPlan',
            value: newPlan.roadmapId,
          }),
        ]),
        tap(() => {
          const message = `New plan has been created`;
          this.snackBar.open(message, undefined, {});
        }),
        catchError(error => {
          return of(
            planActions.planError({
              message: 'Unable to create new plan',
              duration: 2000,
              error,
            }),
          );
        }),
      );
    }),
  );

  @Effect()
  deletePlan$ = this.actions$.pipe(
    ofType(planActions.deletePlan),
    mergeMap(({ roadmapId }) => {
      return this.api.deleteDegreePlan(roadmapId).pipe(
        map(() => planActions.deletePlanSuccess({ roadmapId })),
        tap(() => {
          const message = `Deleting selected plan`;
          this.snackBar.open(message, undefined, { duration: 10000 });
        }),
        catchError(error => {
          return of(
            planActions.planError({
              message: 'Unable to delete plan',
              duration: 2000,
              error,
            }),
          );
        }),
      );
    }),
  );
}
