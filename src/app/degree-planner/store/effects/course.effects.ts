import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of, forkJoin } from 'rxjs';
import {
  tap,
  map,
  mergeMap,
  withLatestFrom,
  filter,
  catchError,
  take,
} from 'rxjs/operators';
import { State } from '@app/degree-planner/store/state';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DegreePlannerApiService } from '@app/degree-planner/services/api.service';
import * as selectors from '@app/degree-planner/store/selectors';
import * as courseActions from '@app/degree-planner/store/actions/course.actions';
import { DegreePlan } from '@app/core/models/degree-plan';
import { Course, CourseBase } from '@app/core/models/course';
import { TermCode, Era, ZERO } from '@app/types/terms';
import * as globalSelectors from '@app/selectors';
import { ifSuccessThenUnwrap } from '@app/core/utils';

@Injectable()
export class CourseEffects {
  constructor(
    private actions$: Actions,
    private api: DegreePlannerApiService,
    private store$: Store<State>,
    private snackBar: MatSnackBar,
  ) {}

  @Effect()
  MoveCourseBetweenTerms$ = this.actions$.pipe(
    ofType(courseActions.moveCourseBetweenTerms),

    withLatestFrom(this.store$.select(selectors.selectVisibleDegreePlan)),
    filter(([_, degreePlan]) => {
      return degreePlan !== undefined;
    }),

    // Get term data for the degree plan specified by the roadmap ID.
    mergeMap(([action, degreePlan]) => {
      const roadmapId = degreePlan!.roadmapId;
      const {
        id: recordId,
        to: toTermCode,
        subjectCode,
        courseId,
        credits,
      } = action;

      const moveCourse = this.api.updateCourseTerm(
        roadmapId,
        recordId,
        toTermCode,
        credits,
      );

      if (toTermCode.era === Era.Active && degreePlan!.primary) {
        /**
         * The `updateCourseTerm` API won't force cart validation which we want
         * if we're adding a course to the cart. Calling the `addCourseToCart`
         * API when moving a cart to an active term will trigger the cart
         * validation.
         */
        const validateCart = this.api.addCourseToCart(
          subjectCode,
          courseId,
          toTermCode,
        );
        return forkJoin([moveCourse, validateCart]).pipe(
          map(([_, course]) => {
            return [course, action] as const;
          }),
        );
      } else {
        return moveCourse.pipe(
          map(course => {
            return [course, action] as const;
          }),
        );
      }
    }),

    map(([course, action]) => {
      return courseActions.moveCourseBetweenTermsSuccess({
        ...action,
        studentEnrollmentStatus: course.studentEnrollmentStatus,
      });
    }),

    tap(action => {
      const touchedTerm = TermCode.describe(action.to);
      const message = `Course has been moved to ${touchedTerm}`;
      this.snackBar.open(message, undefined, {});
    }),

    catchError(error => {
      return of(
        courseActions.courseError({
          message: 'Unable to move course',
          error,
        }),
      );
    }),
  );

  @Effect()
  AddCourse$ = this.actions$.pipe(
    ofType(courseActions.addCourse),

    withLatestFrom(this.store$.select(selectors.selectVisibleDegreePlan)),

    filter(([_, visibleDegreePlan]) => visibleDegreePlan !== undefined),

    mergeMap(([action, visibleDegreePlan]) => {
      // TODO error handle the API calls
      const roadmapId = (visibleDegreePlan as DegreePlan).roadmapId;
      const { subjectCode, termCode, courseId, newIndex } = action;

      const isPrimaryPlan = (visibleDegreePlan as DegreePlan).primary;
      const addCourse$ =
        termCode.era === Era.Active && isPrimaryPlan
          ? this.api.addCourseToCart(subjectCode, courseId, termCode)
          : this.api.addCourse(roadmapId, subjectCode, courseId, termCode);

      const courseBaseToCourse$ = addCourse$.pipe(
        map<CourseBase, Course>(courseBase => ({
          ...courseBase,
          termCode: TermCode.encode(termCode),
        })),
      );

      const toSuccessAction$ = courseBaseToCourse$.pipe(
        map(course =>
          courseActions.addCourseSuccess({ termCode, course, newIndex }),
        ),
      );

      return toSuccessAction$;
    }),

    tap(state => {
      this.store$
        .select(globalSelectors.terms.getSubjectsForTerm, ZERO)
        .pipe(ifSuccessThenUnwrap(), take(1))
        .toPromise()
        .then(subjects => {
          const touchedCourse = state.course;
          const touchedTerm = TermCode.describe(
            TermCode.decodeOrThrow(touchedCourse.termCode),
          );
          const subject = subjects.find(
            ({ code }) => code === touchedCourse.subjectCode,
          )!.description;
          const message = `${subject} ${touchedCourse.catalogNumber} has been added to ${touchedTerm}`;
          this.snackBar.open(message, undefined, {});
        });
    }),

    catchError(error => {
      return of(
        courseActions.courseError({
          message: 'Unable to add course',
          error,
        }),
      );
    }),
  );

  @Effect()
  RemoveCourse$ = this.actions$.pipe(
    ofType(courseActions.removeCourse),

    withLatestFrom(this.store$.select(selectors.selectVisibleDegreePlan)),

    filter(([_, visibleDegreePlan]) => visibleDegreePlan !== undefined),

    mergeMap(([action, visibleDegreePlan]) => {
      const roadmapId = (visibleDegreePlan as DegreePlan).roadmapId;
      const recordId = action.recordId;
      const fromTermCode = action.fromTermCode;

      const removeCourse$ = this.api.removeCourse(roadmapId, recordId);
      const toSuccessAction$ = removeCourse$.pipe(
        map(() =>
          courseActions.removeCourseSuccess({ fromTermCode, recordId }),
        ),
      );

      return toSuccessAction$;
    }),

    catchError(error => {
      return of(
        courseActions.courseError({
          message: 'Unable to remove course',
          error,
        }),
      );
    }),
  );

  @Effect()
  RemoveSavedForLater$ = this.actions$.pipe(
    ofType(courseActions.removeSaveForLater),

    mergeMap(action => {
      const { subjectCode, courseId } = action;
      return this.api
        .removeSavedForLater(subjectCode, courseId)
        .pipe(map(() => action));
    }),

    map(action => courseActions.removeSaveForLaterSuccess(action)),

    catchError(error => {
      return of(
        courseActions.courseError({
          message: 'Unable to remove saved course',
          error,
        }),
      );
    }),
  );

  @Effect()
  SaveForLater$ = this.actions$.pipe(
    ofType(courseActions.addSaveForLater),

    mergeMap(action => {
      const { subjectCode, courseId } = action;
      return this.api
        .saveForLater(subjectCode, courseId)
        .pipe(map(() => action));
    }),

    map(action => courseActions.addSaveForLaterSuccess(action)),

    tap(() => {
      const message = 'Course has been saved for later';
      this.snackBar.open(message, undefined, {});
    }),

    catchError(error => {
      return of(
        courseActions.courseError({
          message: 'Unable to save course for later',
          error,
        }),
      );
    }),
  );
}
