import { NgModule } from '@angular/core';
import { MatListModule } from '@angular/material/list';
import { DegreePlannerViewComponent } from './degree-planner-view/degree-planner-view.component';
import { SharedModule } from '@app/shared/shared.module';
import { TermContainerComponent } from './term-container/term-container.component';
import { SidenavMenuItemComponent } from './sidenav-menu-item/sidenav-menu-item.component';
import { SavedForLaterContainerComponent } from './saved-for-later-container/saved-for-later-container.component';
import { CourseItemComponent } from './shared/course-item/course-item.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { YearContainerComponent } from '@app/degree-planner/year-container/year-container.component';
import { CourseSearchComponent } from '@app/degree-planner/course-search/course-search.component';
import { EffectsModule } from '@ngrx/effects';
import { DegreePlanEffects } from './store/effects/plan.effects';
import { NoteEffects } from './store/effects/note.effects';
import { CourseEffects } from './store/effects/course.effects';
import { ErrorEffects } from './store/effects/error.effects';
import { StoreModule } from '@ngrx/store';
import { degreePlannerReducer } from './store/reducer';
import { DegreePlannerRoutingModule } from './degree-planner-routing.module';
import { CourseDetailsDialogComponent } from './dialogs/course-details-dialog/course-details-dialog.component';
import { IE11WarningDialogComponent } from './dialogs/ie11-warning-dialog/ie11-warning-dialog.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { AddPlanDialogComponent } from './dialogs/add-plan-dialog.component';
import { RenamePlanDialogComponent } from './dialogs/rename-plan-dialog.component';
import { AddNoteDialogComponent } from './dialogs/add-note-dialog.component';
import { EditNoteDialogComponent } from './dialogs/edit-note-dialog.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { LoadableDirectivesModule } from 'loadable.ts';

@NgModule({
  imports: [
    SharedModule,
    MatListModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    FlexLayoutModule,
    MatCardModule,
    MatTabsModule,
    DragDropModule,
    StoreModule.forFeature('degreePlanner', degreePlannerReducer),
    EffectsModule.forFeature([
      DegreePlanEffects,
      NoteEffects,
      CourseEffects,
      ErrorEffects,
    ]),
    DegreePlannerRoutingModule,
    NgxMatSelectSearchModule,
    LoadableDirectivesModule,
  ],
  exports: [DragDropModule],
  declarations: [
    DegreePlannerViewComponent,
    TermContainerComponent,
    CourseItemComponent,
    SidenavMenuItemComponent,
    SavedForLaterContainerComponent,
    YearContainerComponent,
    CourseSearchComponent,
    CourseDetailsDialogComponent,
    IE11WarningDialogComponent,
    CourseDetailsComponent,
    AddPlanDialogComponent,
    RenamePlanDialogComponent,
    AddNoteDialogComponent,
    EditNoteDialogComponent,
  ],
})
export class DegreePlannerModule {}
