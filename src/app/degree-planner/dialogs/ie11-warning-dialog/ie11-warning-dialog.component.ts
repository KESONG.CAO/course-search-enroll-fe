import { Component } from '@angular/core';

@Component({
  selector: 'cse-ie11-warning-dialog',
  template: `
    <cse-dialog headline="Internet Explorer is not fully supported">
      <cse-dialog-body class="cse-typeset">
        <p>
          The Course Search &amp; Enroll app works best with Edge, Chrome, or
          Firefox.
        </p>
      </cse-dialog-body>
      <cse-dialog-actions>
        <a
          mat-button
          target="_blank"
          href="https://kb.wisc.edu/page.php?id=44398"
        >
          Get help
        </a>
        <div class="spacer"></div>
        <button mat-raised-button mat-dialog-close color="primary">Okay</button>
      </cse-dialog-actions>
    </cse-dialog>
  `,
})
export class IE11WarningDialogComponent {}
