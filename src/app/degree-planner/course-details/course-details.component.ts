import { MediaMatcher } from '@angular/cdk/layout';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  take,
  withLatestFrom,
  map,
} from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { CourseDetails } from '@app/core/models/course-details';
import * as selectors from '@app/degree-planner/store/selectors';
import * as utils from '@app/degree-planner/shared/utils';
import { TermCode, WithEra } from '@app/types/terms';
import { State } from '@app/degree-planner/store/state';
import { Course } from '@app/core/models/course';
import { PlannedTerm } from '@app/core/models/planned-term';
import { isntUndefined } from '@app/core/utils';
import * as courseActions from '@app/degree-planner/store/actions/course.actions';
import { DialogService } from '@app/shared/services/dialog.service';

@Component({
  selector: 'cse-old-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.scss'],
})
export class CourseDetailsComponent implements OnInit, OnDestroy {
  @Input() public courseDetails: CourseDetails;
  @Input() public type: 'course' | 'search' | 'saved';
  public selectedSearchTerm: TermCode | undefined;
  public term$: Observable<PlannedTerm>;
  public termSelector: FormGroup;
  public mobileView: MediaQueryList;

  public selectedSearchTerm$: Observable<TermCode | undefined>;
  public droppableTermCodes$: Observable<WithEra[]>;
  public searchTermSubscription: Subscription;
  public termSubscription: Subscription;
  public plannedCourses: ReadonlyArray<Course>;

  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private dialog: DialogService,
    mediaMatcher: MediaMatcher,
  ) {
    this.mobileView = mediaMatcher.matchMedia('(max-width: 959px)');
  }

  ngOnInit() {
    this.searchTermSubscription = this.store
      .select(selectors.getSelectedSearchTerm)
      .pipe(take(1))
      .subscribe(term => (this.termSelector = this.fb.group({ term })));

    this.droppableTermCodes$ = this.store.pipe(
      select(selectors.selectAllVisibleYears),
      utils.yearsToDroppableTermCodes(),
      distinctUntilChanged(utils.compareArrays(TermCode.equals)),
      withLatestFrom(this.store.select(selectors.getActiveTermCodes)),
      map(([termCodes, active]) =>
        termCodes.map(WithEra.fromTermCodeCurry(active)),
      ),
    );
  }

  ngOnDestroy() {
    this.searchTermSubscription.unsubscribe();
  }

  addCourseToPlan($event) {
    $event.preventDefault();

    const termCode: WithEra | undefined = this.termSelector.value.term;
    const subjectCode = this.courseDetails.subject.subjectCode;
    const courseId = this.courseDetails.courseId;

    if (termCode === undefined) {
      return;
    }

    const payload = {
      courseId,
      termCode,
      subjectCode,
      title: this.courseDetails.title,
      catalogNumber: this.courseDetails.catalogNumber,
    };

    this.term$ = this.store.pipe(
      select(selectors.selectVisibleTerm, {
        termCode,
      }),
      filter(isntUndefined),
      distinctUntilChanged(),
    );
    this.termSubscription = this.term$.subscribe(term => {
      this.plannedCourses = term.plannedCourses;
    });

    const isCourseInPlannedCourses = this.plannedCourses.some(
      course => course.courseId === this.courseDetails.courseId,
    );

    if (isCourseInPlannedCourses) {
      this.dialog.acknowledge({
        headline: `Can't add course to term`,
        reasoning: ['This course already exists in the selected term'],
      });
    } else {
      switch (this.type) {
        case 'search':
          this.store.dispatch(courseActions.addCourse(payload));
          break;

        case 'saved':
          this.store.dispatch(courseActions.addCourse(payload));
          this.store.dispatch(
            courseActions.removeSaveForLater({ subjectCode, courseId }),
          );
          break;
      }
    }
  }

  public sameTermCodes(a: TermCode | undefined, b: TermCode | undefined) {
    if (a === undefined && b === undefined) {
      return true;
    } else if (!a || !b) {
      return false;
    } else {
      return TermCode.equals(a, b);
    }
  }
}
