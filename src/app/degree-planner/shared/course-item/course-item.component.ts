import { MediaMatcher } from '@angular/cdk/layout';
import { Component, Input, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as utils from '@app/degree-planner/shared/utils';
import * as courseActions from '@app/degree-planner/store/actions/course.actions';
import * as uiActions from '@app/degree-planner/store/actions/ui.actions';
import { State } from '@app/degree-planner/store/state';
import { Course, pickCreditAmountFromCourse } from '@app/core/models/course';
import * as selectors from '@app/degree-planner/store/selectors';
import { CourseDetailsDialogComponent } from '@app/degree-planner/dialogs/course-details-dialog/course-details-dialog.component';
import {
  distinctUntilChanged,
  filter,
  map,
  withLatestFrom,
} from 'rxjs/operators';
import { TermCode, Era, WithEra } from '@app/types/terms';
import { PlannedTerm } from '@app/core/models/planned-term';
import { isntUndefined } from '@app/core/utils';
import { prefs as prefSelectors } from '@app/selectors';
import { DialogService } from '@app/shared/services/dialog.service';

@Component({
  selector: 'cse-course-item',
  templateUrl: './course-item.component.html',
  styleUrls: ['./course-item.component.scss'],
})
export class CourseItemComponent implements OnInit {
  @Input() course: Course;
  @Input() isCurrentTerm: boolean;
  @Input() isPastTerm: boolean;
  @Input() disabled: boolean;
  @Input() type: 'saved' | 'course' | 'search';
  @Input() era?: Era;
  visibleTerms: any;
  activeTerm: any;
  public status:
    | 'AdvancedStanding'
    | 'InProgress'
    | 'Waitlisted'
    | 'NotOfferedInTerm'
    | 'DoesNotExist'
    | 'Normal';
  public droppableTermCodes$: Observable<WithEra[]>;
  public term$: Observable<PlannedTerm>;
  public plannedCourses: ReadonlyArray<Course>;
  public toActiveTerm: boolean;
  public mobileView: MediaQueryList;
  public showGrades$: Observable<boolean>;
  public normalizedGrade: string;

  constructor(
    private dialog: DialogService,
    private store: Store<State>,
    private snackBar: MatSnackBar,
    public mediaMatcher: MediaMatcher,
  ) {
    this.mobileView = mediaMatcher.matchMedia('(max-width: 959px)');
  }

  ngOnInit() {
    const isActive = this.era === Era.Active;
    const isNotOffered = this.course.studentEnrollmentStatus === 'NOTOFFERED';
    const doesNotExist = this.course.studentEnrollmentStatus === 'DOESNOTEXIST';
    const isAdvancedStanding =
      this.course.studentEnrollmentStatus === 'Transfer';

    const isWaitlisted = this.course.studentEnrollmentStatus === 'Waitlisted';
    const isInProgress =
      isActive && this.course.studentEnrollmentStatus === 'Enrolled';

    if (isWaitlisted) {
      this.status = 'Waitlisted';
    } else if (isAdvancedStanding) {
      this.status = 'AdvancedStanding';
    } else if (isInProgress) {
      this.status = 'InProgress';
    } else if (isNotOffered) {
      this.status = 'NotOfferedInTerm';
    } else if (doesNotExist) {
      this.status = 'DoesNotExist';
    } else {
      this.status = 'Normal';
    }

    this.showGrades$ = this.store
      .select(prefSelectors.getKey, 'degreePlannerGradesVisibility')
      .pipe(map(val => val === true));

    if (this.course.studentEnrollmentStatus === 'Transfer') {
      this.normalizedGrade = 'T';
    } else if (typeof this.course.grade === 'string') {
      this.normalizedGrade = this.course.grade;
    } else {
      this.normalizedGrade = '\xa0'; // non-breaking space (equivalent to HTML &nbsp;)
    }
  }

  onMenuOpen() {
    this.droppableTermCodes$ = this.store.pipe(
      select(selectors.selectAllVisibleYears),
      utils.yearsToDroppableTermCodes(),
      distinctUntilChanged(utils.compareArrays(TermCode.equals)),
      withLatestFrom(this.store.select(selectors.getActiveTermCodes)),
      map(([termCodes, active]) =>
        termCodes.map(WithEra.fromTermCodeCurry(active)),
      ),
    );
  }

  moveToSavedForLater(course) {
    this.store.dispatch(
      courseActions.addSaveForLater({
        courseId: course.courseId,
        subjectCode: course.subjectCode,
        title: course.title,
        catalogNumber: course.catalogNumber,
        newIndex: 0,
      }),
    );
  }

  /**
   *
   *  Handle moving a course to different terms based on course type
   *
   */
  onMove(toTermCode: WithEra) {
    this.term$ = this.store.pipe(
      select(selectors.selectVisibleTerm, { termCode: toTermCode }),
      filter(isntUndefined),
      distinctUntilChanged(),
    );

    this.term$.subscribe(term => {
      this.plannedCourses = term.plannedCourses;
      this.toActiveTerm = term.termCode.era === Era.Active;
    });

    const isCourseInPlannedCourses = this.plannedCourses.some(
      course => course.courseId === this.course.courseId,
    );

    if (isCourseInPlannedCourses) {
      this.dialog.acknowledge({
        headline: `Can't add course to term`,
        reasoning: ['This course already exists in the selected term'],
      });
      return;
    }

    switch (this.type) {
      case 'course': {
        const id = this.course.id as number;
        const { subjectCode, courseId } = this.course;
        const from = TermCode.decodeOrThrow(this.course.termCode);
        this.store.dispatch(
          courseActions.moveCourseBetweenTerms({
            to: toTermCode,
            from,
            id,
            newIndex: 0,
            courseId,
            subjectCode,
            credits: pickCreditAmountFromCourse(this.course),
          }),
        );
        break;
      }

      case 'saved': {
        const { subjectCode, courseId } = this.course;
        this.addToTerm(toTermCode);
        this.store.dispatch(
          courseActions.removeSaveForLater({ subjectCode, courseId }),
        );
        break;
      }

      case 'search': {
        this.addToTerm(toTermCode);
        if (this.mobileView.matches) {
          this.store.dispatch(uiActions.closeCourseSearch());
        }
        break;
      }
    }
  }

  /**
   *
   *  Handle saving a course for later (This is not possible if a course is already saved)
   *
   */
  onSaveForLater() {
    const {
      courseId,
      subjectCode,
      title,
      catalogNumber,
      termCode,
    } = this.course;

    // Dispatch a save for later event
    this.store.dispatch(
      courseActions.addSaveForLater({
        courseId: courseId,
        subjectCode: subjectCode,
        title: title,
        catalogNumber: catalogNumber,
        newIndex: 0,
      }),
    );

    // If course is in a term, we need to remove it
    // If this course came from search and is mobile, close the search
    switch (this.type) {
      case 'course':
        this.store.dispatch(
          courseActions.removeCourse({
            fromTermCode: TermCode.decodeOrThrow(termCode),
            recordId: this.course.id as number,
          }),
        );
        break;

      case 'search':
        if (this.mobileView.matches) {
          this.store.dispatch(uiActions.closeCourseSearch());
        }
        break;
    }
  }

  /**
   *
   *  Handle removing a course (This is not possible for type 'search')
   *
   */
  async onRemove() {
    const container =
      this.type === 'saved'
        ? 'saved courses'
        : this.era === Era.Future
        ? 'degree plan'
        : 'degree plan and your cart';

    const shouldRemove = await this.dialog.confirm({
      headline: 'Remove course?',
      reasoning: [
        `This will remove "${this.course.title}" from your ${container}.`,
      ],
      affirmative: 'Remove',
    });

    if (shouldRemove) {
      switch (this.type) {
        case 'course':
          this.store.dispatch(
            courseActions.removeCourse({
              fromTermCode: TermCode.decodeOrThrow(this.course.termCode),
              recordId: this.course.id as number,
            }),
          );
          break;

        case 'saved':
          const { subjectCode, courseId } = this.course;
          this.store.dispatch(
            courseActions.removeSaveForLater({ subjectCode, courseId }),
          );
          break;
      }
    }
  }

  addToTerm(toTermCode: WithEra) {
    this.store.dispatch(
      courseActions.addCourse({
        courseId: this.course.courseId,
        termCode: toTermCode,
        subjectCode: this.course.subjectCode,
        title: this.course.title,
        catalogNumber: this.course.catalogNumber,
      }),
    );
  }

  openCourseDetailsDialog() {
    const { subjectCode, courseId } = this.course;
    if (this.course.studentEnrollmentStatus === 'DOESNOTEXIST') {
      this.snackBar.open(`This course is no longer offered`);
      return;
    } else if (this.course.studentEnrollmentStatus === 'Transfer') {
      this.snackBar.open(`No details are kept for transferred courses`);
      return;
    }

    this.dialog.large(CourseDetailsDialogComponent, {
      subjectCode,
      courseId,
      type: this.type,
    });
  }

  // Check for enter key presses
  detectEnter($event: any, root: any) {
    if ($event.target === root && $event.keyCode === 13) {
      this.openCourseDetailsDialog();
    }
  }
}
