import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { Component, Input, OnInit } from '@angular/core';
import { State } from '@app/degree-planner/store/state';
import * as selectors from '@app/degree-planner/store/selectors';
import * as uiActions from '@app/degree-planner/store/actions/ui.actions';
import { YearCode, WithEra, Era } from '@app/types/terms';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'cse-year-container',
  templateUrl: './year-container.component.html',
  styleUrls: ['./year-container.component.scss'],
})
export class YearContainerComponent implements OnInit {
  @Input() yearCode: YearCode;

  public title$: Observable<string>;
  public fallTermCode: Observable<WithEra>;
  public springTermCode: Observable<WithEra>;
  public summerTermCode: Observable<WithEra>;
  public isExpanded$: Observable<boolean>;

  constructor(private store: Store<State>) {}

  public ngOnInit(): void {
    const activeTermCodes = this.store
      .select(selectors.getActiveTermCodes)
      .pipe(filter(active => active.length > 0));

    this.title$ = activeTermCodes.pipe(
      map(active => {
        const { fall, summer } = WithEra.fromYearCode(active, this.yearCode);
        const fromYear = YearCode.beginningYear(this.yearCode);
        const toYear = YearCode.endingYear(this.yearCode);

        if (summer.era === Era.Past) {
          return `Past year: ${fromYear}-${toYear}`;
        }

        if (fall.era === Era.Future) {
          return `Future year: ${fromYear}-${toYear}`;
        }

        return `Active year: ${fromYear}-${toYear}`;
      }),
    );

    this.fallTermCode = activeTermCodes.pipe(
      map(active => WithEra.fromYearCode(active, this.yearCode).fall),
    );

    this.springTermCode = activeTermCodes.pipe(
      map(active => WithEra.fromYearCode(active, this.yearCode).spring),
    );

    this.summerTermCode = activeTermCodes.pipe(
      map(active => WithEra.fromYearCode(active, this.yearCode).summer),
    );

    this.isExpanded$ = this.store.pipe(
      select(selectors.selectYearExpandedState, { yearCode: this.yearCode }),
    );
  }

  public expandYear() {
    this.store.dispatch(
      uiActions.expandAcademicYear({ yearCode: this.yearCode }),
    );
  }

  public collapseYear() {
    this.store.dispatch(
      uiActions.collapseAcademicYear({ yearCode: this.yearCode }),
    );
  }
}
