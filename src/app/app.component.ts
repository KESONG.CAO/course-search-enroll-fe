import { Component, OnInit, HostBinding } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { GlobalState } from './state';
import * as actions from './actions';
import {
  Router,
  ActivationStart,
  RouteConfigLoadStart,
  RouteConfigLoadEnd,
} from '@angular/router';
import { map, filter } from 'rxjs/operators';
import {
  BannerService,
  LAST_DISMISSED_BANNER_KEY,
} from '@app/services/banner.service';

@Component({
  selector: 'cse-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  @HostBinding('class') public classList = '';
  public jumpLinks: Observable<{ id: string; text: string }[]>;
  public loadCount = 0;
  public isLoading = false;
  public banner$: Observable<string | null>;
  public dismissedBannerThisSession = false;

  constructor(
    private store: Store<GlobalState>,
    router: Router,
    banners: BannerService,
  ) {
    this.banner$ = banners.banner.pipe(map(banner => banner?.text ?? null));

    this.jumpLinks = router.events.pipe(
      filter((event): event is ActivationStart => {
        return event instanceof ActivationStart;
      }),
      map(event => {
        const { jumpLinks } = event.snapshot.data;
        if (Array.isArray(jumpLinks)) {
          return jumpLinks;
        } else {
          return [];
        }
      }),
    );

    router.events.subscribe(event => {
      if (event instanceof RouteConfigLoadStart) {
        this.loadCount++;
      } else if (event instanceof RouteConfigLoadEnd) {
        this.loadCount--;
      }
      this.isLoading = !!this.loadCount;
    });

    router.events
      .pipe(
        filter((event): event is ActivationStart => {
          return event instanceof ActivationStart;
        }),
        map(event => {
          if (typeof event.snapshot.data.scrollStrategy === 'string') {
            return event.snapshot.data.scrollStrategy;
          } else {
            return 'fixed';
          }
        }),
      )
      .subscribe(strategy => {
        this.classList = `scroll-strategy-${strategy}`;
      });

    // A tool for helping debug banners
    (window as any).clearDissmissedBanner = this.clearDissmissedBanner.bind(
      this,
    );
  }

  ngOnInit() {
    this.store.dispatch(actions.prefs.fetch());
    this.store.dispatch(actions.terms.fetch());
    this.store.dispatch(actions.student.fetch());
  }

  onDismissBanner(text: string) {
    this.dismissedBannerThisSession = true;
    this.store.dispatch(
      actions.prefs.setKey({
        key: LAST_DISMISSED_BANNER_KEY,
        value: text,
      }),
    );
  }

  clearDissmissedBanner() {
    this.store.dispatch(
      actions.prefs.setKey({
        key: LAST_DISMISSED_BANNER_KEY,
        value: null,
      }),
    );
  }
}
