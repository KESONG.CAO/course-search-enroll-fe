import { createAction, props } from '@ngrx/store';
import { TermCode } from '@app/types/terms';
import {
  CourseRef,
  RoadmapCourseRef,
  CurrentCourseRef,
} from '@app/types/courses';
import {
  Schedule,
  GenerateRequest,
  NewScheduleBreak,
  ExistingScheduleBreak,
} from '@app/types/schedules';
import { CheckedPackages, SchedulerState, SchedulesError } from './state';

export namespace lifecycle {
  export const init = createAction('scheduler/lifecycle/init');
}

export const selectCourse = createAction(
  'scheduler/selectCourse',
  props<{ ref: CourseRef; isChecked: boolean }>(),
);

export namespace packages {
  export const show = createAction(
    'scheduler/packages/show',
    props<{
      ref: RoadmapCourseRef | CurrentCourseRef;
    }>(),
  );

  export const hide = createAction('scheduler/packages/hide');

  export const setCheckedState = createAction(
    'scheduler/packages/setCheckedState',
    props<{
      ref: RoadmapCourseRef;
      checked: CheckedPackages;
    }>(),
  );
}

export namespace scheduler {
  export const regenerate = createAction('scheduler/generate/regenerate');

  export const start = createAction(
    'scheduler/generate/start',
    props<{ request: GenerateRequest }>(),
  );

  export const done = createAction(
    'scheduler/generate/done',
    props<{
      request: GenerateRequest;
      schedules: Schedule[];
      checkedCourses: SchedulerState['checkedCourses'];
      checkedPacks: SchedulerState['checkedPacks'];
    }>(),
  );

  export const error = createAction(
    'scheduler/generate/error',
    props<SchedulesError>(),
  );

  export const next = createAction('scheduler/scheduler/next');

  export const prev = createAction('scheduler/scheduler/prev');

  export const jump = createAction(
    'scheduler/scheduler/jump',
    props<{ index: number }>(),
  );

  export const retry = createAction('scheduler/scheduler/retry');
}

export namespace breaks {
  export const add = createAction(
    'scheduler/breaks/add',
    props<{
      termCode: TermCode;
      brk: NewScheduleBreak;
    }>(),
  );

  export const update = createAction(
    'scheduler/breaks/update',
    props<{
      termCode: TermCode;
      brk: ExistingScheduleBreak;
    }>(),
  );

  export const remove = createAction(
    'scheduler/breaks/remove',
    props<{ id: number }>(),
  );
}

export namespace compare {
  export const mark = createAction(
    'scheduler/compare/mark',
    props<{ index: number }>(),
  );

  export const unmark = createAction(
    'scheduler/compare/unmark',
    props<{ index: number }>(),
  );
}

export namespace list {
  export const show = createAction('scheduler/list/show');

  export const hide = createAction('scheduler/list/hide');
}
