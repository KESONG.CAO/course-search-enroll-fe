import { Component, Inject } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidatorFn,
} from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { toHourMinuteMeridianUtc } from '@app/shared/date-formats';
import { BreakEntityBlock } from '@app/types/schedules';

@Component({
  selector: 'cse-edit-break-dialog',
  templateUrl: './edit-break-dialog.component.html',
  styleUrls: ['./edit-break-dialog.component.scss'],
})
export class EditBreakDialogComponent {
  public increments = generateTimeIncrements();

  public form = new FormGroup({
    id: new FormControl(this.data.id),
    days: new FormGroup(
      {
        sun: new FormControl(this.data.schedule.daysList.includes('SUNDAY')),
        mon: new FormControl(this.data.schedule.daysList.includes('MONDAY')),
        tue: new FormControl(this.data.schedule.daysList.includes('TUESDAY')),
        wed: new FormControl(this.data.schedule.daysList.includes('WEDNESDAY')),
        thu: new FormControl(this.data.schedule.daysList.includes('THURSDAY')),
        fri: new FormControl(this.data.schedule.daysList.includes('FRIDAY')),
        sat: new FormControl(this.data.schedule.daysList.includes('SATURDAY')),
      },
      [someAreTrue],
    ),
    times: new FormGroup(
      {
        startSeconds: new FormControl(this.data.schedule.startSeconds),
        endSeconds: new FormControl(
          this.data.schedule.startSeconds + this.data.schedule.durationSeconds,
        ),
      },
      [startComesBeforeEnd],
    ),
    label: new FormControl(this.data.name),
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: BreakEntityBlock['data']) {}
}

interface Increment {
  value: number; // milliseconds after midnight (in UTC)
  label: string;
}

const SEC_PER_MINUTE = 60;
const SEC_PER_HOUR = SEC_PER_MINUTE * 60;

/**
 * Generate a list of time increments to populate the start and end time
 * dropdowns. By default, these time increments run from 9am to 10pm (inclusive)
 * and occur every 15 minutes.
 *
 * Internally the increments are represented as the number of milliseconds after
 * midnight in the UTC timezone since this is the time format expected by the
 * schedule breaks endpoint.
 */
export const generateTimeIncrements = (
  startHour = 7,
  endHour = 12 + 10,
  minutesBetween = 15,
): Increment[] => {
  const start = startHour * SEC_PER_HOUR;
  const end = endHour * SEC_PER_HOUR;
  const interval = minutesBetween * SEC_PER_MINUTE;

  const incs: Increment[] = [];

  for (let value = start; value <= end; value += interval) {
    incs.push({
      value,
      label: toHourMinuteMeridianUtc(value * 1000),
    });
  }

  return incs;
};

export const someAreTrue: ValidatorFn = (group: FormGroup) => {
  if (Object.values(group.controls).some(ctrl => !!ctrl.value)) {
    return null;
  } else {
    return { noneAreTrue: true };
  }
};

export const startComesBeforeEnd: ValidatorFn = (group: FormGroup) => {
  const start: AbstractControl = group.get('startSeconds')!;
  const end: AbstractControl = group.get('endSeconds')!;

  if (
    typeof end.value === 'number' &&
    typeof start.value === 'number' &&
    end.value <= start.value
  ) {
    return { times: true };
  } else {
    return null;
  }
};
