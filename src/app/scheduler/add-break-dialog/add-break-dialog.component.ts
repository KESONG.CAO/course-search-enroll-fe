import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {
  startComesBeforeEnd,
  someAreTrue,
  generateTimeIncrements,
} from '../edit-break-dialog/edit-break-dialog.component';

@Component({
  selector: 'cse-add-break-dialog',
  templateUrl: './add-break-dialog.component.html',
  styleUrls: ['./add-break-dialog.component.scss'],
})
export class AddBreakDialogComponent {
  public increments = generateTimeIncrements();

  public form = new FormGroup({
    days: new FormGroup(
      {
        sun: new FormControl(false),
        mon: new FormControl(false),
        tue: new FormControl(false),
        wed: new FormControl(false),
        thu: new FormControl(false),
        fri: new FormControl(false),
        sat: new FormControl(false),
      },
      [someAreTrue],
    ),
    times: new FormGroup(
      {
        startSeconds: new FormControl(null),
        endSeconds: new FormControl(null),
      },
      [startComesBeforeEnd],
    ),
    label: new FormControl(null),
  });
}
