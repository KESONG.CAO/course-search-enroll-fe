import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'cse-expired-message',
  templateUrl: './expired-message.component.html',
  styleUrls: ['./expired-message.component.scss'],
})
export class ExpiredMessageComponent {
  @Output() public refresh = new EventEmitter<void>();
}
