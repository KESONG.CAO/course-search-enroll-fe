import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from '@app/shared/shared.module';
import { HeaderComponent as DegreePlannerHeaderComponent } from './degree-planner/header/header.component';
import { UnknownRouteComponent } from '@app/components/unknown-route.component';
import { FeedbackDialogComponent } from '@app/components/feedback-dialog.component';
import { HelpDialogComponent } from '@app/components/help-dialog.component';
import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { reducers, metaReducers, runtimeChecks } from './reducers';
import { EffectsModule } from '@ngrx/effects';
import { Effects } from './effects';
import { ErrorLoadingTermsDialogComponent } from './components/error-loading-terms-dialog.component';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    AppRoutingModule,

    // State management configuration
    StoreModule.forRoot(reducers, { metaReducers, runtimeChecks }),
    StoreRouterConnectingModule.forRoot(),
    StoreDevtoolsModule.instrument({ maxAge: 50 }),
    EffectsModule.forRoot([Effects]),
  ],
  declarations: [
    AppComponent,
    DegreePlannerHeaderComponent,
    UnknownRouteComponent,
    FeedbackDialogComponent,
    HelpDialogComponent,
    ErrorLoadingTermsDialogComponent,
    HeaderComponent,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
