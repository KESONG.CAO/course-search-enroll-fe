import {
  ActionReducerMap,
  MetaReducer,
  createReducer,
  on,
  On,
} from '@ngrx/store';
import * as actions from './actions';
import { GlobalState, Prefs } from './state';
import {
  Loadable,
  monad,
  success,
  failed as didFail,
  LOADING,
  isSuccess,
  isLoading,
} from 'loadable.ts';
import { TermCode, Terms } from './types/terms';
import { StudentInfo } from './types/student';
import produce from 'immer';
import { HasCurrentId, HasTermCode, RoadmapCourseRef } from './types/courses';
import { HasCourseRef } from './types/courses/attributes';

namespace prefs {
  type R = On<Loadable<Prefs>>;

  export const found: R = on(actions.prefs.found, (_state, { prefs }) => {
    return success(prefs);
  });

  export const failed: R = on(actions.prefs.failed, _state => {
    return didFail(null);
  });

  export const setKey: R = on(actions.prefs.setKey, (state, { key, value }) => {
    return monad(state).map(prefs => {
      return { ...prefs, [key]: value };
    });
  });

  export const setNestedKey: R = on(
    actions.prefs.setNestedKey,
    (state, { pref, key, value }) => {
      return monad(state).map(prefs => {
        const old: unknown = prefs[pref];
        if (typeof old === 'object' && old !== null) {
          return { ...prefs, [pref]: { ...old, [key]: value } };
        } else {
          return { ...prefs, [pref]: { [key]: value } };
        }
      });
    },
  );
}

namespace currentTermCode {
  type R = On<Loadable<TermCode>>;

  export const set: R = on(
    actions.currentTermCode.set,
    actions.terms.found,
    (_state, { currentTermCode }) => success(currentTermCode),
  );
}

namespace terms {
  type R = On<Loadable<Terms>>;

  export const found: R = on(actions.terms.found, (_state, { terms }) => {
    return success(terms);
  });

  export const failed: R = on(actions.terms.failed, _state => {
    return didFail(null);
  });
}

namespace student {
  type R = On<Loadable<StudentInfo>>;

  export const found: R = on(actions.student.found, (_state, { info }) => {
    return success(info);
  });

  export const failed: R = on(actions.student.failed, _state => {
    return didFail(null);
  });
}

namespace courses {
  type R = On<GlobalState['courses']>;

  export const load: R = on(actions.courses.load, (state, { termCode }) => {
    return produce(state, draft => {
      draft[TermCode.encode(termCode)] = {
        isValidating: false,
        roadmap: LOADING,
        current: LOADING,
      };
    });
  });

  export const done: R = on(
    actions.courses.done,
    (state, { termCode, roadmap, current }) => {
      return produce(state, draft => {
        draft[TermCode.encode(termCode)] = {
          isValidating: false,
          roadmap,
          current,
        };
      });
    },
  );

  export namespace roadmap {
    const findRoadmapCourse = (
      { termCode, ref }: HasTermCode & HasCourseRef<RoadmapCourseRef>,
      courses: GlobalState['courses'],
    ) => {
      const encoded = TermCode.encode(termCode);
      const roadmap = courses[encoded]?.roadmap;
      if (!roadmap || !isSuccess(roadmap)) {
        return null;
      }

      return roadmap.value.find(c => c.ref.roadmapId === ref.roadmapId) ?? null;
    };

    export const load: R = on(
      actions.courses.roadmap.load,
      (state, { termCode }) => {
        return produce(state, draft => {
          const key = TermCode.encode(termCode);

          if (!draft[key]) {
            draft[key] = {
              isValidating: false,
              roadmap: LOADING,
              current: null,
            };
          } else {
            draft[key].isValidating = false;
            draft[key].roadmap = LOADING;
          }
        });
      },
    );

    export const done: R = on(
      actions.courses.roadmap.done,
      (state, { termCode, roadmap }) => {
        return produce(state, draft => {
          const key = TermCode.encode(termCode);

          if (!draft[key]) {
            draft[key] = { isValidating: false, roadmap, current: null };
          } else {
            draft[key].isValidating = false;
            draft[key].roadmap = roadmap;
          }
        });
      },
    );

    export const modifyWithValidation: R = on(
      actions.courses.roadmap.addCourseWithoutPack,
      actions.courses.roadmap.addCourseWithPack,
      (state, { course }) => {
        return produce(state, draft => {
          const key = TermCode.encode(course.termCode);

          if (!draft[key]) {
            draft[key] = {
              isValidating: true,
              roadmap: LOADING,
              current: null,
            };
          } else {
            draft[key].isValidating = true;
          }
        });
      },
    );

    export const modify: R = on(
      actions.courses.roadmap.removePack,
      actions.courses.roadmap.removeCourseAndPack,
      (state, { course }) => {
        return produce(state, draft => {
          const key = TermCode.encode(course.termCode);

          if (!draft[key]) {
            draft[key] = {
              isValidating: false,
              roadmap: LOADING,
              current: null,
            };
          } else {
            draft[key].roadmap = LOADING;
          }
        });
      },
    );

    export const modifyOnBulk: R = on(
      actions.courses.roadmap.addCourseWithPackBulk,
      (state, { courses }) => {
        if (courses.length < 1) {
          return state;
        }

        return produce(state, draft => {
          const key = TermCode.encode(courses[0].course.termCode);

          if (!draft[key]) {
            draft[key] = {
              isValidating: true,
              roadmap: LOADING,
              current: null,
            };
          } else {
            draft[key].isValidating = true;
          }
        });
      },
    );

    export const validate: R = on(
      actions.courses.roadmap.validate,
      (state, { termCode }) => {
        return produce(state, draft => {
          const key = TermCode.encode(termCode);

          if (!draft[key]) {
            draft[key] = {
              isValidating: true,
              roadmap: LOADING,
              current: null,
            };
          } else {
            draft[key].isValidating = true;
          }
        });
      },
    );

    export namespace packs {
      export const load: R = on(
        actions.courses.roadmap.packs.load,
        (state, { course }) => {
          return produce(state, draft => {
            const found = findRoadmapCourse(course, draft);

            if (found) {
              if (found.package) {
                found.package.details = LOADING;
              }

              found.packages = LOADING;
            }
          });
        },
      );

      export const done: R = on(
        actions.courses.roadmap.packs.done,
        (state, { course, pack, packs }) => {
          return produce(state, draft => {
            const found = findRoadmapCourse(course, draft);

            if (found) {
              if (found.package) {
                found.package.details = pack;
              }

              found.packages = packs;
            }
          });
        },
      );
    }
  }

  export namespace current {
    const findCurrentCOurse = (
      { termCode, currentId }: HasTermCode & HasCurrentId,
      courses: GlobalState['courses'],
    ) => {
      const encoded = TermCode.encode(termCode);
      const current = courses[encoded]?.current;
      if (!current || !isSuccess(current)) {
        return null;
      }

      return current.value.find(c => c.currentId === currentId) ?? null;
    };

    export const load: R = on(
      actions.courses.current.load,
      actions.courses.current.drop,
      (state, { termCode }) => {
        return produce(state, draft => {
          const key = TermCode.encode(termCode);

          if (!draft[key]) {
            draft[key] = {
              isValidating: false,
              roadmap: null,
              current: LOADING,
            };
          } else {
            draft[key].current = LOADING;
          }
        });
      },
    );

    export const done: R = on(
      actions.courses.current.done,
      (state, { termCode, current }) => {
        return produce(state, draft => {
          const key = TermCode.encode(termCode);

          if (!draft[key]) {
            draft[key] = { isValidating: false, roadmap: null, current };
          } else {
            draft[key].current = current;
          }
        });
      },
    );

    export const enroll: R = on(
      actions.courses.current.enroll,
      (state, { termCode }) => {
        return produce(state, draft => {
          const key = TermCode.encode(termCode);

          if (draft[key]) {
            draft[key] = {
              isValidating: false,
              roadmap: LOADING,
              current: LOADING,
            };
          }
        });
      },
    );

    export namespace pack {
      export const load: R = on(
        actions.courses.current.pack.load,
        (state, { course }) => {
          return produce(state, draft => {
            const found = findCurrentCOurse(course, draft);

            if (found) {
              found.package.details = LOADING;
            }
          });
        },
      );

      export const done: R = on(
        actions.courses.current.pack.done,
        (state, { course, pack }) => {
          return produce(state, draft => {
            const found = findCurrentCOurse(course, draft);

            if (found) {
              found.package.details = pack;
            }
          });
        },
      );
    }

    export namespace details {
      export const load: R = on(actions.details.load, (state, { course }) => {
        return produce(state, draft => {
          if (course.ref.kind === 'current') {
            const found = findCurrentCOurse(
              {
                termCode: course.ref.termCode,
                currentId: course.ref.currentId,
              },
              draft,
            );

            if (found) {
              found.details = { mode: 'loading' };
            }
          }
        });
      });

      export const done: R = on(
        actions.details.done,
        (state, { course, details }) => {
          return produce(state, draft => {
            if (course.ref.kind === 'current') {
              const found = findCurrentCOurse(
                {
                  termCode: course.ref.termCode,
                  currentId: course.ref.currentId,
                },
                draft,
              );

              if (found) {
                if (isSuccess(details)) {
                  found.details = { mode: 'loaded', ...details.value };
                } else if (isLoading(details)) {
                  found.details = { mode: 'loading' };
                } else {
                  found.details = { mode: 'error' };
                }
              }
            }
          });
        },
      );
    }
  }
}

namespace saved {
  type R = On<GlobalState['saved']>;

  export const load: R = on(
    actions.saved.load,
    actions.saved.add,
    actions.saved.remove,
    () => {
      return LOADING;
    },
  );

  export const done: R = on(actions.saved.done, (_, { saved }) => {
    return saved;
  });
}

export const reducers: ActionReducerMap<GlobalState> = {
  prefs: createReducer(
    LOADING,
    prefs.found,
    prefs.failed,
    prefs.setKey,
    prefs.setNestedKey,
  ),
  currentTermCode: createReducer(LOADING, currentTermCode.set),
  terms: createReducer(LOADING, terms.found, terms.failed),
  student: createReducer(LOADING, student.found, student.failed),
  courses: createReducer(
    {},
    courses.load,
    courses.done,
    courses.roadmap.load,
    courses.roadmap.done,
    courses.roadmap.modify,
    courses.roadmap.modifyOnBulk,
    courses.roadmap.validate,
    courses.roadmap.packs.load,
    courses.roadmap.packs.done,
    courses.current.load,
    courses.current.done,
    courses.current.enroll,
    courses.current.pack.load,
    courses.current.pack.done,
    courses.current.details.load,
    courses.current.details.done,
  ),
  saved: createReducer(null, saved.load, saved.done),
};

export const metaReducers: MetaReducer<GlobalState>[] = [];

export const runtimeChecks = {
  strictStateImmutability: true,
  strictActionImmutability: true,
  strictStateSerializability: false,
  strictActionSerializability: false,
};
