export const environment = {
  production: true,
  version: '2.0.0',
  apiPlannerUrl: '/api/planner/v1',
  apiSearchUrl: '/api/search/v1',
  apiEnrollUrl: '/api/enroll/v1',
  apiDarsUrl: '/api/dars',
  apiDarsData: '/api/data',
  snackbarDuration: 4000,
};
