# Course Search & Enroll Front End

## Development server

Run `npm start` or `ng serve --proxy-config proxy.json` for a dev server.
Navigate to `http://localhost:4200/`. The app will automatically reload if you
change any of the source files.

## Data management

During an app session, a large amount of data will need to be downloaded from a
lot of backend APIs. Some of this data will only be used by the view that
downloaded it but most of this data is useful to multiple views. When data is
useful to multiple views we want a structured way to share that data so that
each view doesn't need to download its own copy of the same data.

This is a situation where the structure of the NgRX data store is useful. For
data that we want to share between views, it can be kept in the app-level store
and when a view needs the data it can query the central store.

What are some examples of data that is shared between views?

- User preferences
- Student academic program info
- A list of the currently active terms and the subjects, sessions, etc. in each
  of those active terms.

What are some examples of data that don't make much sense to be shared between
views? Any data that is transient like search results or UI state should be kept
within the relevant view.

### Long-term advantages of shared data

An exciting consequence of having a structured way to share data between views
is how this can help keep views synchronized in the future. If a user saves a
course for later in the My Courses view and then clicks into the Degree Planner
view, the Degree Planner should reflect the new data and not a stale copy from
earlier in the session.

### Shared data architecture

#### Loading data and storing it in the `GlobalState`

Any data that is going to be shared between views needs a place in the
`GlobalState` object described in the `src/app/state.ts` file.

For data that should be loaded when the app launches, the following pattern has
been useful:

1. Dispatch an action to fetch the data from within the `AppComponent#ngOnInit`
   method. This is already where the term data, user preferences, and student
   info fetch actions are dispatched.

2. Add an effect in the `src/app/effects.ts` file to fetch the data from the
   appropriate API endpoint. This is the step where any data preprocessing
   should be done. For example, this step is where backend term-code strings are
   converted into frontend `TermCode` objects.

   **Be sure to handle errors fetching or parsing the data.**

   If the term data can't be loaded the app can't work so any errors are handled
   by showing the user an un-closeable error message.

   Other API calls should probably be handled in a less extreme manner by only
   preventing the usage of certain parts of the app while also giving the user
   an opportunity to retry the failed API call.

   When the API call has been resolved either successfully or unsuccessfully,
   dispatch a new action from the effect to update the `GlobalState` object with
   either the successfully loaded data or an error indicator.

3. Update the `GlobalState` object with the data or with an error indicator.
   After the state is updated, any selectors listening for this data can start
   using it in the current view.

#### Components consuming shared global data

Any data stored in the `GlobalState` should have accompanying NgRX selectors
defined in the `src/app/selectors.ts` file. For data with some type `T`, these
selectors should return a value of type `Loadable<T>` where the `Loadable` type
comes from the [`loadable.ts`](https://www.npmjs.com/package/loadable.ts)
library installed via npm.

For example, to retrieve the array of active term-codes from the global store,
the following code would work:

```typescript
import { Store } from '@ngrx/store';
import { GlobalState } from '@app/state';
import * as globalSelectors from '@app/selectors';
import { ifSuccessThenUnwrap } from '@app/core/utils';

export class MyComponent {
  public activeTermCodes$: Observable<TermCode[]>;

  constructor(store: Store<GlobalState>) {
    this.activeTermCodes$ = store
      .select(globalSelectors.terms.getActiveTermCodes) // <- (a)
      .pipe(ifSuccessThenUnwrap()); // <- (b)
  }
}
```

A couple lines of note:

- **(a)** This line uses an NgRX selector built for getting data out of the
  global store. However it returns an observable of values with the type
  `Loadable<TermCode[]>`. The values are wrapped in the `Loadable<T>` type to
  indicate that the values may be loading, may have failed to load, or may have
  loaded already. In this context the component isn't interested in the values
  that are loading or that have failed to load. To return only the successfully
  loaded values, observe the following line.

- **(b)** This line filters out any values from the observable that didn't load
  successfully. The inclusion of this operator means that the
  `this.activeTermCodes$` observable won't produce any values until the set of
  active term-codes have been successfully loaded from the backend. This has the
  benefit of keeping any downstream observables or components waiting until
  enough data exists to run the app. In some situations this waiting is not
  desirable but usually with the active term-codes this is what we want.

## Common patterns

### How to use theme colors inside of a component's stylesheet?

If you're using an Angular Material component, check if the component supports
the `color` attribute like this:

```html
<mat-toolbar color="primary"></mat-toolbar>
```

If you need to use the theme colors in SCSS, **don't hard-code the UW red and UW blue colors.**
Import the `cse-theme` SCSS file like this:

```scss
@import 'cse-theme';

// the rest of the component's styles
```

And then use the `primary-color()` and `accent-color()` SCSS functions wherever
you need the theme colors:

```scss
@import 'cse-theme';

.my-custom-button {
  background: primary-color();
}

// the functions support tints as well
.my-other-button {
  background-color: accent-color(600);
}
```

### How to change a component behavior or appearance based on the screen size?

When in a stylesheet, import the `cse-theme` and use the `is-mobile` or `is-desktop` mixins:

```scss
@import 'cse-theme';

@include is-mobile {
  div {
    flex-direction: column;
  }
}

@include is-desktop {
  div {
    flex-direction: row;
  }
}
```

When in a component definition:

```ts
import { Breakpoints } from '@app/shared/breakpoints';
import { BreakpointObserver } from '@angular/cdk/layout';

class MyComponent {
  constructor(breakpoints: BreakpointObserver) {
    breakpoints
      .observe(Breakpoints.Mobile)
      .subscribe(({ matches: isMobile }) => {
        console.log(`is mobile? ${isMobile}`);
      });
  }
}
```

### How to display a dialog?

The `MatDialog` service often has too many options for efficiently creating
dialogs so this project created a wrapper around the Angular Material dialogs
called `DialogService`. Components using this service can create dialogs that
will behave correctly on different screen sizes and have consistent
accessibility and layout.

#### Acknowledgment dialogs

Sometimes if an action fails the user should be clearly notified but there is
no action for the user to take. In these cases the `DialogService` provides an
acknowledgment dialog:

```typescript
class MyComponent {
  constructor(private dialog: DialogService) {}

  ifTheActionFailed() {
    this.dialog.acknowledge({
      headline: 'The action failed',
      affirmative: 'Okay',
    });
  }
}
```

The `DialogService#acknowledge` method returns a `Promise<void>` that will
resolve when the user closes the dialog. The text of the dialog headline,
contents, and buttons is configurable.

#### Confirmation dialogs

If the dialog only needs to ask the user for confirmation, there's no need to
create a new component. The `DialogService` has a method for creating a prompt
dialog like this:

```typescript
class MyComponent {
  constructor(private dialog: DialogService) {}

  async onClick() {
    const shouldDoAction = await this.dialog.confirm({
      headline: 'Are you sure?',
      negative: 'No',
      affirmative: 'Yes',
    });

    if (shouldDoAction) {
      console.log('okay!');
    } else {
      console.log('nevermind');
    }
  }
}
```

The `DialogService#confirm` method returns a `Promise<boolean>` that will
resolve when the user makes a choice. The text of the dialog headline, contents,
and buttons is configurable.

Examples of confirmation dialogs can be found in the My Courses view.

#### Custom dialogs

Some dialogs need very specific layouts or behaviors that can't be accommodated
by the acknowledge and confirm methods. For these cases the `DialogService` has
two methods `DialogService#small` and `DialogService#large` for creating dialogs
with a custom component as the dialog contents.

For example a custom dialog component could look like this:

```typescript
@Component({
  selector: 'cse-custom-dialog',
  template: `
    <cse-dialog headline="My dialog">
      <cse-dialog-body>
        <p>Pick a number:</p>
        <ol>
          <li *ngFor="let num of numbers">{{ num }}</li>
        </ol>
      </cse-dialog-body>
      <cse-dialog-actions>
        <button mat-button mat-dialog-close>Nevermind</button>
        <div class="spacer"></div>
        <button mat-raised-button mat-dialog-close>I picked a number</button>
      </cse-dialog-actions>
    </cse-dialog>
  `,
})
export class CustomDialog {
  constructor(@Inject(MAT_DIALOG_DATA) public numbers: number[]) {}
}
```

And a dialog using that component could be instantiated like this:

```typescript
class MyComponent {
  constructor(private dialog: DialogService) {}

  async onClick() {
    const dataForDialog = [1, 2, 3];
    const response = await this.dialog.large(CustomDialog, dataForDialog);
    console.log('whatever the dialog component chose to return:', response);
  }
}
```

The only difference between the `DialogService#large` and `DialogService#small`
methods is the maximum width of the dialog. On wide screens the maximum width
of small dialogs is `512px` and the maximum width of large dialogs is `1024px`.

## Useful information

### Term-codes

Term-codes are how the UW Madison curricular backend references terms (i.e.
semesters). Term-codes are represented on the backend as 4 character strings
where each character is an ASCII digit. For example, here are some valid
term-codes along with a human-readable description of the corresponding term:

```
1234    -- Spring 2023
1212    -- Fall 2020
1214    -- Spring 2021
1216    -- Summer 2021
0994    -- Spring 1999
```

#### Structure

At first glance it may be tricky to see how the 4 digit term-codes correspond
to years and semesters but the structure of the term-codes is pretty
straightforward:

```
.- the 1st digit is the century offset
|
|  .- the 4th digit is the term offset
|  |
v  v
1234
 ^^
 |
 '- the 2nd and 3rd digits are the year offset
```

##### Century offset

The century digit encodes what century it is. Years between 1900 and 1999 have a
century offset of 0. Years between 2000 and 2099 have an offset of 1 with future
centuries incrementing by one.

##### Year offset

The middle 2 digits encode what year in the century the term is from.
If the term takes place in the 2004 academic year, the year offset is `04`.
If the term takes place in 1982 academic year, the year offset would be `82`.

Given a century offset and a year offset, the formula for determining the
academic year is:

```
1900 + (100 * century_offset) + year_offset
```

**Be aware:** the year computed from a term-code always corresponds to the
calendar year that the term's academic year _ends_ in. For example, the fall
2020 term takes place in the 2021 academic year so it's term-code year offset is
`21`, not `20`. Spring and summer terms have the same academic and calendar years.

##### Term offset

The last digit determines whether the term takes place in the fall, spring, or
summer. These values are more arbitrary:

| Term offset value | Semester |
| ----------------- | -------- |
| `2`               | Fall     |
| `3` (rare)        | Fall     |
| `4`               | Spring   |
| `6`               | Summer   |

### The zero term-code

A term-code set to the value `0000` is special and sort of represents the idea
of 'any term'. One place this value is used is searching for courses. If the
user wants to search all active and inactive terms, the API call queries courses
in the `0000` term as a way of indicating that the user didn't pick a specific
term and wants all possible matches.

### Internal representation

Because term-codes are used throughout the curricular data and because it's
tedious to constantly be re-parsing term-codes for presentation reasons the
app parses all term-codes after receiving them from the backend.

There are a few constraints on how term-codes are internally represented:

- Should easily JSON serialized for storage in NgRX
- Should not require frequent re-parsing
- Should be sortable and comparable

JSON serializability eliminates the use of JS classes because the prototype
chain isn't serializable and would be lost during the JSON encoding. Instead
term-codes are represented at runtime using plain JS objects and at compile
time with the `TermCode` type.

Functions are provided in the `@app/types/terms` library for encoding, decoding,
comparison, and sorting. The internal term-code objects look like this:

```typescript
type Digit = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9';

interface TermCode {
  century: Digit;
  year: [Digit, Digit];
  term: '2' | '3' | '4' | '6';
}

enum Era {
  Past,
  Active,
  Future,
}

interface WithEra extends TermCode {
  era: Era;
}
```

The `WithEra` term is used in some circumstances where a term-code's relation to
the active term set is necessary for correct behavior. An example of this is the
degree planner view.

#### Encoding and decoding term-codes

The internal `@app/types/terms` library can be used to convert strings into
parsed term-codes and back again. For example:

```typescript
import { TermCode, WithEra } from '@app/types/terms';

// Converting strings to term-codes:

TermCode.decodeOrNull('1234'); // { century: '1', year: ['2', '3'], term: '4' }
TermCode.decodeOrNull('abcd'); // null
TermCode.decodeOrThrow('abcd'); // ERROR! 'abcd' is not a valid term-code

// Converting a term-code to different kinds of strings for machines or humans:

const termCode: TermCode = { century: '1', year: ['2', '3'], term: '4' };

TermCode.encode(termCode); // '1234'
TermCode.describe(termCode); // 'Spring 2023'

// Creating a new term-code with an `era` field given an active term set:

const active = [TermCode.decodeOrThrow('1212')];
WithEra.fromTermCode(active, termCode); // { ...termCode, era: Future }
```

## Pipes

The following pipes are implemented in the `shared` module and are automatically
imported into all of the view modules in the project.

| Name         | Input type                        | Argument     | Return type | Example                    |
| ------------ | --------------------------------- | ------------ | ----------- | -------------------------- |
| `isInactive` | `HasTermCodeWithEra` or `WithEra` |              | `boolean`   |                            |
| `isActive`   | `HasTermCodeWithEra` or `WithEra` |              | `boolean`   |                            |
| `isPast`     | `HasTermCodeWithEra` or `WithEra` |              | `boolean`   |                            |
| `isFuture`   | `HasTermCodeWithEra` or `WithEra` |              | `boolean`   |                            |
| `encodeTerm` | `HasTermCodeOrZero` or `TermCode` |              | `string`    | Spring 2021                |
| `has`        | `any`                             | a field name | `boolean`   | `course \| has: 'subject'` |
| `get`        | `any`                             | a field name | `any`       | `course \| get: 'subject'` |

## Proxying

You do not need to run any of the backend infrastructure locally to develop the
front end. All requests are being proxied to the test server (test.enroll.wisc.edu)

## Mod Headers

Very much of the application will not work without being logged in as a student.
To impersonate a student you will need a student login that has been approved
for the test environment. Speak with the dev team and they will provide you with
a proper student. Next, you will need to user a browser extension to include the
appropriate headers. I suggest the ModHeaders extension for Chrome & Firefox.
Add the following headers:

    uid: <test student value>
    wiscEduISISEmplID: <test student value>
    wiscEduPVI: <test student value>
    Authorization: <proxy auth string>
    X-API-Key: <web sockets auth string>

## Tier Config

Each of the 3 tiers (Dev, Test, Prod) Load a `config.js` file with various
environment settings. This file is also in the repo but only gets used on
`build -prod`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the
`dist/` directory. Use the `--prod` flag for a production build.

## Deploying the Front End

The front end is deployed to Amazon S3 via git lab runner. From the main repo
page -> CI/CD -> Pipelines and click the appropriate commit to deploy. A simple
button click will deploy the front end to S3 in the chosen tier. All prod
deploys need team coordination and a service window.

## Running TS Linter

Run `ng lint` to excute typescript linting. `ng lint --fix` will attempt to fix
any errors

## Accessibilty Change Log

[Accessibility Change Log](accessibility.md)
